export const tables = Object.freeze({
    USERS: "users",
    PROJECT: "projects",
    ARTICLE: "articles",
    DISCUSSION: "discussions",
    PROJVOTE: "projVotes",
    ARTVOTE: "artVotes",
    DISCUSVOTE: "discusVotes",
    PROJCOMMENT: "projComments",
    DISCUSCOMMENT: "discusComments",
    PROJCOMMENTREPLY: "projCommentsReply",
    DISCUSCOMMENTREPLY: "discusCommentsReply"
})
