import { DiscussionService } from "../services/DiscussionService"
import { Request, Response } from "express"
import { logger } from "../utils/logger"
import jwt from "../jwt";
import jwtSimple from "jwt-simple";
import { DiscusCommentReplyService } from "../services/CommentReplyService";
import { DiscusCommentService } from "../services/CommentService";
import { DiscusVoteService } from "../services/VoteService";

export class DiscussionController {
    constructor(private discussionService: DiscussionService, private discusCommentReplyService: DiscusCommentReplyService, private discusCommentService: DiscusCommentService, private discusVoteService: DiscusVoteService) { }

    createDiscussion = async (req: Request, res: Response) => {
        try {
            if (!req.body.title || !req.body.description) {
                res.status(401).json({ msg: "Missing Required Input" })
                return;
            }
            let { token, title, description, tags } = req.body;
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user_id = payload.id
            await this.discussionService.createDiscussion(
                user_id,
                title,
                description,
                tags,
            );
            res.json({ msg: "Discussion Successfully Created" })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getDiscussion = async (req: Request, res: Response) => {
        try {
            const { token, username } = req.body;
            const user = await this.discussionService.getUserByUsername(username)
            const allDiscussion = await this.discussionService.getDiscussion(user.id);
            let discusComments: any = [];
            let discusVotes: any = [];
            let discusUsers: any = [];
            let discusCommentReplys: any = [];
            let allDiscusCommentUsersReplys: any = [];
            let allDiscusCommentUsers: any = [];
            let commentCountNums: any = [];
            let { discusComment, discusVote, discusCommentReply, discusCommentUser, discusCommentReplyUser, discusVoteCount, discusStatComment, discusStatCommentReply, commentCountNum }: any = []
            for (let i = 0; i < allDiscussion.length; i++) {
                const discusUser = await this.discussionService.getDiscusUserByID(allDiscussion[i].user_id);
                // discusUser.avator = `http://${req.url}/${discusUser.avator}`
                discusUsers.push(discusUser);

                discusComment = await this.discusCommentService.getDiscusComment(allDiscussion[i].id);
                discusComments.push(discusComment);

                discusVoteCount = await this.discusVoteService.getDiscusVoteCount(allDiscussion[i].id);
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    discusVote = await this.discusVoteService.getDiscusVoteByUser(allDiscussion[i].id, payload.id);
                    if (discusVote.length < 1) {
                        discusVote = [{ voted: false }]
                    }
                } else {
                    discusVote = [{ voted: false }]
                }

                discusVotes.push(discusVote);
                await this.discussionService.updateDiscusVote(allDiscussion[i].id, Number(discusVoteCount[0].count))
                allDiscussion[i].vote = discusVoteCount[0].count

                discusStatComment = await this.discussionService.getStatDiscusComment(allDiscussion[i].id);
                discusStatCommentReply = await this.discussionService.getStatDiscusCommentReply(allDiscussion[i].id);
                commentCountNum = Number(discusStatComment[0].count) + Number(discusStatCommentReply[0].count)
                commentCountNums.push(commentCountNum)

                let discusCommentUsers: any = [];
                for (let j = 0; j < discusComment.length; j++) {
                    let discusCommentUsersReplys: any = [];
                    discusCommentReply = await this.discusCommentReplyService.getDiscusCommentReply(allDiscussion[i].id, discusComment[j].id);
                    discusCommentReplys.push(discusCommentReply)
                    discusCommentUser = await this.discusCommentService.getDiscusCommentUserByID(discusComment[j].user_id);
                    // discusCommentUser.avator = `http://${req.url}/${discusCommentUser.avator}`
                    discusCommentUsers.push(discusCommentUser);
                    for (let k = 0; k < discusCommentReply.length; k++) {
                        discusCommentReplyUser = await this.discusCommentReplyService.getDiscusCommentReplyUserByID(discusCommentReply[k].user_id);
                        // discusCommentReplyUser.avator = `http://${req.url}/${discusCommentReplyUser.avator}`
                        discusCommentUsersReplys.push(discusCommentReplyUser);
                    }
                    allDiscusCommentUsersReplys.push(discusCommentReplyUser);
                }
                allDiscusCommentUsers.push(discusCommentUsers)
            }
            const commentCount = { commentCountNums: commentCountNums }
            const allDiscusUsers = { discusUsers: discusUsers }
            const allDiscusCommentsUsers = { allDiscusCommentUsers: allDiscusCommentUsers }
            const allDiscusCommentReplysUsers = { allDiscusCommentUsersReplys: allDiscusCommentUsersReplys }
            // console.log(AllDiscussion, 'AllDiscussion')
            res.json({ allDiscussion, discusComments, discusVotes, discusCommentReplys, allDiscusUsers, allDiscusCommentsUsers, allDiscusCommentReplysUsers, commentCount })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getDiscussionByTag = async (req: Request, res: Response) => {
        try {
            const { token, tag } = req.body;
            const allDiscussion = await this.discussionService.getDiscusByTag(tag);
            let discusComments: any = [];
            let discusVotes: any = [];
            let discusUsers: any = [];
            let discusCommentReplys: any = [];
            let allDiscusCommentUsersReplys: any = [];
            let allDiscusCommentUsers: any = [];
            let commentCountNums: any = [];
            let { discusComment, discusVote, discusCommentReply, discusCommentUser, discusCommentReplyUser, discusVoteCount, discusStatComment, discusStatCommentReply, commentCountNum }: any = []
            for (let i = 0; i < allDiscussion.length; i++) {
                const discusUser = await this.discussionService.getDiscusUserByID(allDiscussion[i].user_id);
                // discusUser.avator = `http://${req.url}/${discusUser.avator}`
                discusUsers.push(discusUser);

                discusComment = await this.discusCommentService.getDiscusComment(allDiscussion[i].id);
                discusComments.push(discusComment);

                discusVoteCount = await this.discusVoteService.getDiscusVoteCount(allDiscussion[i].id);
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    discusVote = await this.discusVoteService.getDiscusVoteByUser(allDiscussion[i].id, payload.id);
                    if (discusVote.length < 1) {
                        discusVote = [{ voted: false }]
                    }
                } else {
                    discusVote = [{ voted: false }]

                }
                discusVotes.push(discusVote);
                await this.discussionService.updateDiscusVote(allDiscussion[i].id, Number(discusVoteCount[0].count))
                allDiscussion[i].vote = discusVoteCount[0].count

                discusStatComment = await this.discussionService.getStatDiscusComment(allDiscussion[i].id);
                discusStatCommentReply = await this.discussionService.getStatDiscusCommentReply(allDiscussion[i].id);
                commentCountNum = Number(discusStatComment[0].count) + Number(discusStatCommentReply[0].count)
                commentCountNums.push(commentCountNum)

                let discusCommentUsers: any = [];
                for (let j = 0; j < discusComment.length; j++) {
                    let discusCommentUsersReplys: any = [];
                    discusCommentReply = await this.discusCommentReplyService.getDiscusCommentReply(allDiscussion[i].id, discusComment[j].id);
                    discusCommentReplys.push(discusCommentReply)
                    discusCommentUser = await this.discusCommentService.getDiscusCommentUserByID(discusComment[j].user_id);
                    // discusCommentUser.avator = `http://${req.url}/${discusCommentUser.avator}`
                    discusCommentUsers.push(discusCommentUser);
                    for (let k = 0; k < discusCommentReply.length; k++) {
                        discusCommentReplyUser = await this.discusCommentReplyService.getDiscusCommentReplyUserByID(discusCommentReply[k].user_id);
                        // discusCommentReplyUser.avator = `http://${req.url}/${discusCommentReplyUser.avator}`
                        discusCommentUsersReplys.push(discusCommentReplyUser);
                    }
                    allDiscusCommentUsersReplys.push(discusCommentReplyUser);
                }
                allDiscusCommentUsers.push(discusCommentUsers)
            }
            const commentCount = { commentCountNums: commentCountNums }
            const allDiscusUsers = { discusUsers: discusUsers }
            const allDiscusCommentsUsers = { allDiscusCommentUsers: allDiscusCommentUsers }
            const allDiscusCommentReplysUsers = { allDiscusCommentUsersReplys: allDiscusCommentUsersReplys }
            // console.log(AllDiscussion, 'AllDiscussion')
            res.json({ allDiscussion, discusComments, discusVotes, discusCommentReplys, allDiscusUsers, allDiscusCommentsUsers, allDiscusCommentReplysUsers, commentCount })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getAllDiscussion = async (req: Request, res: Response) => {
        try {
            const { token } = req.body;
            const allDiscussion = await this.discussionService.getAllDiscussion();
            let discusComments: any = [];
            let discusVotes: any = [];
            let discusUsers: any = [];
            let discusCommentReplys: any = [];
            let allDiscusCommentUsersReplys: any = [];
            let allDiscusCommentUsers: any = [];
            let commentCountNums: any = [];
            let { discusComment, discusVote, discusCommentReply, discusCommentUser, discusCommentReplyUser, discusVoteCount, discusStatComment, discusStatCommentReply, commentCountNum }: any = []
            for (let i = 0; i < allDiscussion.length; i++) {
                const discusUser = await this.discussionService.getDiscusUserByID(allDiscussion[i].user_id);
                // discusUser.avator = `http://${req.url}/${discusUser.avator}`
                discusUsers.push(discusUser);

                discusComment = await this.discusCommentService.getDiscusComment(allDiscussion[i].id);
                discusComments.push(discusComment);
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    discusVote = await this.discusVoteService.getDiscusVoteByUser(allDiscussion[i].id, payload.id);
                    if (discusVote.length < 1) {
                        discusVote = [{ voted: false }]
                    }
                } else {
                    discusVote = [{ voted: false }]
                }
                discusVotes.push(discusVote);

                discusVoteCount = await this.discusVoteService.getDiscusVoteCount(allDiscussion[i].id);
                await this.discussionService.updateDiscusVote(allDiscussion[i].id, Number(discusVoteCount[0].count))
                allDiscussion[i].vote = discusVoteCount[0].count

                discusStatComment = await this.discussionService.getStatDiscusComment(allDiscussion[i].id);
                discusStatCommentReply = await this.discussionService.getStatDiscusCommentReply(allDiscussion[i].id);
                commentCountNum = Number(discusStatComment[0].count) + Number(discusStatCommentReply[0].count)
                commentCountNums.push(commentCountNum)

                let discusCommentUsers: any = [];
                for (let j = 0; j < discusComment.length; j++) {
                    let discusCommentUsersReplys: any = [];
                    discusCommentReply = await this.discusCommentReplyService.getDiscusCommentReply(allDiscussion[i].id, discusComment[j].id);
                    discusCommentReplys.push(discusCommentReply)
                    discusCommentUser = await this.discusCommentService.getDiscusCommentUserByID(discusComment[j].user_id);
                    // discusCommentUser.avator = `http://${req.url}/${discusCommentUser.avator}`
                    discusCommentUsers.push(discusCommentUser);
                    for (let k = 0; k < discusCommentReply.length; k++) {
                        discusCommentReplyUser = await this.discusCommentReplyService.getDiscusCommentReplyUserByID(discusCommentReply[k].user_id);
                        // discusCommentReplyUser.avator = `http://${req.url}/${discusCommentReplyUser.avator}`
                        discusCommentUsersReplys.push(discusCommentReplyUser);
                    }
                    allDiscusCommentUsersReplys.push(discusCommentReplyUser);
                }
                allDiscusCommentUsers.push(discusCommentUsers)
            }


            // feature
            const featAllDiscussion = await this.discussionService.getDiscusSortByVotes();
            let featDiscusComments: any = [];
            let featDiscusVotes: any = [];
            let featDiscusUsers: any = [];
            let featDiscusCommentReplys: any = [];
            let featAllDiscusCommentUsersReplys: any = [];
            let featAllDiscusCommentUsers: any = [];
            let featCommentCountNums: any = [];
            let { featDiscusComment, featDiscusVote, featDiscusCommentReply, featDiscusCommentUser, featDiscusCommentReplyUser, featDiscusVoteCount, featDiscusStatComment, featDiscusStatCommentReply, featCommentCountNum }: any = []
            for (let i = 0; i < featAllDiscussion.length; i++) {
                const discusUser = await this.discussionService.getDiscusUserByID(featAllDiscussion[i].user_id);
                // discusUser.avator = `http://${req.url}/${discusUser.avator}`
                featDiscusUsers.push(discusUser);
                featDiscusComment = await this.discusCommentService.getDiscusComment(featAllDiscussion[i].id);
                featDiscusComments.push(featDiscusComment);

                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    featDiscusVote = await this.discusVoteService.getDiscusVoteByUser(featAllDiscussion[i].id, payload.id);
                    if (featDiscusVote.length < 1) {
                        featDiscusVote = [{ voted: false }]
                    }
                } else {
                    featDiscusVote = [{ voted: false }]
                }
                featDiscusVotes.push(featDiscusVote);
                featDiscusVoteCount = await this.discusVoteService.getDiscusVoteCount(featAllDiscussion[i].id);
                await this.discussionService.updateDiscusVote(featAllDiscussion[i].id, Number(featDiscusVoteCount[0].count))
                featAllDiscussion[i].vote = featDiscusVoteCount[0].count

                featDiscusStatComment = await this.discussionService.getStatDiscusComment(featAllDiscussion[i].id);
                featDiscusStatCommentReply = await this.discussionService.getStatDiscusCommentReply(featAllDiscussion[i].id);
                featCommentCountNum = Number(featDiscusStatComment[0].count) + Number(featDiscusStatCommentReply[0].count)
                featCommentCountNums.push(featCommentCountNum)
                let featDiscusCommentUsers: any = [];
                for (let j = 0; j < featDiscusComment.length; j++) {
                    let discusCommentUsersReplys: any = [];
                    featDiscusCommentReply = await this.discusCommentReplyService.getDiscusCommentReply(featAllDiscussion[i].id, featDiscusComment[j].id);
                    featDiscusCommentReplys.push(featDiscusCommentReply)
                    featDiscusCommentUser = await this.discusCommentService.getDiscusCommentUserByID(featDiscusComment[j].user_id);
                    // featDiscusCommentUser.avator = `http://${req.url}/${featDiscusCommentUser.avator}`
                    featDiscusCommentUsers.push(featDiscusCommentUser);
                    for (let k = 0; k < featDiscusCommentReply.length; k++) {
                        featDiscusCommentReplyUser = await this.discusCommentReplyService.getDiscusCommentReplyUserByID(featDiscusCommentReply[k].user_id);
                        // featDiscusCommentReplyUser.avator = `http://${req.url}/${featDiscusCommentReplyUser.avator}`
                        discusCommentUsersReplys.push(featDiscusCommentReplyUser);
                    }
                    featAllDiscusCommentUsersReplys.push(discusCommentUsersReplys);
                }
                featAllDiscusCommentUsers.push(featDiscusCommentUsers)
            }

            // feature 
            const featCommentCount = { featCommentCountNums: featCommentCountNums }
            const featAllDiscusUsers = { featDiscusUsers: featDiscusUsers }
            const featAllDiscusCommentsUsers = { featAllDiscusCommentUsers: featAllDiscusCommentUsers }
            const featAllDiscusCommentReplysUsers = { featAllDiscusCommentUsersReplys: featAllDiscusCommentUsersReplys }

            const commentCount = { commentCountNums: commentCountNums }
            const allDiscusUsers = { discusUsers: discusUsers }
            const allDiscusCommentsUsers = { allDiscusCommentUsers: allDiscusCommentUsers }
            const allDiscusCommentReplysUsers = { allDiscusCommentUsersReplys: allDiscusCommentUsersReplys }
            // console.log(discusComments, 'discusComments')
            res.json({ allDiscussion, discusComments, discusVotes, discusCommentReplys, allDiscusUsers, allDiscusCommentsUsers, allDiscusCommentReplysUsers, commentCount, featAllDiscussion, featDiscusComments, featDiscusVotes, featDiscusCommentReplys, featAllDiscusUsers, featAllDiscusCommentsUsers, featAllDiscusCommentReplysUsers, featCommentCount })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }
};