import { ProjectService } from "../services/ProjectService"
import { ProjVoteService } from "../services/VoteService"
import { Request, Response } from "express"
import { logger } from "../utils/logger"
import jwt from "../jwt";
import jwtSimple from "jwt-simple";
import { ProjCommentService } from "../services/CommentService";
import { ProjCommentReplyService } from "../services/CommentReplyService";

export class ProjectController {
    constructor(private projectService: ProjectService, private projVoteService: ProjVoteService, private projCommentService: ProjCommentService, private projCommentReplyService: ProjCommentReplyService) { }

    createProject = async (req: Request, res: Response) => {
        try {
            const files = req.files as { [fieldname: string]: Express.Multer.File[] };
            if (!req.body.title || !req.body.headline || !req.body.status || !req.body.description) {
                res.status(401).json({ msg: "Missing Required Input" })
                return;
            }
            let imgThumb = files.imgThumb[0].filename
            // let imgThumb = `http://${req.hostname}:8080/${files.imgThumb[0].filename}`
            let imgDetail: any = []
            for (let i = 0; i < files.imgDetail.length; i++) {
                imgDetail.push(files.imgDetail[i].filename)
                // imgDetail.push`http://${req.hostname}:8080/${files.imgDetail[i].filename}`
            }
            let { token, title, headline, status, description, tags, link, videoLink } = req.body;
            link = link.includes("https://") ? link : "https://" + link
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user_id = payload.id
            await this.projectService.createProject(
                user_id,
                title,
                headline,
                status,
                description,
                tags,
                link,
                imgThumb,
                imgDetail,
                videoLink
            );
            res.json({ msg: "Project Successfully Created" })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getProject = async (req: Request, res: Response) => {
        try {
            const { token, username } = req.body;
            const user = await this.projectService.getUserByUsername(username)
            const allProject = await this.projectService.getProject(user.id);
            let projComments: any = [];
            let projVotes: any = [];
            let projUsers: any = [];
            let projCommentReplys: any = [];
            let allProjCommentUsersReplys: any = [];
            let allProjCommentUsers: any = [];
            let commentCountNums: any = [];
            let { projComment, projVote, projCommentReply, projCommentUser, projCommentReplyUser, projVoteCount, projStatComment, projStatCommentReply, commentCountNum }: any = []
            for (let i = 0; i < allProject.length; i++) {
                allProject[i].imgDetail = allProject[i].imgDetail.replace('{"', '').replace('"}', '').split('","')
                // allProject[i].imgDetail = `http://${req.rawHeaders[1]}/${allProject[i].imgDetail}`
                // allProject[i].imgThumb = `http://${req.rawHeaders[1]}/${allProject[i].imgThumb}`
                const projUser = await this.projectService.getProjUserByID(allProject[i].user_id);
                // projUser.avator = `http://${req.rawHeaders[1]}/${projUser.avator}`
                projUsers.push(projUser);
                projComment = await this.projCommentService.getProjComment(allProject[i].id);
                projComments.push(projComment);

                // console.log(projCommentUser, 'projCommentUser')
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    projVote = await this.projVoteService.getProjVoteByUser(allProject[i].id, payload.id);
                    if (projVote.length < 1) {
                        projVote = [{ voted: false }]
                    }
                } else {
                    projVote = [{ voted: false }]
                }
                projVoteCount = await this.projVoteService.getProjVoteCount(allProject[i].id);
                await this.projectService.updateProjVote(allProject[i].id, Number(projVoteCount[0].count))
                allProject[i].vote = projVoteCount[0].count
                projVotes.push(projVote);

                projStatComment = await this.projectService.getStatProjComment(allProject[i].id);
                projStatCommentReply = await this.projectService.getStatProjCommentReply(allProject[i].id);
                commentCountNum = Number(projStatComment[0].count) + Number(projStatCommentReply[0].count)
                commentCountNums.push(commentCountNum)
                let projCommentUsers: any = [];
                for (let j = 0; j < projComment.length; j++) {
                    let projCommentUsersReplys: any = [];
                    projCommentReply = await this.projCommentReplyService.getProjCommentReply(allProject[i].id, projComment[j].id);
                    projCommentReplys.push(projCommentReply)
                    projCommentUser = await this.projCommentService.getProjCommentUserByID(projComment[j].user_id);
                    // projCommentUser.avator = `http://${req.rawHeaders[1]}/${projCommentUser.avator}`
                    projCommentUsers.push(projCommentUser);
                    for (let k = 0; k < projCommentReply.length; k++) {
                        projCommentReplyUser = await this.projCommentReplyService.getProjCommentReplyUserByID(projCommentReply[k].user_id);
                        // projCommentReplyUser.avator = `http://${req.rawHeaders[1]}/${projCommentReplyUser.avator}`
                        projCommentUsersReplys.push(projCommentReplyUser);
                    }
                    allProjCommentUsersReplys.push(projCommentUsersReplys);
                }
                allProjCommentUsers.push(projCommentUsers)
            }

            const commentCount = { commentCountNums: commentCountNums }
            const allProjUsers = { projUsers: projUsers }
            const allProjCommentsUsers = { allProjCommentUsers: allProjCommentUsers }
            const allProjCommentReplysUsers = { allProjCommentUsersReplys: allProjCommentUsersReplys }
            // console.log(allProject, 'allProject')
            res.json({ allProject, projComments, projVotes, projCommentReplys, allProjUsers, allProjCommentsUsers, allProjCommentReplysUsers, commentCount })

        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }
    getProjectByTag = async (req: Request, res: Response) => {
        try {
            const { token, tag } = req.body;
            const allProject = await this.projectService.getProjectByTag(tag);
            let projComments: any = [];
            let projVotes: any = [];
            let projUsers: any = [];
            let projCommentReplys: any = [];
            let allProjCommentUsersReplys: any = [];
            let allProjCommentUsers: any = [];
            let commentCountNums: any = [];
            let { projComment, projVote, projCommentReply, projCommentUser, projCommentReplyUser, projVoteCount, projStatComment, projStatCommentReply, commentCountNum }: any = []
            for (let i = 0; i < allProject.length; i++) {
                allProject[i].imgDetail = allProject[i].imgDetail.replace('{"', '').replace('"}', '').split('","')
                // allProject[i].imgDetail = `http://${req.rawHeaders[1]}/${allProject[i].imgDetail}`
                // allProject[i].imgThumb = `http://${req.rawHeaders[1]}/${allProject[i].imgThumb}`
                const projUser = await this.projectService.getProjUserByID(allProject[i].user_id);
                // projUser.avator = `http://${req.rawHeaders[1]}/${projUser.avator}`
                projUsers.push(projUser);
                projComment = await this.projCommentService.getProjComment(allProject[i].id);
                projComments.push(projComment);
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    projVote = await this.projVoteService.getProjVoteByUser(allProject[i].id, payload.id);
                    if (projVote.length < 1) {
                        projVote = [{ voted: false }]
                    }
                } else {
                    projVote = [{ voted: false }]
                }
                projVoteCount = await this.projVoteService.getProjVoteCount(allProject[i].id);
                await this.projectService.updateProjVote(allProject[i].id, Number(projVoteCount[0].count))
                allProject[i].vote = projVoteCount[0].count
                projVotes.push(projVote);

                projStatComment = await this.projectService.getStatProjComment(allProject[i].id);
                projStatCommentReply = await this.projectService.getStatProjCommentReply(allProject[i].id);
                commentCountNum = Number(projStatComment[0].count) + Number(projStatCommentReply[0].count)
                commentCountNums.push(commentCountNum)
                let projCommentUsers: any = [];
                for (let j = 0; j < projComment.length; j++) {
                    let projCommentUsersReplys: any = [];
                    projCommentReply = await this.projCommentReplyService.getProjCommentReply(allProject[i].id, projComment[j].id);
                    projCommentReplys.push(projCommentReply)
                    projCommentUser = await this.projCommentService.getProjCommentUserByID(projComment[j].user_id);
                    // projCommentUser.avator = `http://${req.rawHeaders[1]}/${projCommentUser.avator}`
                    projCommentUsers.push(projCommentUser);
                    for (let k = 0; k < projCommentReply.length; k++) {
                        projCommentReplyUser = await this.projCommentReplyService.getProjCommentReplyUserByID(projCommentReply[k].user_id);
                        // projCommentReplyUser.avator = `http://${req.rawHeaders[1]}/${projCommentReplyUser.avator}`
                        projCommentUsersReplys.push(projCommentReplyUser);
                    }
                    allProjCommentUsersReplys.push(projCommentUsersReplys);
                }
                allProjCommentUsers.push(projCommentUsers)
            }

            const commentCount = { commentCountNums: commentCountNums }
            const allProjUsers = { projUsers: projUsers }
            const allProjCommentsUsers = { allProjCommentUsers: allProjCommentUsers }
            const allProjCommentReplysUsers = { allProjCommentUsersReplys: allProjCommentUsersReplys }
            // console.log(allProject, 'allProject')
            res.json({ allProject, projComments, projVotes, projCommentReplys, allProjUsers, allProjCommentsUsers, allProjCommentReplysUsers, commentCount })

        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getAllProject = async (req: Request, res: Response) => {
        try {
            const { token } = req.body;
            let allProject = await this.projectService.getAllProject();
            let projComments: any = [];
            let projVotes: any = [];
            let projUsers: any = [];
            let projCommentReplys: any = [];
            let allProjCommentUsersReplys: any = [];
            let allProjCommentUsers: any = [];
            let commentCountNums: any = [];
            let { projComment, projVote, projCommentReply, projCommentUser, projCommentReplyUser, projVoteCount, projStatComment, projStatCommentReply, commentCountNum }: any = []
            for (let i = 0; i < allProject.length; i++) {
                allProject[i].imgDetail = allProject[i].imgDetail.replace('{"', '').replace('"}', '').split('","')
                // allProject[i].imgDetail = `http://${req.rawHeaders[1]}/${allProject[i].imgDetail}`
                // allProject[i].imgThumb = `http://${req.rawHeaders[1]}/${allProject[i].imgThumb}`
                const projUser = await this.projectService.getProjUserByID(allProject[i].user_id);
                // projUser.avator = `http://${req.rawHeaders[1]}/${projUser.avator}`
                projUsers.push(projUser);
                projComment = await this.projCommentService.getProjComment(allProject[i].id);
                projComments.push(projComment);
                // console.log(projCommentUser, 'projCommentUser')
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    projVote = await this.projVoteService.getProjVoteByUser(allProject[i].id, payload.id);
                    if (projVote.length < 1) {
                        projVote = [{ voted: false }]
                    }
                } else {
                    projVote = [{ voted: false }]
                }
                projVotes.push(projVote);
                projVoteCount = await this.projVoteService.getProjVoteCount(allProject[i].id);
                await this.projectService.updateProjVote(allProject[i].id, Number(projVoteCount[0].count))
                allProject[i].vote = projVoteCount[0].count

                projStatComment = await this.projectService.getStatProjComment(allProject[i].id);
                projStatCommentReply = await this.projectService.getStatProjCommentReply(allProject[i].id);
                commentCountNum = Number(projStatComment[0].count) + Number(projStatCommentReply[0].count)
                commentCountNums.push(commentCountNum)
                let projCommentUsers: any = [];
                for (let j = 0; j < projComment.length; j++) {
                    let projCommentUsersReplys: any = [];
                    projCommentReply = await this.projCommentReplyService.getProjCommentReply(allProject[i].id, projComment[j].id);
                    projCommentReplys.push(projCommentReply)
                    projCommentUser = await this.projCommentService.getProjCommentUserByID(projComment[j].user_id);
                    // projCommentUser.avator = `http://${req.rawHeaders[1]}/${projCommentUser.avator}`
                    projCommentUsers.push(projCommentUser);
                    for (let k = 0; k < projCommentReply.length; k++) {
                        projCommentReplyUser = await this.projCommentReplyService.getProjCommentReplyUserByID(projCommentReply[k].user_id);
                        // projCommentReplyUser.avator = `http://${req.rawHeaders[1]}/${projCommentReplyUser.avator}`
                        projCommentUsersReplys.push(projCommentReplyUser);
                    }
                    allProjCommentUsersReplys.push(projCommentUsersReplys);
                }
                allProjCommentUsers.push(projCommentUsers)
            }

            // feature
            const featAllProject = await this.projectService.getProjSortByVotes();
            let featProjComments: any = [];
            let featProjVotes: any = [];
            let featProjUsers: any = [];
            let featProjCommentReplys: any = [];
            let featAllProjCommentUsersReplys: any = [];
            let featAllProjCommentUsers: any = [];
            let featCommentCountNums: any = [];
            let { featProjComment, featProjVote, featProjCommentReply, featProjCommentUser, featProjCommentReplyUser, featProjVoteCount, featProjStatComment, featProjStatCommentReply, featCommentCountNum }: any = []
            for (let i = 0; i < featAllProject.length; i++) {
                featAllProject[i].imgDetail = featAllProject[i].imgDetail.replace('{"', '').replace('"}', '').split('","')
                // featAllProject[i].imgDetail = `http://${req.rawHeaders[1]}/${featAllProject[i].imgDetail}`
                // featAllProject[i].imgThumb = `http://${req.rawHeaders[1]}/${featAllProject[i].imgThumb}`
                const projUser = await this.projectService.getProjUserByID(featAllProject[i].user_id);
                // projUser.avator = `http://${req.rawHeaders[1]}/${projUser.avator}`
                featProjUsers.push(projUser);
                featProjComment = await this.projCommentService.getProjComment(featAllProject[i].id);
                featProjComments.push(featProjComment);

                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    featProjVote = await this.projVoteService.getProjVoteByUser(featAllProject[i].id, payload.id);
                    if (featProjVote.length < 1) {
                        featProjVote = [{ voted: false }]
                    }
                } else {
                    featProjVote = [{ voted: false }]
                }
                featProjVotes.push(featProjVote);
                featProjVoteCount = await this.projVoteService.getProjVoteCount(featAllProject[i].id);
                await this.projectService.updateProjVote(featAllProject[i].id, Number(featProjVoteCount[0].count))
                featAllProject[i].vote = featProjVoteCount[0].count

                featProjStatComment = await this.projectService.getStatProjComment(featAllProject[i].id);
                featProjStatCommentReply = await this.projectService.getStatProjCommentReply(featAllProject[i].id);
                featCommentCountNum = Number(featProjStatComment[0].count) + Number(featProjStatCommentReply[0].count)
                featCommentCountNums.push(featCommentCountNum)
                let featProjCommentUsers: any = [];
                for (let j = 0; j < featProjComment.length; j++) {
                    let projCommentUsersReplys: any = [];
                    featProjCommentReply = await this.projCommentReplyService.getProjCommentReply(featAllProject[i].id, featProjComment[j].id);
                    featProjCommentReplys.push(featProjCommentReply)
                    featProjCommentUser = await this.projCommentService.getProjCommentUserByID(featProjComment[j].user_id);
                    // featProjCommentUser.avator = `http://${req.rawHeaders[1]}/${featProjCommentUser.avator}`
                    featProjCommentUsers.push(featProjCommentUser);
                    for (let k = 0; k < featProjCommentReply.length; k++) {
                        featProjCommentReplyUser = await this.projCommentReplyService.getProjCommentReplyUserByID(featProjCommentReply[k].user_id);
                        // featProjCommentReplyUser.avator = `http://${req.rawHeaders[1]}/${featProjCommentReplyUser.avator}`
                        projCommentUsersReplys.push(featProjCommentReplyUser);
                    }
                    featAllProjCommentUsersReplys.push(projCommentUsersReplys);
                }
                featAllProjCommentUsers.push(featProjCommentUsers)
            }

            const commentCount = { commentCountNums: commentCountNums }
            const allProjUsers = { projUsers: projUsers }
            const allProjCommentsUsers = { allProjCommentUsers: allProjCommentUsers }
            const allProjCommentReplysUsers = { allProjCommentUsersReplys: allProjCommentUsersReplys }

            // feature 
            const featCommentCount = { featCommentCountNums: featCommentCountNums }
            const featAllProjUsers = { featProjUsers: featProjUsers }
            const featAllProjCommentsUsers = { featAllProjCommentUsers: featAllProjCommentUsers }
            const featAllProjCommentReplysUsers = { featAllProjCommentUsersReplys: featAllProjCommentUsersReplys }

            // console.log(projComments, 'projComments')
            res.json({ allProject, projComments, projVotes, projCommentReplys, allProjUsers, allProjCommentsUsers, allProjCommentReplysUsers, commentCount, featAllProject, featProjComments, featProjVotes, featProjCommentReplys, featAllProjUsers, featAllProjCommentsUsers, featAllProjCommentReplysUsers, featCommentCount })

        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }
};