import { ProjVoteService, DiscusVoteService, ArtVoteService } from "../services/VoteService"
import { Request, Response } from "express"
import express from "express";
import { logger } from "../utils/logger"
import jwt from "../jwt";
import jwtSimple from "jwt-simple";

export class ProjVoteController {
    constructor(private projVoteService: ProjVoteService) { }

    updateProjVote = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            let { token, proj_id, voted } = req.body
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const votes = await this.projVoteService.getProjVoteByUser(proj_id, payload.id)
            if (votes.length < 1) {
                await this.projVoteService.createProjVote(
                    payload.id,
                    proj_id
                );
            } else {
                await this.projVoteService.updateProjVote(payload.id, proj_id, voted);
            }
            res.json({ msg: "Update Successful" });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };

};

export class DiscusVoteController {
    constructor(private discusVoteService: DiscusVoteService) { }

    updateDiscusVote = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            let { token, discus_id, voted } = req.body
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const votes = await this.discusVoteService.getDiscusVoteByUser(discus_id, payload.id)
            if (votes.length < 1) {
                await this.discusVoteService.createDiscusVote(
                    payload.id,
                    discus_id
                );
            } else {
                await this.discusVoteService.updateDiscusVote(payload.id, discus_id, voted);
            }
            res.json({ msg: "Update Successful" });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };
};

export class ArtVoteController {
    constructor(private artVoteService: ArtVoteService) { }

    updateArtVote = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            let { token, art_id, voted } = req.body
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const votes = await this.artVoteService.getArtVoteByUser(art_id, payload.id)
            if (votes.length < 1) {
                await this.artVoteService.createArtVote(
                    payload.id,
                    art_id
                );
            } else {
                await this.artVoteService.updateArtVote(payload.id, art_id, voted);
            }
            res.json({ msg: "Update Successful" });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };
};