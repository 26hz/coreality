import { ProjCommentReplyService, DiscusCommentReplyService } from "../services/CommentReplyService"
import { Request, Response } from "express"
import { logger } from "../utils/logger"
import jwt from "../jwt";
import jwtSimple from "jwt-simple";

export class ProjCommentReplyController {
    constructor(private projCommentReplyService: ProjCommentReplyService) { }

    createProjCommentReply = async (req: Request, res: Response) => {
        try {
            if (!req.body.description) {
                res.status(401).json({ msg: "No INPUT COMMENT" })
                return;
            }
            const { token, proj_id, comment_id, description } = req.body;
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user_id = payload.id
            await this.projCommentReplyService.createProjCommentReply(
                user_id,
                proj_id,
                comment_id,
                description
            );
            res.json({ msg: "Project Comment Successfully Created" })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }
};

export class DiscusCommentReplyController {
    constructor(private discusCommentReplyService: DiscusCommentReplyService) { }

    createDiscusCommentReply = async (req: Request, res: Response) => {
        try {
            if (!req.body.description) {
                res.status(401).json({ msg: "No INPUT COMMENT" })
                return;
            }
            const { token, discus_id, comment_id, description } = req.body;
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user_id = payload.id
            await this.discusCommentReplyService.createDiscusCommentReply(
                user_id,
                discus_id,
                comment_id,
                description
            );
            res.json({ msg: "Discussion Comment Successfully Created" })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

};