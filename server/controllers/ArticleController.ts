import { ArticleService } from "../services/ArticleService"
import { Request, Response } from "express"
import { logger } from "../utils/logger"
import jwt from "../jwt";
import jwtSimple from "jwt-simple";
import { ArtVoteService } from "../services/VoteService";

export class ArticleController {
    constructor(private articleService: ArticleService, private artVoteService: ArtVoteService) { }

    createArticle = async (req: Request, res: Response) => {
        try {
            if (!req.body.title || !req.body.description || !req.body.content) {
                res.status(401).json({ msg: "Missing Required Input" })
                return;
            }
            const imgThumb = `http://${req.hostname}:8080/${req.file?.filename}`
            // const imgThumb = `http://${req.rawHeaders[1]}/${req.file?.filename}`
            let { token, title, description, content, tags } = req.body;
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user_id = payload.id
            await this.articleService.createArticle(
                user_id,
                title,
                description,
                content,
                tags,
                imgThumb
            );
            res.json({ msg: "Article Successfully Created" })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getArticle = async (req: Request, res: Response) => {
        try {
            const { token, username } = req.body;
            //[Code Revew] SQL joins
            const user = await this.articleService.getUserByUsername(username)
            const allArticle = await this.articleService.getArticle(user.id);
            let artVotes: any = [];
            let artUsers: any = [];
            let { artVote, artVoteCount }: any = [] //[Code Review] wrong data type
            for (let i = 0; i < allArticle.length; i++) {
                // allArticle[i].imgThumb = `http://${req.rawHeaders[1]}/${allArticle[i].imgThumb}`
                const artUser = await this.articleService.getArtUserByID(allArticle[i].user_id);
                // artUser.avator = `http://${req.rawHeaders[1]}/${artUser.avator}`
                artUsers.push(artUser);

                //[Code Review] logic put to frontend
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    artVote = await this.artVoteService.getArtVoteByUser(allArticle[i].id, payload.id);
                    if (artVote.length < 1) {
                        artVote = [{ voted: false }]
                    }
                } else {
                    artVote = await this.artVoteService.getArtVote(allArticle[i].id);
                    if (artVote[0].voted == true) {
                        artVote = [{ voted: false }]
                    }
                }
                artVotes.push(artVote);
                artVoteCount = await this.artVoteService.getArtVoteCount(allArticle[i].id);
                //[Code Review] no need update
                await this.articleService.updateArtVote(allArticle[i].id, Number(artVoteCount[0].count))
                allArticle[i].vote = artVoteCount[0].count

            }
            const allArtUsers = { artUsers: artUsers }
            res.json({ allArticle, artVotes, allArtUsers })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getArticleByTag = async (req: Request, res: Response) => {
        try {
            const { token, tag } = req.body;
            const allArticle = await this.articleService.getArticleByTag(tag);
            let artVotes: any = [];
            let artUsers: any = [];
            let { artVote, artVoteCount }: any = []
            for (let i = 0; i < allArticle.length; i++) {
                // allArticle[i].imgThumb = `http://${req.rawHeaders[1]}/${allArticle[i].imgThumb}`
                const artUser = await this.articleService.getArtUserByID(allArticle[i].user_id);
                // artUser.avator = `http://${req.rawHeaders[1]}/${artUser.avator}`
                artUsers.push(artUser);

                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    artVote = await this.artVoteService.getArtVoteByUser(allArticle[i].id, payload.id);
                    if (artVote.length < 1) {
                        artVote = [{ voted: false }]
                    }
                } else {
                    artVote = await this.artVoteService.getArtVote(allArticle[i].id);
                    if (artVote[0].voted == true) {
                        artVote = [{ voted: false }]
                    }
                }
                artVotes.push(artVote);
                artVoteCount = await this.artVoteService.getArtVoteCount(allArticle[i].id);
                await this.articleService.updateArtVote(allArticle[i].id, Number(artVoteCount[0].count))
                allArticle[i].vote = artVoteCount[0].count

            }
            const allArtUsers = { artUsers: artUsers }
            res.json({ allArticle, artVotes, allArtUsers })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getAllArticle = async (req: Request, res: Response) => {
        try {
            const { token } = req.body;
            let artVotes: any = [];
            let artUsers: any = [];
            let { artVote, artVoteCount }: any = []
            const allArticle = await this.articleService.getAllArticle();
            for (let i = 0; i < allArticle.length; i++) {
                // allArticle[i].imgThumb = `http://${req.rawHeaders[1]}/${allArticle[i].imgThumb}`
                const artUser = await this.articleService.getArtUserByID(allArticle[i].user_id);
                // artUser.avator = `http://${req.rawHeaders[1]}/${artUser.avator}`
                artUsers.push(artUser);
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    artVote = await this.artVoteService.getArtVoteByUser(allArticle[i].id, payload.id);
                    if (artVote.length < 1) {
                        artVote = [{ voted: false }]
                    }
                } else {
                    artVote = await this.artVoteService.getArtVote(allArticle[i].id);
                    if (artVote[0].voted == true) {
                        artVote = [{ voted: false }]
                    }
                }
                artVotes.push(artVote);
                artVoteCount = await this.artVoteService.getArtVoteCount(allArticle[i].id);
                await this.articleService.updateArtVote(allArticle[i].id, Number(artVoteCount[0].count))
                allArticle[i].vote = artVoteCount[0].count
            }

            // feature
            let featArtVotes: any = [];
            let featArtUsers: any = [];
            let { featArtVote, featArtVoteCount }: any = []
            const featAllArticle = await this.articleService.getArtSortByVotes();
            for (let i = 0; i < featAllArticle.length; i++) {
                // featAllArticle[i].imgThumb = `http://${req.rawHeaders[1]}/${allArticle[i].imgThumb}`
                const featArtUser = await this.articleService.getArtUserByID(featAllArticle[i].user_id);
                // featArtUser.avator = `http://${req.rawHeaders[1]}/${featArtUser.avator}`
                featArtUsers.push(featArtUser);
                if (token) {
                    const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
                    featArtVote = await this.artVoteService.getArtVoteByUser(featAllArticle[i].id, payload.id);
                    if (featArtVote.length < 1) {
                        featArtVote = [{ voted: false }]
                    }
                } else {
                    featArtVote = await this.artVoteService.getArtVote(featAllArticle[i].id);
                    if (featArtVote[0].voted == true) {
                        featArtVote = [{ voted: false }]
                    }
                }
                featArtVotes.push(artVote);
                featArtVoteCount = await this.artVoteService.getArtVoteCount(featAllArticle[i].id);
                await this.articleService.updateArtVote(featAllArticle[i].id, Number(featArtVoteCount[0].count))
                featAllArticle[i].vote = featArtVoteCount[0].count
            }
            const featAllArtUsers = { featArtUsers: featArtUsers }
            const allArtUsers = { artUsers: artUsers }
            res.json({ allArticle, artVotes, allArtUsers, featAllArticle, featArtVotes, featAllArtUsers })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }
};