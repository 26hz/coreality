import { UserService } from "../services/UserServices"
import { Request, Response } from "express"
import { checkPassword, hashPassword } from "../hash";
import jwt from "../jwt";
import jwtSimple from "jwt-simple";
import express from "express";
import fetch from "node-fetch";
import { logger } from "../utils/logger"
import { ArtVoteService, DiscusVoteService, ProjVoteService } from "../services/VoteService";
import { ProjectService } from "../services/ProjectService";
import { DiscussionService } from "../services/DiscussionService";
import { ArticleService } from "../services/ArticleService";

function removeDuplicates(data: any, key: any) {

    return [
        ...new Map(data.map((item: any) => [key(item), item])).values()
    ]

};

export class UserController {
    constructor(private userService: UserService, private projVoteService: ProjVoteService, private discusVoteService: DiscusVoteService, private artVoteService: ArtVoteService, private projectService: ProjectService, private discussionService: DiscussionService, private articleService: ArticleService) { }

    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.email || !req.body.password) {
                res.status(401).json({ msg: "Missing Email / Password" })
                return;
            }
            const { email, password } = req.body;
            const user = await this.userService.getUserByEmail(email);
            if (!user || !(await checkPassword(password, user.password))) {
                res.status(401).json({ msg: "Wrong Email / Password" })
                return;
            }
            const payload = {
                id: user.id,
                email: user.email
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token,
            })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    loginFacebook = async (req: express.Request, res: express.Response) => {
        try {
            if (!req.body.accessToken) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }
            const { accessToken } = req.body;

            const fetchResponse = await fetch(
                `https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`
            );
            const result = await fetchResponse.json();
            if (result.error) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }
            let user = await this.userService.getUserByEmail(result.email);
            if (!user) {
                res.status(401).json({ msg: "Please signup an account" });
                return;
            }
            const payload = {
                id: user.id,
                email: user.email,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            res.json({
                token: token,
            });
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" });
        }
    };

    loginGoogle = async (req: express.Request, res: express.Response) => {
        try {

            if (!req.body.accessToken) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }
            const { accessToken } = req.body;

            const fetchResponse = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
                method: "get",
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            const result = await fetchResponse.json();
            if (result.error) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }
            let user = await this.userService.getUserByEmail(result.email);
            if (!user) {
                res.status(402).json({ msg: "Please signup an account" });
                return;
            }
            const payload = {
                id: user.id,
                email: user.email,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            res.json({
                token: token,
            });
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" });
        }
    };

    signup = async (req: Request, res: Response) => {
        try {
            if (!req.body.email || !req.body.password || !req.body.username) {
                res.status(401).json({ msg: "Missing Email / Password / Username" })
                return;
            }
            let { email, username, password, displayname, role, exp, skilltag } = req.body;
            let user = await this.userService.getUserByEmail(email);
            let getUsername = await this.userService.getUserByUsername(username);
            if (!user) {
                password = await hashPassword(password);
                user = await this.userService.createUser(
                    email,
                    username,
                    password,
                    displayname,
                    role,
                    exp,
                    skilltag,
                );

                const payload = {
                    id: user.id,
                    email: user.email
                };
                const token = jwtSimple.encode(payload, jwt.jwtSecret);
                res.json({
                    token: token,
                })

            } else if (!getUsername) {
                res.status(401).json({ msg: "Username Exsist" })
                return;
            } else {
                res.status(401).json({ msg: "Email Exist" })
                return;
            }

        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

    getUserInfoByID = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            const { token } = req.body
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user = await this.userService.getUserByID(payload.id);
            if (!user) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const { password, ...others } = user;

            res.json({ ...others });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };

    updateUserProfile = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            let { token, username, displayname, role, headline, bio, website, facebook, twitter, linkedin, skilltag, exp } = req.body
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            facebook = facebook.includes("https://") ? facebook : "https://" + facebook
            twitter = twitter.includes("https://") ? twitter : "https://" + twitter
            linkedin = linkedin.includes("https://") ? linkedin : "https://" + linkedin
            website = website.includes("https://") ? website : "https://" + website
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user = await this.userService.updateUserProfile(payload.id, username, displayname, role, headline, bio, website, facebook, twitter, linkedin, skilltag, exp);
            if (!user) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            console.log(user)
            res.json({ msg: "Update Successful" });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };

    updateAvator = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            // need to change before deploy
            const avator = `http://${req.hostname}/${req.file?.filename}`
            // const avator = req.file?.filename
            const { token } = req.body
            if (!avator) {
                return res.status(401).json({ msg: "no image detected" })
            }
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);

            const user = await this.userService.updateAvator(payload.id, avator);
            // user.avator = `http://${req.rawHeaders[1]}/${user.avator}`
            if (!token || !user) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            res.json({ msg: " Successfully uploaded" })
            return next();
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }


    updateEmail = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            const { token, email } = req.body
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user = await this.userService.updateEmail(payload.id, email);
            if (!user) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            res.json({ msg: "Update Successful" });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };
    updatePassword = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            let { token, oldPassword, newPassword } = req.body
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            let user = await this.userService.getUserByID(payload.id);
            if (!(await checkPassword(oldPassword, user.password))) {
                res.status(401).json({ msg: "Wrong password" })
                return;
            }
            newPassword = await hashPassword(newPassword);
            const userPassword = await this.userService.updatePassword(payload.id, newPassword);
            if (!userPassword) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            res.json({ msg: "Update Successful" });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };

    getUserInfoByVotes = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            const userByVote = await this.userService.getUserSortByVotes();
            let userByVotes: any[] = []
            for (let i = 0; i < userByVote.length; i++) {
                let { password, email, role, exp, skilltag, bio, headline, googlelogin, facebooklogin, facebook, twitter, linkedin, github, website, totalVotes, created_at, updated_at, ...others } = userByVote[i];
                userByVotes.push({ ...others })
            }
            // console.log(userByVotes)
            res.json(userByVotes);
            return next();
        } catch (err) {
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };

    getTagsbyCount = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            const allProject = await this.projectService.getAllProject();
            const allTags: any = []
            const allProjTags: any = []
            for (let i = 0; i < allProject.length; i++) {
                for (let a = 0; a < allProject[i].tags.length; a++) {
                    const tagCount = await this.projectService.getProjectTagCount(allProject[i].tags[a])
                    const projTag = { tagName: allProject[i].tags[a], tagCountNum: Number(tagCount[0].count) }
                    allProjTags.push(projTag)
                }
            }
            const allProjTagsClean = removeDuplicates(allProjTags, (item: any) => item.tagName);

            const allDiscussion = await this.discussionService.getAllDiscussion();
            const allDiscusTags: any = []
            for (let i = 0; i < allDiscussion.length; i++) {
                for (let a = 0; a < allDiscussion[i].tags.length; a++) {
                    const tagCount = await this.discussionService.getDiscusTagCount(allDiscussion[i].tags[a])
                    const discusTag = { tagName: allDiscussion[i].tags[a], tagCountNum: Number(tagCount[0].count) }
                    allDiscusTags.push(discusTag)
                }
            }
            const allDiscusTagsClean = removeDuplicates(allDiscusTags, (item: any) => item.tagName);

            const allArticle = await this.articleService.getAllArticle();
            const allArtTags: any = []
            for (let i = 0; i < allArticle.length; i++) {
                for (let a = 0; a < allArticle[i].tags.length; a++) {
                    const tagCount = await this.articleService.getArticleTagCount(allArticle[i].tags[a])
                    const artTag = { tagName: allArticle[i].tags[a], tagCountNum: Number(tagCount[0].count) }
                    allArtTags.push(artTag)
                }
            }
            const allArtTagsClean = removeDuplicates(allArtTags, (item: any) => item.tagName);

            const allUsers = await this.userService.getAllUsers();
            const allUsersTags: any = []
            for (let i = 0; i < allUsers.length; i++) {
                for (let a = 0; a < allUsers[i].skilltag.length; a++) {
                    const tagCount = await this.userService.getUserSkillTagCount(allUsers[i].skilltag[a])
                    const userTag = { tagName: allUsers[i].skilltag[a], tagCountNum: Number(tagCount[0].count) }
                    allUsersTags.push(userTag)
                }
            }
            const allUsersTagsClean = removeDuplicates(allUsersTags, (item: any) => item.tagName);
            allTags.push(allProjTagsClean, allDiscusTagsClean, allArtTagsClean, allUsersTagsClean)
            res.json(allTags);
            return next();
        } catch (err) {
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };

    getUserInfoByUsername = async (req: Request, res: Response, next: express.NextFunction) => {
        try {
            const { username } = req.body
            const user = await this.userService.getUserByUsername(username);
            if (!user) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            const id = user.id
            const userStatProj = await this.userService.getStatProj(id);
            const userStatArticles = await this.userService.getStatArticles(id);
            const userStatDiscus = await this.userService.getStatDiscus(id);
            const userStatProjVote = await this.projVoteService.getUserVoteCount(id);
            const userStatDiscusVote = await this.discusVoteService.getUserVoteCount(id);
            const userStatArtVote = await this.artVoteService.getUserVoteCount(id);
            const userStatProjComment = await this.userService.getStatProjComment(id);
            const userProjCommentReply = await this.userService.getStatProjCommentReply(id);
            const userDiscusComment = await this.userService.getStatDiscusComment(id);
            const userDiscusCommentReply = await this.userService.getStatDiscusCommentReply(id);
            const commentCount = Number(userProjCommentReply[0].count) + Number(userStatProjComment[0].count) + Number(userDiscusComment[0].count) + Number(userDiscusCommentReply[0].count)
            const projCount = Number(userStatProj[0].count)
            const artCount = Number(userStatArticles[0].count)
            const discusCount = Number(userStatDiscus[0].count)
            const voteCount = Number(userStatProjVote[0].count) + Number(userStatDiscusVote[0].count) + Number(userStatArtVote[0].count) + Number(userDiscusCommentReply[0].count)
            await this.userService.updateUserVote(id, voteCount)
            const { password, ...others } = user;
            res.json({ ...others, commentCount, projCount, artCount, discusCount, voteCount });
            return next();
        } catch (err) {
            // use logger
            logger.error(err.toString());
            res.status(500).json({ message: "internal server error" });
        }
    };
}


