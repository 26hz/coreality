import { ProjCommentService, DiscusCommentService } from "../services/CommentService"
import { Request, Response } from "express"
import { logger } from "../utils/logger"
import jwt from "../jwt";
import jwtSimple from "jwt-simple";

export class ProjCommentController {
    constructor(private projCommentService: ProjCommentService) { }

    createProjComment = async (req: Request, res: Response) => {
        try {
            if (!req.body.description) {
                res.status(401).json({ msg: "No INPUT COMMENT" })
                return;
            }
            const { token, proj_id, description } = req.body;
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user_id = payload.id
            // console.log(req.body)
            await this.projCommentService.createProjComment(
                user_id,
                proj_id,
                description
            );
            res.json({ msg: "Project Comment Successfully Created" })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

};

export class DiscusCommentController {
    constructor(private discusCommentService: DiscusCommentService) { }

    createDiscusComment = async (req: Request, res: Response) => {
        try {
            if (!req.body.description) {
                res.status(401).json({ msg: "No INPUT COMMENT" })
                return;
            }
            const { token, discus_id, description } = req.body;
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user_id = payload.id
            await this.discusCommentService.createDiscusComment(
                user_id,
                discus_id,
                description
            );
            res.json({ msg: "Discussion Comment Successfully Created" })
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({ msg: "internal server error" })
        }
    }

};