import Knex from "knex"
import { tables } from "../table"

export class ProjectService {
    constructor(private knex: Knex) { }

    async createProject(user_id: number, title: string, headline: string, status: boolean, description: string, tags: string, link: string, imgThumb: string, imgDetail: string, videoLink: string) {
        const [project] = await this.knex(tables.PROJECT)
            .join(tables.USERS, { user_id: user_id })
            .insert({ user_id, title, headline, status, description, tags, link, imgThumb, imgDetail, videoLink })
            .returning(["id"]);
        return project;
    }

    async getProject(id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJECT)
                .where("user_id", id)
        );
    }

    async getAllProject() {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJECT)
        );
    }

    async getProjUserByID(user_id: number) {
        const [project] = await this.knex(tables.USERS)
            .select("id", "displayname", "avator", "headline")
            .from(tables.USERS)
            .where("id", user_id)
        return project;
    }

    async updateProjVote(proj_id: number, vote: number) {
        return (
            await this.knex(tables.PROJECT)
                .where({ id: proj_id })
                .update({ vote: vote })
        );
    }
    async getStatProjComment(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJCOMMENT)
                .where("proj_id", id)
        );
    }

    async getStatProjCommentReply(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJCOMMENTREPLY)
                .where("proj_id", id)
        );
    }
    async joinProj_user(user_id: number) {
        const [user] = await this.knex(tables.PROJECT)
            .join(tables.USERS, 'users.id', 'user_id')
            .select(["user_id", "projects.id", "title", "projects.headline", "description", 'link', "imgThumb", "displayname", "avator", "projects.created_at"])
            .where({ user_id: user_id })
        return user;
    }
    async getProjSortByVotes() {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJECT)
                .orderBy('vote', 'desc')
                .limit(2)
        );
    }
    async getProjectTagCount(tag: string) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJECT)
                .whereRaw('? = ANY(tags) ', [tag])
        );
    }

    async getProjectByTag(tag: string) {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJECT)
                .whereRaw('? = ANY(tags) ', [tag])
        );
    }

    async getUserByUsername(username: string) {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
                .where("username", username)
        )[0];
    }
}

