import { tables } from "../table"
import { IUser } from "./models"
import Knex from "knex"

export class UserService {
    constructor(private knex: Knex) { }

    async getUserByID(id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
                .where("id", id)
        )[0];
    }

    async getUserByUsername(username: string) {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
                .where("username", username)
        )[0];
    }

    async getUserByEmail(email: string): Promise<IUser> {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
                .where("email", email)
        )[0];
    }

    async getAllUsers() {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
        );
    }

    async createUser(email: string, username: string, password: string, displayname: string, role: string, exp: number, skilltag: string) {
        const [user] = await this.knex(tables.USERS)
            .insert({ email, username, password, displayname, role, exp, skilltag })
            .returning(["id", "email"]);
        return user;
    }

    async updateUserProfile(id: number, username: string, displayname: string, role: string, headline: string, bio: string, website: string, facebook: string, twitter: string, linkedin: string, skilltag: string, exp: number) {
        return (
            await this.knex(tables.USERS)
                .where({ id: id })
                .update({ username: username, displayname: displayname, role: role, headline: headline, bio: bio, website: website, facebook: facebook, twitter: twitter, linkedin: linkedin, skilltag: skilltag, exp: exp })
        );
    };

    async updateAvator(id: number, avator: string) {
        return (
            await this.knex(tables.USERS)
                .where({ id: id })
                .update({ avator: avator })
        )
    };

    async updateEmail(id: number, email: string) {
        return (
            await this.knex(tables.USERS)
                .where({ id: id })
                .update({ email })
        );
    };

    async updatePassword(id: number, password: string) {
        return (
            await this.knex(tables.USERS)
                .where({ id: id })
                .update({ password })
        );
    };

    async getStatProj(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJECT)
                .where("user_id", id)
        );
    }

    async getStatProjComment(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJCOMMENT)
                .where("user_id", id)
        );
    }
    async getStatProjCommentReply(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJCOMMENTREPLY)
                .where("user_id", id)
        );
    }
    async getStatDiscusCommentReply(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSCOMMENT)
                .where("user_id", id)
        );
    }
    async getStatDiscusComment(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSCOMMENTREPLY)
                .where("user_id", id)
        );
    }
    async getStatDiscus(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSSION)
                .where("user_id", id)
        );
    }
    async getStatArticles(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.ARTICLE)
                .where("user_id", id)
        );
    }
    async updateUserVote(user_id: number, totalVotes: number) {
        return (
            await this.knex(tables.USERS)
                .where({ id: user_id })
                .update({ totalVotes: totalVotes })
        );
    }

    async getUserSortByVotes() {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
                .orderBy('totalVotes', 'desc')
                .limit(10)
        );
    }

    async getUserSkillTagCount(tag: string) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.USERS)
                .whereRaw('? = ANY(skilltag) ', [tag])
        );
    }
    async getUserSkillByTag(tag: string) {
        return (
            await this.knex
                .select("id", "displayname", "avator", "headline")
                .from(tables.USERS)
                .whereRaw('? = ANY(skilltag) ', [tag])
        );
    }
};