import Knex from "knex"
import { tables } from "../table"

export class ArticleService {
    constructor(private knex: Knex) { }

    async createArticle(user_id: number, title: string, description: string, content: string, tags: string, imgThumb: string) {
        const [user] = await this.knex(tables.ARTICLE)
            .join(tables.USERS, { user_id: user_id })
            .insert({ user_id, title, description, content, tags, imgThumb })
            .returning("id")
        return user;
    }

    async getArticle(id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.ARTICLE)
                .where("user_id", id)
        );
    }

    async getArtUserByID(user_id: number) {
        const [article] = await this.knex(tables.USERS)
            .select("id", "displayname", "avator", "headline")
            .from(tables.USERS)
            .where("id", user_id)
        return article;
    }

    async getAllArticle() {
        return (
            await this.knex
                .select("*")
                .from(tables.ARTICLE)
        );
    }
    async updateArtVote(art_id: number, vote: number) {
        return (
            await this.knex(tables.PROJECT)
                .where({ id: art_id })
                .update({ vote: vote })
        );
    }

    async joinArt_user(user_id: number) {
        const [user] = await this.knex(tables.ARTICLE)
            .join(tables.USERS, 'users.id', 'user_id')
            .select(["user_id", "articles.id", "title", "content", "description", "imgThumb", "displayname", "avator", "created_at"])
            .where({ user_id: user_id })
        return user;
    }

    async getArtSortByVotes() {
        return (
            await this.knex
                .select("*")
                .from(tables.ARTICLE)
                .orderBy('vote', 'desc')
                .limit(2)
        );
    }
    async getArticleTagCount(tag: string) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.ARTICLE)
                .whereRaw('? = ANY(tags) ', [tag])
        );
    }
    async getArticleByTag(tag: string) {
        return (
            await this.knex
                .select("*")
                .from(tables.ARTICLE)
                .whereRaw('? = ANY(tags) ', [tag])
        );
    }
    async getUserByUsername(username: string) {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
                .where("username", username)
        )[0];
    }
}
