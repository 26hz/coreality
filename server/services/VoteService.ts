import Knex from "knex"
import { tables } from "../table"

export class DiscusVoteService {
    constructor(private knex: Knex) { }

    async createDiscusVote(user_id: number, discus_id: number) {
        const [discusionVote] = await this.knex(tables.DISCUSVOTE)
            .join(tables.USERS, { user_id: user_id })
            .join(tables.DISCUSSION, { discus_id: discus_id })
            .insert({ user_id, discus_id })
        return discusionVote;
    }

    async updateDiscusVote(user_id: number, discus_id: number, voted: boolean) {
        return (
            await this.knex(tables.DISCUSVOTE)
                .where({ user_id: user_id, discus_id: discus_id })
                .update({ voted: voted })
        );
    }

    async getDiscusVote(discus_id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSVOTE)
                .where("discus_id", discus_id)
        );
    }
    async getDiscusVoteByUser(discus_id: number, id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSVOTE)
                .where({ discus_id: discus_id, user_id: id })
        );
    }

    async getAllDiscusVote() {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSVOTE)
        );
    }
    async getDiscusVoteCount(discus_id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSVOTE)
                .where({ discus_id: discus_id, voted: true })
        );
    }
    async getUserVoteCount(user_id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSVOTE)
                .where({ user_id: user_id, voted: true })
        );
    }


}

export class ProjVoteService {
    constructor(private knex: Knex) { }

    async createProjVote(user_id: number, proj_id: number) {
        const [project] = await this.knex(tables.PROJVOTE)
            .join(tables.USERS, { user_id: user_id })
            .join(tables.PROJECT, { proj_id: proj_id })
            .insert({ user_id, proj_id })
        return project;
    }

    async updateProjVote(user_id: number, proj_id: number, voted: boolean) {
        return (
            await this.knex(tables.PROJVOTE)
                .where({ user_id: user_id, proj_id: proj_id })
                .update({ voted: voted })
        );
    }
    async getProjVoteByUser(proj_id: number, id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJVOTE)
                .where({ proj_id: proj_id, user_id: id })
        );
    }
    async getProjVote(proj_id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJVOTE)
                .where({ proj_id: proj_id })
        );
    }
    async getAllProjVote() {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJVOTE)
        );
    }
    async getProjVoteCount(proj_id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJVOTE)
                .where({ proj_id: proj_id, voted: true })
        );
    }
    async getUserVoteCount(user_id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.PROJVOTE)
                .where({ user_id: user_id, voted: true })
        );
    }

}

export class ArtVoteService {
    constructor(private knex: Knex) { }

    async createArtVote(user_id: number, art_id: number) {
        const [user] = await this.knex(tables.ARTVOTE)
            .join(tables.USERS, { user_id: user_id })
            .join(tables.ARTICLE, { art_id: art_id })
            .insert({ user_id, art_id })
        return user;
    }

    async updateArtVote(user_id: number, art_id: number, voted: boolean) {
        return (
            await this.knex(tables.ARTVOTE)
                .where({ user_id: user_id, art_id: art_id })
                .update({ voted: voted })
        );
    }
    async getArtVoteByUser(art_id: number, id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.ARTVOTE)
                .where({ art_id: art_id, user_id: id })
        );
    }

    async getArtVote(art_id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.ARTVOTE)
                .where("art_id", art_id)
        );
    }
    async getAllArtVote() {
        return (
            await this.knex
                .select("*")
                .from(tables.ARTVOTE)
        );
    }
    async getArtVoteCount(art_id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.ARTVOTE)
                .where({ art_id: art_id, voted: true })
        );
    }
    async getUserVoteCount(user_id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.ARTVOTE)
                .where({ user_id: user_id, voted: true })
        );
    }
}
