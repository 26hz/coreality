import Knex from "knex"
import { tables } from "../table"

export class ProjCommentReplyService {
    constructor(private knex: Knex) { }

    async createProjCommentReply(user_id: number, proj_id: number, comment_id: number, description: string) {
        const [user] = await this.knex(tables.PROJCOMMENTREPLY)
            .join(tables.USERS, { user_id: user_id })
            .join(tables.PROJECT, { proj_id: proj_id })
            .join(tables.PROJCOMMENT, { comment_id: comment_id })
            .insert({ user_id, proj_id, comment_id, description })
        return user;
    }
    async getProjCommentReply(proj_id: number, comment_id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJCOMMENTREPLY)
                .where({ proj_id: proj_id, comment_id: comment_id })
        );
    }
    async getProjCommentReplyUserByID(user_id: number) {
        const [projectCommentReply] = await this.knex(tables.USERS)
            .select("id", "username", "displayname", "avator")
            .from(tables.USERS)
            .where("id", user_id)
        return projectCommentReply
    }


    async joinProjCommentReply_user(user_id: number) {
        const [user] = await this.knex(tables.PROJCOMMENTREPLY)
            .join(tables.USERS, 'users.id', 'user_id')
            .select(["user_id", "proj_id", "comment_id", "description", "displayname", "avator", "projCommentsReply.created_at"])
            .where({ user_id: user_id })
        return user;
    }
}

export class DiscusCommentReplyService {
    constructor(private knex: Knex) { }

    async createDiscusCommentReply(user_id: number, discus_id: number, comment_id: number, description: string) {
        const [user] = await this.knex(tables.DISCUSCOMMENTREPLY)
            .join(tables.USERS, { user_id: user_id })
            .join(tables.DISCUSSION, { discus_id: discus_id })
            .join(tables.DISCUSCOMMENT, { comment_id: comment_id })
            .insert({ user_id, discus_id, comment_id, description })
        return user;
    }
    async getDiscusCommentReply(discus_id: number, comment_id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSCOMMENTREPLY)
                .where({ discus_id: discus_id, comment_id: comment_id })
        );
    }

    async getDiscusCommentReplyUserByID(user_id: number) {
        const [discusCommentReply] = await this.knex(tables.USERS)
            .select("id", "username", "displayname", "avator")
            .from(tables.USERS)
            .where("id", user_id)
        return discusCommentReply
    }

    async joinDiscusCommentReply_user(user_id: number) {
        const [user] = await this.knex(tables.DISCUSCOMMENTREPLY)
            .join(tables.USERS, 'users.id', 'user_id')
            .select(["user_id", "discus_id", "comment_id", "description", "displayname", "avator", "comments", "discusCommentsReply.created_at"])
            .where({ user_id: user_id })
        return user;
    }
}


