export interface IUser {
    id: number;
    email: string;
    password: string;
}
export interface IProj {
    id: number;
    user_id: number;
    title: string;
    header: string;
    status: boolean;
    description: string;
    link?: string;
    vote?: number;
    imgThumb?: string;
    imgDetail?: string;
    videoLink?: string;
    comment?: string;
}
declare global {
    namespace Express {
        interface Request {
            user?: {
                id: number;
                email: string;
            }
        }
    }
}