import Knex from "knex"
import { tables } from "../table"

export class DiscussionService {
    constructor(private knex: Knex) { }

    async createDiscussion(user_id: number, title: string, description: string, tags: string) {
        const [discussion] = await this.knex(tables.DISCUSSION)
            .join(tables.USERS, { user_id: user_id })
            .insert({ user_id, title, description, tags })
            .returning(["id"]);
        return discussion;
    }
    async getDiscussion(id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSSION)
                .where("user_id", id)
        );
    }
    async getAllDiscussion() {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSSION)
        );
    }

    async getDiscusUserByID(user_id: number) {
        const [disucssion] = await this.knex(tables.USERS)
            .select("id", "displayname", "avator", "headline")
            .from(tables.USERS)
            .where("id", user_id)
        return disucssion;
    }

    async updateDiscusVote(discus_id: number, vote: number) {
        return (
            await this.knex(tables.PROJECT)
                .where({ id: discus_id })
                .update({ vote: vote })
        );
    }
    async getStatDiscusComment(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSCOMMENT)
                .where("discus_id", id)
        );
    }
    async getStatDiscusCommentReply(id: number) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSCOMMENTREPLY)
                .where("discus_id", id)
        );
    }
    async joinDiscus_user(user_id: number) {
        const [user] = await this.knex(tables.DISCUSSION)
            .join(tables.USERS, 'users.id', 'user_id')
            .select(["user_id", "discussions.id", "title", "description", "displayname", "avator", "created_at"])
            .where({ user_id: user_id })
        return user;
    }
    async getDiscusSortByVotes() {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSSION)
                .orderBy('vote', 'desc')
                .limit(2)
        );
    }
    async getDiscusTagCount(tag: string) {
        return (
            await this.knex
                .select()
                .count("*")
                .from(tables.DISCUSSION)
                .whereRaw('? = ANY(tags) ', [tag])
        );
    }
    async getDiscusByTag(tag: string) {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSSION)
                .whereRaw('? = ANY(tags) ', [tag])
        );
    }
    async getUserByUsername(username: string) {
        return (
            await this.knex
                .select("*")
                .from(tables.USERS)
                .where("username", username)
        )[0];
    }
}
