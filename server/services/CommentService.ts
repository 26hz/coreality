import Knex from "knex"
import { tables } from "../table"

export class ProjCommentService {
    constructor(private knex: Knex) { }

    async createProjComment(user_id: number, proj_id: number, description: string) {
        const [user] = await this.knex(tables.PROJCOMMENT)
            .join(tables.USERS, { user_id: user_id })
            .join(tables.PROJECT, { proj_id: proj_id })
            .insert({ user_id, proj_id, description })
        return user;
    }
    async getProjComment(proj_id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.PROJCOMMENT)
                .where("proj_id", proj_id)
        );
    }

    async getProjCommentUserByID(user_id: number) {
        const [projectComment] = await this.knex(tables.USERS)
            .select("id", "username", "displayname", "avator")
            .from(tables.USERS)
            .where("id", user_id)
        return projectComment
    }

    async joinProjComment_user(user_id: number) {
        const [user] = await this.knex(tables.PROJCOMMENT)
            .join(tables.USERS, 'users.id', 'user_id')
            .select(["user_id", "proj_id", "showComment", "description", "displayname", "avator", "projComments.created_at"])
            .where({ user_id: user_id })
        return user;
    }
}

export class DiscusCommentService {
    constructor(private knex: Knex) { }

    async createDiscusComment(user_id: number, discus_id: number, description: string) {
        const [user] = await this.knex(tables.DISCUSCOMMENT)
            .join(tables.USERS, { user_id: user_id })
            .join(tables.DISCUSSION, { discus_id: discus_id })
            .insert({ user_id, discus_id, description })
        return user;
    }
    async getDiscusComment(discus_id: number) {
        return (
            await this.knex
                .select("*")
                .from(tables.DISCUSCOMMENT)
                .where("discus_id", discus_id)
        );
    }

    async getDiscusCommentUserByID(user_id: number) {
        const [discusComment] = await this.knex(tables.USERS)
            .select("id", "username", "displayname", "avator")
            .from(tables.USERS)
            .where("id", user_id)
        return discusComment
    }


    async joinDiscusComment_user(user_id: number) {
        const [user] = await this.knex(tables.DISCUSCOMMENT)
            .join(tables.USERS, 'users.id', 'user_id')
            .select(["user_id", "discus_id", "showComment", "description", "displayname", "avator", "discusComments.created_at"])
            .where({ user_id: user_id })
        return user;
    }
}


