import { Bearer } from "permit";
import express from "express";
import jwtSimple from "jwt-simple";
import jwt from "./jwt";
import { UserService } from "./services/UserServices"
import { logger } from "./utils/logger";

const permit = new Bearer({
    query: "access_token",
});
// create guard
export function createIsLoggedIn(userService: UserService) {
    // use guard function
    return async function (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction) {
        try {
            // get token first 
            const token = permit.check(req);
            // if dont have token
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            // verify is this our token 
            const payload: { id: number } = jwtSimple.decode(token, jwt.jwtSecret);
            const user = await userService.getUserByID(payload.id);
            if (!user) {
                return res.status(401).json({ msg: "Permission Denied" })
            }
            // get password and other thing will put on the back 
            const { password, ...others } = user;
            // need to set up global models
            req.user = { ...others } // get everthing expect password
            return next();
        } catch (err) {
            logger.error(err);
            res.status(401).json({ msg: "Permission Denied" });

        }
    }
}

