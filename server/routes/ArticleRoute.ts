import express from 'express';
import { articleController } from "../main";
import { upload } from '../main';
export const articleRoutes = express.Router();

articleRoutes.post("/createarticle", upload.single('imgThumb'), articleController.createArticle);
articleRoutes.post("/articleInfos", articleController.getArticle);
articleRoutes.post("/articleInfosByTag", articleController.getArticleByTag);
articleRoutes.post("/articleAllInfos", articleController.getAllArticle);
