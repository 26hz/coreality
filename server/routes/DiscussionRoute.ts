import express from 'express';
import { discussionController } from "../main";
export const discussionRoutes = express.Router();

discussionRoutes.post("/createdisccusion", discussionController.createDiscussion);
discussionRoutes.post("/discussionInfos", discussionController.getDiscussion);
discussionRoutes.post("/discussionInfosByTag", discussionController.getDiscussionByTag);
discussionRoutes.post("/disucsAllInfos", discussionController.getAllDiscussion);
