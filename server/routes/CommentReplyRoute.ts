import express from 'express';
import { discusCommentReplyController, projCommentReplyController } from "../main";
export const projCommentReplyRoutes = express.Router();
export const discusCommentReplyRoutes = express.Router();

projCommentReplyRoutes.post("/createprojcommentreply", projCommentReplyController.createProjCommentReply);
discusCommentReplyRoutes.post("/creatediscuscommentreply", discusCommentReplyController.createDiscusCommentReply);
