import express from 'express';
import { discusVoteController, projVoteController, artVoteController } from "../main";
export const projVoteRoutes = express.Router();
export const discusVoteRoutes = express.Router();
export const artVoteRoutes = express.Router();

projVoteRoutes.put("/updateProjVote", projVoteController.updateProjVote);
discusVoteRoutes.put("/updateDiscusVote", discusVoteController.updateDiscusVote);
artVoteRoutes.put("/updateArtVote", artVoteController.updateArtVote);