import express from 'express';
import { projectController } from "../main";
import { upload } from '../main';
export const projectRoutes = express.Router();

projectRoutes.post("/createproject", upload.fields([
    { name: 'imgThumb', maxCount: 1 },
    { name: 'imgDetail', maxCount: 4 }
]), projectController.createProject);
projectRoutes.post("/projectInfos", projectController.getProject);
projectRoutes.post("/projectInfosByTag", projectController.getProjectByTag);
projectRoutes.post("/projectAllInfos", projectController.getAllProject);
