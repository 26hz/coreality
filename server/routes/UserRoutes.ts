import express from 'express';
import { userController } from "../main";
import { upload } from '../main';
export const userRoutes = express.Router();

userRoutes.post("/login", userController.login);
userRoutes.post("/login/facebook", userController.loginFacebook);
userRoutes.post("/login/google", userController.loginGoogle);
userRoutes.post("/signup", userController.signup);
userRoutes.post("/userInfos", userController.getUserInfoByID);
userRoutes.put("/updateprofile", userController.updateUserProfile);
userRoutes.put("/updateavator", upload.single('avator'), userController.updateAvator);
userRoutes.put("/updateEmail", userController.updateEmail);
userRoutes.put("/updatePassword", userController.updatePassword);
userRoutes.get("/getUserByVotes", userController.getUserInfoByVotes);
userRoutes.get("/getTags", userController.getTagsbyCount);
userRoutes.post("/userProfileInfos", userController.getUserInfoByUsername);
// userRoutes.get("/", userController.elasticClient);