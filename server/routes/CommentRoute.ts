import express from 'express';
import { discusCommentController, projCommentController } from "../main";
export const projCommentRoutes = express.Router();
export const discusCommentRoutes = express.Router();

projCommentRoutes.post("/createprojcomment", projCommentController.createProjComment);
discusCommentRoutes.post("/creatediscuscomment", discusCommentController.createDiscusComment);
