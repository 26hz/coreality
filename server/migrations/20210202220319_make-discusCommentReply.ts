import * as Knex from "knex";
const dicusCommentReplyTable = "discusCommentsReply";
const discusCommentTable = "discusComments";
const discussionTable = "discussions";
const usersTable = "users";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(dicusCommentReplyTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.integer("discus_id").unsigned
        table.integer("comment_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`)
        table.foreign("discus_id").references(`${discussionTable}.id`)
        table.foreign("comment_id").references(`${discusCommentTable}.id`)
        table.text("description").notNullable
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(discusCommentTable)

}

