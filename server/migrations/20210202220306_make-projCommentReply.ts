import * as Knex from "knex";
const projCommentReplyTable = "projCommentsReply";
const projCommentTable = "projComments";
const projectsTable = "projects";
const usersTable = "users";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(projCommentReplyTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.integer("proj_id").unsigned
        table.integer("comment_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`)
        table.foreign("proj_id").references(`${projectsTable}.id`)
        table.foreign("comment_id").references(`${projCommentTable}.id`)
        table.text("description").notNullable
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(projCommentReplyTable)

}

