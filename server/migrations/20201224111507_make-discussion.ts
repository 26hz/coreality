import * as Knex from "knex";
const discusTable = "discussions";
const usersTable = "users";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(discusTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`);
        table.text("title").notNullable
        table.text("description").notNullable
        table.specificType('tags', 'TEXT[]')
        table.integer("vote").defaultTo(0)
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(discusTable)
}

