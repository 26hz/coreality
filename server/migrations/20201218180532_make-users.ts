import * as Knex from "knex";

const usersTable = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(usersTable, (table) => {
        table.increments();
        table.string("username").notNullable().unique
        table.string("password").notNullable()
        table.string("email").notNullable().unique
        table.string("role").notNullable()
        table.integer("exp").notNullable()
        table.specificType('skilltag', 'TEXT[]').notNullable()
        table.string("bio")
        table.string("headline")
        table.string("displayname")
        table.string("avator")
        table.string('googlelogin')
        table.string('facebooklogin')
        table.string('facebook')
        table.string("twitter")
        table.string("linkedin")
        table.string("github")
        table.string("website")
        table.integer("totalVotes").defaultTo(0)
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(usersTable)
}

