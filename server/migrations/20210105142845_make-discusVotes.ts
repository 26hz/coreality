import * as Knex from "knex";
const discusVotesTable = "discusVotes";
const usersTable = "users";
const discusTable = "discussions";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(discusVotesTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.integer("discus_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`)
        table.foreign("discus_id").references(`${discusTable}.id`)
        table.boolean("voted").defaultTo(true).notNullable
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(discusVotesTable)
}

