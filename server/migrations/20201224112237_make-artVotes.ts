import * as Knex from "knex";
const artVotesTable = "artVotes";
const usersTable = "users";
const articleTable = "articles";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(artVotesTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.integer("art_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`)
        table.foreign("art_id").references(`${articleTable}.id`)
        table.boolean("voted").defaultTo(true).notNullable
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(artVotesTable)
}

