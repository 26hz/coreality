import * as Knex from "knex";
const discusCommentTable = "discusComments";
const usersTable = "users";
const discussionsTable = "discussions";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(discusCommentTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.integer("discus_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`)
        table.foreign("discus_id").references(`${discussionsTable}.id`)
        table.boolean("showComment").defaultTo(false)
        table.text("description").notNullable
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(discusCommentTable)

}

