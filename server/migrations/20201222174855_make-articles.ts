import * as Knex from "knex";
const articleTable = "articles";
const usersTable = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(articleTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`);
        table.string("title").notNullable()
        table.text("description").notNullable()
        table.text("content").notNullable()
        table.integer("vote").defaultTo(0)
        table.specificType('tags', 'TEXT[]')
        table.string("imgThumb")
        table.string("videoLink")
        table.integer("art_votes_id").unsigned
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(articleTable)
}

