import * as Knex from "knex";
const projVotesTable = "projVotes";
const projectsTable = "projects";
const usersTable = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(projVotesTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.integer("proj_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`)
        table.foreign("proj_id").references(`${projectsTable}.id`)
        table.boolean("voted").defaultTo(true).notNullable
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(projVotesTable)
}

