import * as Knex from "knex";
const projectsTable = "projects";
const usersTable = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(projectsTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned
        table.foreign("user_id").references(`${usersTable}.id`);
        table.string("title").notNullable()
        table.string("headline").notNullable()
        table.boolean("status").notNullable()
        table.text("description").notNullable()
        table.specificType('tags', 'TEXT[]')
        table.string("link")
        table.integer("vote").defaultTo(0)
        table.string("imgThumb")
        table.string("imgDetail")
        table.string("videoLink")
        table.string("participated_user_username").unsigned
        table.timestamps(false, true);
    })
}



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(projectsTable)
}

