import express from "express";
import { userRoutes } from "./routes/UserRoutes";
import { projectRoutes } from "./routes/ProjectRoutes";
import { articleRoutes } from "./routes/ArticleRoute";
import { discussionRoutes } from "./routes/DiscussionRoute";
import { projCommentRoutes, discusCommentRoutes } from "./routes/CommentRoute";
import { projCommentReplyRoutes, discusCommentReplyRoutes } from "./routes/CommentReplyRoute";
import { projVoteRoutes, discusVoteRoutes, artVoteRoutes } from './routes/VoteRoute'
export const routes = express.Router();

routes.use("/", userRoutes);
routes.use("/", projectRoutes);
routes.use("/", articleRoutes);
routes.use("/", discussionRoutes);
routes.use("/", projCommentRoutes);
routes.use("/", discusCommentRoutes);
routes.use("/", projCommentReplyRoutes);
routes.use("/", discusCommentReplyRoutes);
routes.use("/", projVoteRoutes);
routes.use("/", discusVoteRoutes);
routes.use("/", artVoteRoutes);
