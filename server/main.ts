import express from "express";
import bodyParser from "body-parser";
import cors from 'cors';
import multer from 'multer';
import dotenv from 'dotenv';
dotenv.config();

import Knex from "knex";
const knexConfig = require("./knexfile");
const knex = Knex(knexConfig["development"])

const app = express();
app.use(cors());
app.use(express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

import { UserService } from "./services/UserServices";
import { UserController } from "./controllers/UserController";
import { ProjectService } from "./services/ProjectService";
import { ProjectController } from "./controllers/ProjectController";
import { ArticleService } from "./services/ArticleService";
import { ArticleController } from "./controllers/ArticleController";
import { DiscussionService } from "./services/DiscussionService";
import { DiscussionController } from "./controllers/DiscussionController";
import { ProjCommentService, DiscusCommentService } from "./services/CommentService";
import { ProjCommentController, DiscusCommentController } from "./controllers/CommentController";
import { ProjCommentReplyService, DiscusCommentReplyService } from "./services/CommentReplyService";
import { ProjCommentReplyController, DiscusCommentReplyController } from "./controllers/CommentReplyController";
import { ProjVoteService, DiscusVoteService, ArtVoteService } from "./services/VoteService";
import { ProjVoteController, DiscusVoteController, ArtVoteController } from "./controllers/VoteController";



const projVoteService = new ProjVoteService(knex);
export const projVoteController = new ProjVoteController(projVoteService);
const projCommentService = new ProjCommentService(knex);
export const projCommentController = new ProjCommentController(projCommentService);
const projCommentReplyService = new ProjCommentReplyService(knex);
export const projCommentReplyController = new ProjCommentReplyController(projCommentReplyService);
const projectService = new ProjectService(knex);
export const projectController = new ProjectController(projectService, projVoteService, projCommentService, projCommentReplyService);

const discusVoteService = new DiscusVoteService(knex);
export const discusVoteController = new DiscusVoteController(discusVoteService);
const discusCommentReplyService = new DiscusCommentReplyService(knex);
export const discusCommentReplyController = new DiscusCommentReplyController(discusCommentReplyService);
const discusCommentService = new DiscusCommentService(knex);
export const discusCommentController = new DiscusCommentController(discusCommentService);
const discussionService = new DiscussionService(knex);
export const discussionController = new DiscussionController(discussionService, discusCommentReplyService, discusCommentService, discusVoteService);

const artVoteService = new ArtVoteService(knex);
export const artVoteController = new ArtVoteController(artVoteService);
const articleService = new ArticleService(knex);
export const articleController = new ArticleController(articleService, artVoteService);

const userService = new UserService(knex);
export const userController = new UserController(userService, projVoteService, discusVoteService, artVoteService, projectService, discussionService, articleService);


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({ storage: storage })


import { routes } from "./routes";
const API_VERSION = "/api/v1";
app.use(API_VERSION, routes)

const PORT = process.env.PORT || 8080;


app.listen(PORT, () => {
    console.log(`[info] listening to Port: ${PORT}`)
})