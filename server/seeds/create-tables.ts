import * as Knex from "knex";
const articlesTable = "articles";
const projectsTable = "projects";
const usersTable = "users";
const projCommentTable = "projComments";
const discusTable = "discussion";
const projVotesTable = "projVotes";
const artVotesTable = "artVotes";
const discusCommentTable = "discusComment";
const discusVotesTable = "discusVotes";

import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(usersTable).del();
    await knex(articlesTable).del();
    await knex(projectsTable).del();
    await knex(discusTable).del();
    await knex(projCommentTable).del();
    await knex(projVotesTable).del();
    await knex(artVotesTable).del();
    await knex(discusCommentTable).del();
    await knex(discusVotesTable).del();

    // Inserts seed entries
    const hashedPassword = await hashPassword("Password1234");
    await knex(usersTable).insert([
        { id: 1, email: "kkevinhb@gmail.com", password: hashedPassword, displayname: "kevin", username: "kkevinhb", headline: "", bio: "", exp: 2, googlelogin: "", facebooklogin: "", twitter: "", website: "gustoxrlab.com", role: "AR developer", skilltag: "WEBAR", avator: "", linkedin: "kevinchui", github: "kkevinhb", facebook: "kevinfb" },
        { id: 2, email: "dantetsang@gamil.com", password: hashedPassword, displayname: "dante", username: "dante123", headline: "", bio: "", exp: 15, googlelogin: "", facebooklogin: "", twitter: "", website: "gustoxrlab.com", role: "Fullstack developer", skilltag: "WEBAR", avator: "", linkedin: "dantetsang", github: "dantetsang", facebook: "dantefb" },
        { id: 3, email: "nor.iffat32@gmail.com", password: hashedPassword, displayname: "farah", username: "farah123", headline: "", bio: "", exp: 1, googlelogin: "", facebooklogin: "", twitter: "", website: "gustoxrlab.com", role: "Frontend developer", skilltag: "Frontend", avator: "", linkedin: "Farah Noriffat", github: "Farah Noriffat", facebook: "farahfb" },
    ]);

    await knex(projectsTable).insert([
        { id: 1, user_id: 1, title: "Arcus title", headline: "This is the project headline", status: true, description: "This is a WebAR platform", tags: "", link: "http://staging.arcus.design/", vote: 0, imgThumb: "", imgDetail: "", videoLink: "https://www.youtube.com/watch?v=HMkrXFIs6XQ&ab_channel=THEOFFICIALJUNOMAK%E9%BA%A5%E6%B5%9A%E9%BE%8D", share: 0, participated_user_id: 2 }
    ]);

    await knex(articlesTable).insert([
        { id: 1, user_id: 1, title: "Coreality article title", description: "Coreality article description", content: "This is a WebAR platform content", vote: 0, tags: "", imgThumb: "", videoLink: "", comment: "This is comment", share: 0 }

    ]);

    await knex(discusTable).insert([
        { id: 1, user_id: 1, description: "this is discussion description", vote: 0 }
    ]);

    await knex(projCommentTable).insert([
        { id: 1, user_id: 2, proj_id: 1 }
    ]);

    await knex(projVotesTable).insert([
        { id: 1, user_id: 2, proj_id: 1 }
    ]);

    await knex(artVotesTable).insert([
        { id: 1, user_id: 1, art_id: 1 }
    ]);

    await knex(discusCommentTable).insert([
        { id: 1, user_id: 1, proj_id: 1 }
    ]);

    await knex(discusVotesTable).insert([
        { id: 1, user_id: 1, discus_id: 1 }
    ]);
};
