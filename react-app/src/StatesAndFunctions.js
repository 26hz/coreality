state = {
    links: [],
    link: '',
    menu: "profile",
		newProjectSteps: 3,
		pictures: [],
		projectName: '',
		headline: '',
		tags: ['Tags01', 'Tags02'],
		tag: '',
		participant: '',
		participants: ['John Doe', "Jane Doe"],
		projectLink: '',
		projectStatus: false,
		myProjects: [
			{
				id: 101,
				projectName: "WOW1",
				headline: "Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet",
				tags: ["WebAR", "JS"],
				comments: 251,
				share: 251,
				upvote: 251,
				isUpvoted: false,
			},
			{
				id: 102,
				projectName: "WOW2",
				headline: "Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet",
				tags: ["WebAR", "JS"],
				comments: 252,
				share: 252,
				upvote: 252,
				isUpvoted: false,
			},
			{
				id: 103,
				projectName: "WOW3",
				headline: "Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet",
				tags: ["WebAR", "JS"],
				comments: 253,
				share: 253,
				upvote: 253,
				isUpvoted: false,
			},
			{
				id: 104,
				projectName: "WOW4",
				headline: "Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet",
				tags: ["WebAR", "JS"],
				comments: 254,
				share: 254,
				upvote: 254,
				isUpvoted: false,
			},
			{
				id: 105,
				projectName: "WOW5",
				headline: "Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet",
				tags: ["WebAR", "JS"],
				comments: 255,
				share: 255,
				upvote: 255,
				isUpvoted: false,
			},
		]

	 }
	 
	 onChangeFiles(e) {
		var pictures = e.target.files;
		var picturesArr = Array.prototype.slice.call(pictures);
		this.setState({ pictures: [...this.state.pictures, ...picturesArr] });
	}
  
  removeFiles(f) {
		this.setState({ pictures: this.state.pictures.filter(x => x !== f) }); 
  }

   handleChange(i, event) {
    let links = [...this.state.links];
    links[i] = event.target.value;
    this.setState({ links });
  }
 
  addClick(){
   this.setState(prevState => ({ links: [...prevState.links, '']}))
  }
 
  removeClick(e, i){
    let links = [...this.state.links];
    links.splice(i,1);
    this.setState({ links });
	}
	
	removeTag(e, i){
    let tags = [...this.state.tags];
    tags.splice(i,1);
    this.setState({ tags });
	}

	removeParticipant(e, i){
    let participants = [...this.state.participants];
    participants.splice(i,1);
    this.setState({ participants });
	}


	handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
	}
	
	handleDelimeter(e){
		if(e.keyCode === 32){
			var tag = this.state.tag;
			var tagFake = tag.split(" ");
			tag = tagFake[0]
			var tags = this.state.tags;
			tags.push(tag);
			tag = '';
			this.setState({
				tag,
				tags,
			})
		} 
	}

	toggleUpvote = (id) => {
		const elementsIndex = this.state.myProjects.findIndex(element => element.id == id )
		let newArray = [...this.state.myProjects]
		newArray[elementsIndex] = {...newArray[elementsIndex], isUpvoted: !newArray[elementsIndex].isUpvoted}
		if(newArray[elementsIndex].isUpvoted){
			newArray[elementsIndex] = {...newArray[elementsIndex], upvote: newArray[elementsIndex].upvote+1}
		} else {
			newArray[elementsIndex] = {...newArray[elementsIndex], upvote: newArray[elementsIndex].upvote-1}
		}
		this.setState({
			myProjects: newArray,
		});
	}