import React from 'react';

import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';

function CommentInputReplyMobileModal(props){
	const {
		data,
		hideParentCommentModal,
		handleComment,
		submitComment,
	} = props;

	return (
		<div className="modal comment-modal d-block">
			<div className="modal-content comment-modal">
				<div onClick={hideParentCommentModal} className="close-modal-icon-container">
					<img src={wrongInvertedIcon} className="close-modal-icon"/>
				</div>
				<div className="text-16 my-4">Add a comment</div>
				<textarea className="mb-4" name="description" onChange={(e) => handleComment(e)} rows="4" />
				<div className="d-flex justify-content-end">
					<div className="btn-primary w-fit" 
						onClick={() => {
							submitComment("parent", data.id);
							hideParentCommentModal();
						}}
					>ADD A COMMENT</div>
				</div>
			</div>
		</div>
	);
}

export default CommentInputReplyMobileModal;