import React from 'react';

function SettingAlert () {
	return (
		<div className="setting-alert">
			An email has been sent to the new account to confirm the changes
			{/* Your password has been successfully changed. An email has been sent with the information. */}
			<div className="text-navy text-14 font-bold">OPEN YOUR EMAIL</div>
		</div>
	);
}

export default SettingAlert;