import React from 'react';

function LoadingModal () {
	return (
		<div className="bg-white mt-5 margin-auto loading p-3 d-flex align-items-center justify-content-center">
			<div className="spinner"></div>
		</div>
	);
}

export default LoadingModal;