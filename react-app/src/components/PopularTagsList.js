import { Link } from 'react-router-dom';

const PopularTagsList = ({
	type,
	tags
}) => {
	return (
		<div>
			{ Array.isArray(tags) && tags.map((tag, i) => 
				<Link
					key={i} 
					to={{
						pathname: "/" + ( type === "project" ? "project/" + tag.tagName : type === "discussion" ? "discuss/"+ tag.tagName : "article/" + tag.tagName),
					}}
				>
					<div className="d-flex align-items-center mb-4">
						<div className="tag text-16 mr-2">{tag.tagName}</div>
						<div className="text-14 text-gray">x{tag.tagCountNum}</div>
					</div>
				</Link>
			)}
		</div>
	);
}

export default PopularTagsList;