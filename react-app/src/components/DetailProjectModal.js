
import React, {  } from 'react';

import CommentsList from '../components/CommentsList';

import ShareDialog from "./ShareDialog";
import MoreTagsModal from "./MoreTagsModal";

import avatar from '../assets/img/avatar.png';

import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';
import editIcon from '../assets/icon/edit.png';

import whiteUpvote from '../assets/icon/white-upvote.png';
import InnerHTML from 'dangerously-set-html-content'
import purpleComment from '../assets/icon/purple-comment.png';
import purpleShare from '../assets/icon/purple-share.png';
import purpleUpvote from '../assets/icon/purple-upvote.png';
import purpleUrl from '../assets/icon/purple-url.png';

function DetailProjectModal(props) {
	
	const {
		data,
		isAuthenticated,
		loggedUser,
		comments,
		projCommentCount,
		reply,
		votes,
		authorUsers,
		commentUsers,
		commentReplyUsers,
		isFeatured,
		type,
		hideDetail,
		displayMoreTags,
		toggleComment,
		handleComment,
		submitComment,
		toggleUpvote,
		handleShare,
	} = props;

	const avator = 	<img className="discussion-avatar mr-3" src={avatar} alt="default-avatar"/>
	const authAvator = <img className="discussion-avatar mr-3" src={loggedUser && loggedUser.avator} alt="user-avatar"/>

	return (
		<div>
			{
				data &&
				<div className="modal d-block">
					<div className="modal-content project-detail">
						<div className="close-modal-icon-container mb-4">
							<img onClick={() => hideDetail(data.type, data.id)}  src={wrongInvertedIcon} className="close-modal-icon"/>
						</div>
						<div className="project-detail-container bg-white my-4 margin-auto p-5">
							<div className="d-flex justify-content-between">
								<div className="project-detail-content bg-white-border">
									<div className="project-detail-intro m-3 preview-container mb-6 d-flex align-items-center">
										<img className="new-project-thumbnail mr-3" src={data.imgThumb} />
										<div className="w-100">
											<div className="text-16 mb-2">{data.title}</div>
											<div className="text-gray text-14 mb-2">{data.headline}</div> 
											<div className="tags-container d-block text-14 my-3 hide-in-mobile">
												{ data.tags && data.tags.map((tag, j) =>
													<div key={j} className={"tag mr-2 " + (j < 2 ? "" : "d-none")}>{tag}</div>
												)}
												<div className={"btn-more tag c-pointer px-3 " + ( data.tags && data.tags.length < 2 ? "d-none" : "" )} onClick={() => displayMoreTags("modal", data.id, data.isFeatured)}>
													more
												</div>
											</div>
											<div className="tags-container text-14 mt-3 mb-6 w-100 mobile-only">
												{ data.tags && data.tags.map((tag, j) =>
													<div key={j} className={"tag mr-2 w-100 " + (j > 1 ? "d-none" : "")}>{tag}</div>
												)}
												<div className={"btn-more tag c-pointer px-3 w-100 " + ( data.tags && data.tags.length < 1 ? "d-none" : "" )} onClick={() => displayMoreTags("modal", data.id, data.isFeatured)}>
													more
												</div>
											</div>
											<div className="d-flex justify-content-between align-items-center project-detail-button-container">
												<div className="d-flex">
													<a href="#comments">
														<div className="comment-btn btn d-flex mr-2 text-navy c-pointer">
															<img src={purpleComment} className="preview-icon mr-2" />
															{ isFeatured ? projCommentCount && projCommentCount.featCommentCountNums && projCommentCount.featCommentCountNums[data.index] : projCommentCount && projCommentCount.commentCountNums && projCommentCount.commentCountNums[data.index]}
														</div>
													</a>
													<div onClick={() => handleShare("modal", data.id, data.index, isFeatured)} className="share-btn btn d-flex text-navy c-pointer">
														<img src={purpleShare} className="preview-icon mr-2" />
														SHARE
													</div>
												</div>
											</div>
										</div>
									</div>
									<div className="ml-2 mobile-only detail-project-col-2 detail-project-cols pt-0 px-3">
										<div className="d-flex align-items-center project-detail-button-container-2 mb-3">
											<div className="text-center text-navy d-flex justify-content-center align-items-center font-bold link-btn c-pointer mr-2">
												<a href={data.link} target="_blank">
													<img src={purpleUrl} className="preview-icon-url mr-2" />
													GET IT NOW
												</a>
											</div>
											<div onClick={(e) => toggleUpvote("modal", data.index, isFeatured)} className="text-navy font-bold upvote-btn c-pointer ml-1">
												<img src={purpleUpvote} className="preview-icon mr-2" />
												{data.vote}
											</div>
										</div>
										<div className="bg-white-border p-3 user-info-container ">
											<div className="d-flex align-items-center mb-3">
												{isFeatured 
													? 
													<div className="text-16 mb-1 mr-3">{authorUsers !== undefined && authorUsers.featProjUsers !== undefined && authorUsers.featProjUsers[data.index] !== undefined && authorUsers.featProjUsers[data.index].displayname}</div>
														: type === "project" ?
														<div className="text-16 mb-1 mr-3">{authorUsers !== undefined && authorUsers.projUsers !== undefined && authorUsers.projUsers[data.index] !== undefined && authorUsers.projUsers[data.index].displayname}</div>
															: avator
												}
											</div>
											<div className="text-16 text-gray">
												{isFeatured ? authorUsers !== undefined && authorUsers.featProjUsers[data.index] && authorUsers.featProjUsers[data.index].headline : authorUsers !== undefined && authorUsers.projUsers[data.index] && authorUsers.projUsers[data.index].headline}
											</div>
										</div>
									</div>
									<div className="detail-project-col-1 detail-project-cols pb-5 px-5">
										<div className="main-preview-image w-100 mb-2">
											<img src={data.imgDetail && data.imgDetail.length > 0 && data.imgDetail[0]} />
										</div>
										<div className="d-flex justify-content-between mt-1 preview-images-container">
											{data.imgDetail && data.imgDetail.map((img, i) => 
												<div key={i} className="preview-images">
													<img src={img} />
												</div>
											)}
										</div>
										<div className="mt-4 mb-1">
										<InnerHTML html={data.description} />
										</div>
										<div className="mt-2">Do you have any questions? Contact <span className="text-navy">@{isFeatured ? authorUsers !== undefined && authorUsers.featProjUsers[data.index] && authorUsers.featProjUsers[data.index].displayname : authorUsers !== undefined && authorUsers.projUsers[data.index] && authorUsers.projUsers[data.index].displayname}</span></div>
									</div>
								</div>
								<div className="ml-2 hide-in-mobile detail-project-col-2 detail-project-cols pt-0 px-3">
									<div className="d-flex align-items-center project-detail-button-container-2 mb-3">
										<div className="text-center text-navy d-flex justify-content-center align-items-center font-bold link-btn c-pointer mr-2">
											<a href={data.link} target="_blank">
												<img src={purpleUrl} className="preview-icon-url mr-2" />
												GET IT NOW
											</a>
										</div>
										<div onClick={() => toggleUpvote("modal", data.index, isFeatured)} className="text-navy font-bold upvote-btn c-pointer ml-1">
											<img src={whiteUpvote} className="preview-icon mr-2" />
											VOTE ({data.vote})
										</div>
									</div>
									<div className="bg-white-border p-3 user-info-container ">
										<div className="d-flex align-items-center mb-3">
											{
													isFeatured && authorUsers && authorUsers.featProjUsers !== undefined && authorUsers.featProjUsers[data.index] !== undefined ?
													<img src={authorUsers.featProjUsers[data.index].avator} className="discussion-avatar mr-3" />
														:
														authorUsers && authorUsers.projUsers !== undefined && authorUsers.projUsers[data.index] !== undefined && authorUsers.projUsers[data.index].avator ?
														<img src={authorUsers.projUsers[data.index].avator} className="discussion-avatar mr-3 pop" />
															: avator
												}
											<div className="text-16 mb-1 mr-3">{isFeatured ? authorUsers !== undefined && authorUsers.featProjUsers[data.index] && authorUsers.featProjUsers[data.index].displayname : authorUsers !== undefined && authorUsers.projUsers[data.index] && authorUsers.projUsers[data.index].displayname}</div>
										</div>
										<div className="text-16 text-gray">
											{isFeatured ? authorUsers !== undefined && authorUsers.featProjUsers[data.index] && authorUsers.featProjUsers[data.index].headline : authorUsers !== undefined && authorUsers.projUsers[data.index] && authorUsers.projUsers[data.index].headline}
										</div>
									</div>
								</div>
							</div>
							<div>
								<div className="mb-5 mt-6 text-20" id="comments">
									Comments
								</div>
								<div className="form-input-discussion-container hide-in-mobile d-flex align-items-center mt-3 mb-6 ">
									{isAuthenticated? (loggedUser.avator ? authAvator : avator) : avator}
									<div className="input-discussion-container p-relative">
										<input type="text" name="myComment" className="input-discussion" onChange={(e) => handleComment(e)} placeholder="Start a new comment..." />
									</div>
									<button onClick={() => submitComment("parent", data.id)} className="btn-inverted p-2 ml-2">
										Send
									</button>
								</div>
								<div className="form-input-discussion-container mobile-only-flex align-items-center mt-3 mb-6 ">
									{isAuthenticated? (loggedUser.avator? authAvator: avator) : avator}
									<div className="input-discussion-container p-relative">
										<input type="text" name="myComment" className="input-discussion" onChange={(e) => handleComment(e)} placeholder="Start a new comment..." />
									</div>
								</div>
								<div className="comment-list">
									<CommentsList 
										data = {data}
										comments = {comments}
										reply = {reply}
										votes = {votes}
										commentUsers = {commentUsers}
										commentReplyUsers = {commentReplyUsers}
										index = {data.index}
										type = {type}
										toggleComment = {toggleComment}
										handleComment = {handleComment}
										submitComment = {submitComment}
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
			}
		</div>
	);
}

export default DetailProjectModal;
