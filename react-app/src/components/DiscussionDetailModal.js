import  { Link } from 'react-router-dom';
import moment from 'moment';

import CommentsList from '../components/CommentsList';

import avatar from '../assets/img/avatar.png';

import fireIcon from '../assets/icon/fire.png';
import editIcon from '../assets/icon/edit.png';
import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';

import purpleComment from '../assets/icon/purple-comment.png';
import purpleShare from '../assets/icon/purple-share.png';
import whiteUpvote from '../assets/icon/white-upvote.png';

const DiscussionDetailModal = ({
	isAuthenticated,
	loggedUser,
	discussion,
	isFeatured,
	type,
	comments,
	reply,
	votes,
	authorUsers,
	commentUsers,
	commentReplyUsers,
	discusCommentCount,
	toggleUpvote,
	closeDetail,
	handleShare,
	displayMoreTags,
	displayComments,
	toggleComment,
	handleComment,
	submitComment,
}) => {

	const avator = 	<img className="discussion-avatar mr-3" src={avatar} alt="default-avatar"/>
	const authAvator = <img className="discussion-avatar mr-3" src={loggedUser && loggedUser.avator} alt="user-avatar"/>

	return (
		<div className="modal discussion d-block">
			<div className="modal-content discussion">
				<div onClick={closeDetail} className="close-modal-icon-container">
					<img src={wrongInvertedIcon} className="close-modal-icon"/>
				</div>
				<div className="bg-white p-3 p-relative mb-3">
					<Link className="mobile-only" to={"/discussion/" + discussion.id}>
						<div className="d-flex align-items-center justify-content-between mb-3">
							<div className="d-flex align-items-center">
								{authorUsers && authorUsers.discusUsers&& authorUsers.discusUsers[discussion.index] && authorUsers.discusUsers[discussion.index].avator ? <img className="discussion-avatar mr-3" src={authorUsers.discusUsers[discussion.index].avator} alt="default-avatar"/> : authorUsers && authorUsers.featDiscusUsers&& authorUsers.featDiscusUsers[discussion.index] && authorUsers.featDiscusUsers[discussion.index].avator ? authorUsers.featDiscusUsers[discussion.index].avator : avator}
								<div className="d-flex">
									<div className="text-16 mr-3">{authorUsers && authorUsers.discusUsers&& authorUsers.discusUsers[discussion.index] && authorUsers.discusUsers[discussion.index].displayname ? authorUsers.discusUsers[discussion.index].displayname : authorUsers && authorUsers.featDiscusUsers && authorUsers.featDiscusUsers[discussion.index] ? authorUsers.featDiscusUsers[discussion.index].displayname : "User Not Found"}</div>
									<div className="text-16 text-gray">
										{moment( new Date(discussion.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
									</div>
								</div>
							</div>
							{
								type==="featured" &&
								<img className="fire-icon" src={fireIcon} />
							}
						</div>
						<div className="discussion-question text-20 mb-2">{discussion.title}</div>
						<div className="discussion-description hide-in-mobile text-gray mt-2">{discussion.description}</div>
						<div className="tags-container d-block mobile-only-flex text-14 mt-3">
						{ discussion.tags && discussion.tags.map((tag, j) =>
							<div key={j} className={"tag mr-2 " + (j > 0 ? "hide-in-mobile " : "")}>{tag}</div>
						)}
							<div className="mobile-only btn-more c-pointer tag px-3" onClick={() => displayMoreTags("modal", discussion.id, isFeatured)}>
								more
							</div>
						</div>
						<div className={"d-flex justify-content-between align-items-center mt-3 " + (discussion.showComment ? "pb-3 border-bottom" : "mb-1")}>
							<div className="d-flex">
								<div onClick={() => displayComments("modal", discussion.id, isFeatured)} className={"comment-btn btn btn d-flex align-items-center mr-2 text-navy c-pointer " + (discussion.showComment ? "active" : "") }>
									<img src={purpleComment} className="preview-icon mr-2" />
									{isFeatured ? discusCommentCount && discusCommentCount.featCommentCountNums && discusCommentCount.featCommentCountNums[discussion.index] : discusCommentCount && discusCommentCount.commentCountNums && discusCommentCount.commentCountNums[discussion.index]}
								</div>
								<div onClick={() => handleShare("modal", discussion.id, discussion.index, isFeatured)} className="share-btn btn d-flex align-items-center text-navy c-pointer">
									<img src={purpleShare} className="preview-icon mr-2" />
									<span className="text-12">Share</span>
								</div>
							</div>
							<div onClick={() => toggleUpvote("modal", discussion.index, isFeatured)} className={"upvote-btn align-items-center text-navy c-pointer " + (votes[discussion.index] && votes[discussion.index][0].voted ? "active" : "") }>
								<img src={whiteUpvote} className="preview-icon mr-4" />
								{discussion.vote}
							</div>
						</div>
					</Link>
					<div className="hide-in-mobile">
						<div className="d-flex align-items-center justify-content-between mb-3">
							<div className="d-flex align-items-center">
								{authorUsers && authorUsers.discusUsers && authorUsers.discusUsers[discussion.index] && authorUsers.discusUsers[discussion.index].avator ? <img className="discussion-avatar mr-3" src={authorUsers.discusUsers[discussion.index].avator} alt="default-avatar"/> : avator}
								<div className="d-flex">
									<div className="text-16 mr-3">{authorUsers && authorUsers.discusUsers && authorUsers.discusUsers[discussion.index] && authorUsers.discusUsers[discussion.index].displayname ? authorUsers && authorUsers.discusUsers[discussion.index].displayname : authorUsers && authorUsers.featDiscusUsers&& authorUsers.featDiscusUsers[discussion.index] ? authorUsers.featDiscusUsers[discussion.index].displayname : "User Not Found"}</div>
									<div className="text-16 text-gray">
										{moment( new Date(discussion.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
									</div>
								</div>
							</div>
							{
								type==="featured" &&
								<img className="fire-icon" src={fireIcon} />
							}
						</div>
						<div className="discussion-question text-20 mb-2">{discussion.title}</div>
						<div className="discussion-description hide-in-mobile text-gray mt-2">{discussion.description}</div>
						<div className="tags-container d-block mobile-only-flex text-14 mt-3">
						{ discussion.tags && discussion.tags.map((tag, j) =>
							<div key={j} className={"tag mr-2 " + (j > 0 ? "hide-in-mobile " : "")}>{tag}</div>
						)}
							<div className="mobile-only btn-more c-pointer tag px-3" onClick={() => displayMoreTags("modal", discussion.id, isFeatured)}>
								more
							</div>
						</div>
						<div className={"d-flex justify-content-between align-items-center mt-3 " + (discussion.showComment ? "pb-3 border-bottom" : "mb-1")}>
							<div className="d-flex">
								<div onClick={() => displayComments("modal", discussion.id, isFeatured)} className={"comment-btn btn btn d-flex align-items-center mr-2 text-navy c-pointer " + (discussion.showComment ? "active" : "") }>
									<img src={purpleComment} className="preview-icon mr-2" />
									{isFeatured ? discusCommentCount && discusCommentCount.featCommentCountNums && discusCommentCount.featCommentCountNums[discussion.index] : discusCommentCount && discusCommentCount.commentCountNums && discusCommentCount.commentCountNums[discussion.index]}
								</div>
								<div onClick={() => handleShare("modal", discussion.id, discussion.index, isFeatured)} className="share-btn btn d-flex align-items-center text-navy c-pointer">
									<img src={purpleShare} className="preview-icon mr-2" />
									<span className="text-12">Share</span>
								</div>
							</div>
							<div onClick={() => toggleUpvote("modal", discussion.index, isFeatured)} className={"upvote-btn align-items-center text-navy c-pointer " +  (votes[discussion.index] && votes[discussion.index][0].voted && "active") }>
								<img src={whiteUpvote} className="preview-icon mr-4" />
								{discussion.vote}
							</div>
						</div>
					</div>
					<div className={discussion.showComment ? "d-block" : "d-none"}>
						<div className="form-input-discussion-container d-flex hide-in-mobile align-items-center mt-3 mb-6 ">
						{isAuthenticated? (loggedUser.avator? authAvator: avator) : avator}
							<div className="input-discussion-container p-relative">
								<img src={editIcon} className="pen-discussion-input"/>
								<input type="text" name="myComment" className="input-discussion" onChange={(e) => handleComment(e)} placeholder="Start a new comment..." />
							</div>
							<button className="btn-inverted p-2 ml-2" onClick={() => submitComment("parent", discussion.id, null, "modal")}>
								Send
							</button>
						</div>
						<div className="comment-list">
							<CommentsList
								isModal = {true}
								isFeatured = {isFeatured}
								type = {"discussion"}
								isAuthenticated = {isAuthenticated}
								loggedUser= {loggedUser}
								data = {discussion}
								comments = {comments}
								reply = {reply}
								commentUsers = {commentUsers}
								commentReplyUsers = {commentReplyUsers}
								type = {type}
								toggleComment = {toggleComment}
								handleComment = {handleComment}
								submitComment = {submitComment}
							/>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default DiscussionDetailModal;