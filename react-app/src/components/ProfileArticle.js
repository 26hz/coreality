import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {articleInfosThunk} from '../redux/article/thunk';
import {updateArtVoteThunk} from '../redux/vote/thunk';
import {useSelector, useDispatch} from 'react-redux';
import { userInfosByVotesThunk } from '../redux/user/thunk'
import {useParams} from 'react-router-dom';

import ArticleList from "../components/ArticleList";

import ShareDialog from "../components/ShareDialog";
import MoreTagsModal from "../components/MoreTagsModal";

import plusIcon from '../assets/icon/plus.png';

function ProfileArticle() {
	const dispatch = useDispatch();
	const allArticle = useSelector(state => state.art.ArticleInfos.allArticle);
	const allArticleUsers = useSelector(state => state.art.ArticleInfos.allArtUsers);
	const artVotes = useSelector(state => state.art.ArticleInfos.artVotes);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const userInfos = useSelector(state => state.user.userInfos);
	const userProfileInfos = useSelector(state => state.user.userProfileInfos);
	let param = useParams();

	useEffect(() => {
		dispatch(articleInfosThunk(param));
		dispatch(userInfosByVotesThunk());
	},[dispatch])

	const [articles, setArticles] = useState([]);
	const [votes, setVotes] = useState([]);
	const [articleUsers, setArticleUsers] = useState([]);
	const [isDetailShown, setDetailShown] = useState(false);
	const [dataDetailModal, setDataDetailModal] = useState({});
	const [isDisplayShare, setShareShown] = useState(false);
	const [isMoreTagsShown, setTagShown] = useState(false);
	const [dataTags, setDataTags] = useState([]);
	const [shareData, setShareData] = useState({});
	const forceUpdate = React.useReducer(bool => !bool)[1];

	useEffect(() => {
		if(allArticle !== null && allArticle !== undefined && allArticle.length > 0){
			setArticles(allArticle);
		}
	},[allArticle]);
	
	var once = true;
	useEffect(() => {
		if(once && articles.length > 0){
			once = false;
			sortLatest();
		}
	},[articles]);

	useEffect(() => {
		if(artVotes !== null && artVotes !== undefined && artVotes.length > 0){
			setVotes(artVotes);
		}
	},[artVotes]);

	useEffect(() => {
		if(allArticleUsers !== undefined && allArticleUsers.artUsers !== null && allArticleUsers.artUsers.length > 0){
			setArticleUsers(allArticleUsers.artUsers);
		}
	},[allArticleUsers])

	const toggleUpvote = (e, index) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data = votes;
		let newObj;
		if(data[index].length > 0){
			newObj = data[index][0];
			newObj.voted = !newObj.voted;
		} else {
			data[index].push({});
			newObj =  data[index][0];
			newObj.voted = true;
		}
		var article = articles;
		if(newObj.voted){
			article[index].vote = parseInt(article[index].vote) + 1;
		} else {
			article[index].vote = parseInt(article[index].vote) - 1;
		}
		dispatch(updateArtVoteThunk(newObj.voted, article[index].id));
		setArticles(article);
		forceUpdate();
	}

	const handleShare = (e, id, index) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		if (navigator.share) {
			navigator.share({
				title: 'Coreality',
				url: window.location.href + "/articles/" + id
			}).then(() => {
				console.log('Thanks for sharing!');
			})
			.catch(console.error);
		} else {
			setShareShown(true);
			var newObj = articles[index];
			newObj.type = "article";
			setShareData(newObj);
		}
	}

	const handleCloseShare = () => {
		setShareShown(false);
	}

	const displayMoreTags = (e, index) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data = articles;
		setDataTags(data[index]);
		setTagShown(true);
	}

	const closeTagsModal = () => {
		setTagShown(false)
	}

	const hideDetail = () =>{
		setDetailShown(false);
	}

	const displayDetail = (index, isFeatured) => {
		var data = articles;
		let dataDetailModal = data[index];
		dataDetailModal.isFeatured = isFeatured;
		dataDetailModal.index = index;
		setDetailShown(true)
		setDataDetailModal(dataDetailModal);
	}

	const sortLatest = () => {
		var data = articles;
		var artUser = articleUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (new Date(data[i - 1].created_at) - Date.now() < new Date(data[i].created_at) - Date.now()) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//article author
					if(artUser.projUsers !== undefined){
						temp = artUser.artUsers[i - 1];
						artUser.artUsers[i - 1] = artUser.artUsers[i];
						artUser.artUsers[i] = temp;
					}
				}
			}
		}
		setArticles(data);
		setArticleUsers(artUser);
		forceUpdate();
	}
	const addArticle = 
	<Link to="/new-article" className="link">
	<button className="new-project-btn align-items-center d-flex px-3 py-2">
		<img className="mr-2" src={plusIcon} />
		ADD NEW ARTICLE
	</button>
	</Link>
	return (
		<div className="profile-content">
			{
				isDisplayShare && 
				<ShareDialog 
					data = {shareData}
					hideShare = {handleCloseShare}
				/>
			}

			{
				isMoreTagsShown &&
				<MoreTagsModal 
					data = {dataTags}
					hideModal = {closeTagsModal}
				/>
			}
			
			<div className="d-flex justify-content-between mt-5 mb-4 flex-direction-column-mobile">
				<span className="text-20">Articles</span>
			{ (userInfos.id == userProfileInfos.id)? addArticle: ''}
			</div>

			<ArticleList 
				isAuthenticated={isAuthenticated}
				loggedUser={userInfos}
				articles = {articles}
				type = "article"
				articleUsers = {articleUsers}
				shareData = {shareData}
				isMoreTagsShown = {isMoreTagsShown}
				isDisplayShare = {isDisplayShare}
				data = {dataDetailModal}
				votes = {votes}
				isDetailShown = {isDetailShown}
				toggleUpvote={toggleUpvote}
				handleShare={handleShare}
				hideShare = {handleCloseShare}
				displayMoreTags={displayMoreTags}
				closeTagsModal = {closeTagsModal}
				displayDetail={displayDetail}
				hideDetail = {hideDetail}
			/>
		</div>
	);
}

export default ProfileArticle;