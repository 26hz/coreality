import React, { useState, useEffect } from 'react';
import {useSelector, useDispatch } from 'react-redux'
import {Input, Label, Button, Alert} from 'reactstrap';
import { uploadAvatorThunk, userInfosThunk } from '../redux/user/thunk';
import avatar from '../assets/img/avatar.png';
export default function SettingAvatar () {

	const [avator, setAvator] = React.useState(null);
	const [selectedFile, setSelectedFile] = useState();
	const [isFilePicked, setIsFilePicked] = useState(false);
	const dispatch = useDispatch();
	const changeHandler = (e) => {
		setSelectedFile(e.target.files[0]);
		setIsFilePicked(true);
        if (e.target.files[0] == null){
            return null
        } else{
            setAvator(URL.createObjectURL(e.target.files[0]))
        }
	};
	
	useEffect(()=>{
        dispatch(userInfosThunk());
	},[dispatch])

	const avatorDefault = useSelector(state => state.user.userInfos.avator);
	const msg = useSelector(state => state.user.avator.msg);

	const handleSubmission = (e) => {
		e.preventDefault()
		dispatch(uploadAvatorThunk(selectedFile))
	};
		let imgPreview;
		let imgDefault = <img className="avatar-image mr-4" alt="default-avatar" src={avatorDefault? avatorDefault : avatar} alt='avator'/>
        if (avator) {
            imgPreview = <img className="avatar-image mr-4" alt="avatar" src={avator} />;
		}
		return (
			<div className="setting-profile-container">
				<div className="text-20 mt-6 mb-5">Set or change your avatar</div>
				<div className="bg-white setting-avatar-container bg-white-border p-5 mb-5 d-flex align-items-center">
						{imgPreview? imgPreview :imgDefault }
					<div className="setting-info-container">
						<div className="mb-2">Recommended Size  132 x 132 px</div>
						<div className="mb-3">JPEG, PNG size: 2MB</div>
						<div className="d-flex setting-btn-container">
							<Label className="btn-save-setting border-none" >
							<Input type="file" name="avator" onChange={changeHandler} />
								CHANGE YOUR PHOTO
							</Label>						
							<Button className="btn-save-setting" type="button" name="upload" onClick={handleSubmission}>UPDATE</Button>
						</div>
						{msg ? <Alert color="red">{msg}</Alert> : ""}
					</div>
				</div>
			</div>
		);
	}
