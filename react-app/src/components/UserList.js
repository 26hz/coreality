import moment from 'moment';
import { Link } from 'react-router-dom';

import avatar from '../assets/img/avatar.png';

import fireIcon from '../assets/icon/fire.png';

const UserList = ({
		users,
		type,
}) => {
	return (
		<div>
			{ Array.isArray(users) && users.map((user, i) => 
				<div key={i}> 
					<div className="p-relative w-100 d-flex justify-content-between align-items-center border-bottom mb-3 pb-3">
						<Link to={"profile/" + user.username}>
							<div className="d-flex align-items-center">
								<img src={user.avator?user.avator: avatar} className="discussion-avatar mr-3" />
								<div className="text-16 mr-3 mb-1">{user.displayname}</div>
							</div>
						</Link>
						{
							i < 2 && type === "activeUsers" &&
						<div className={"tooltip" }>
							<img src={fireIcon} className="fire-icon-home" />
							<span className="tooltiptext text-left">#{i+1} User of the day <br/> {moment(Date.now()).format("MMMM Do, YYYY")}</span>
						</div>
						}
					</div>
				</div>
			)}
		</div>
	);
}

export default UserList;