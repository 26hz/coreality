import React from 'react';
import {Input, Form, FormGroup} from 'reactstrap';

import wrongIcon from '../assets/icon/wrong.png';
import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';

function AddDiscussionMobileModal(props){
	const {
		tag,
		inputs,
		msg,
		alertmsg,
		hideModal,
		handleInputChange,
		handleDelimeter,
		changeTagValue,
		removeTag,
		onSubmit,
		checkKeyDown,
		register,
		handleSubmit,
	} = props;

	return (
		<div className="modal add-discussion d-block">
			<div className="modal-content">
				<div onClick={hideModal} className="close-modal-icon-container mb-4">
					<img src={wrongInvertedIcon} className="close-modal-icon"/>
				</div>
				<Form onSubmit={handleSubmit(onSubmit)} onKeyDown={(e) => checkKeyDown(e)} className="w-100 p-3 mb-3">
					<div className="mb-3">
						<div className="label mb-1">Discussion Title</div>
						<FormGroup className="input-container mw-100-important">
							<Input 
							innerRef={register}
							type="text" 
							placeholder="What is the best strategy to reduce..." 
							className="mw-100-important text-14" 
							name="title" 
							onChange={(e) => handleInputChange(e)} 
							value={inputs.title}/>
						</FormGroup>
					</div>
					<div className="mb-3">
						<div className="label mb-1">What is your discussion about?</div>
						<FormGroup className="input-container mw-100-important">
							<textarea 
							ref={register} 
							className="mw-100-important text-14" 
							placeholder="Write the content of your Discussion" 
							onChange={(e) => handleInputChange(e)} 
							name="description" 
							value={inputs.description}/>
						</FormGroup>
					</div>
					<div className="mb-4">
						<div className="label mb-1">Add Tags</div>
						<div className="input-container mw-100-important text-14">
							<input type="text" name="tags" className="pr-6 mw-100-important" placeholder="Select tags" onKeyUp={(e) => handleDelimeter(e)} onChange={(e) => changeTagValue(e)} value={tag} />
						</div>
						<div className="tags-container mt-3 d-block">
							{ inputs.tags.length > 0 && inputs.tags.map((e, i) =>
								<div key={i} className={"tag-input text-14 mr-2 mb-2" + (i < 4 ? "d-block" : "d-none")}>{e}<img src={wrongIcon} onClick={() => removeTag(i)} className="ml-2 c-pointer"/></div>
							)}
						</div>
					</div>
					<div className="buttons-container">
						<Input className="btn-primary discussion-btn ml-3 mb-4" type="submit" value="PUBLISH" />
					</div>
					{!msg? msg : alertmsg}
				</Form>
			</div>
		</div>
	);
}

export default AddDiscussionMobileModal;