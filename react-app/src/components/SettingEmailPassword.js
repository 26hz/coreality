import React, { useState, useRef } from 'react';
import { Form, FormGroup, Input, Alert } from "reactstrap";
import { useForm } from "react-hook-form";
import {useSelector, useDispatch} from 'react-redux';
import {updateEmailThunk, updatePasswordThunk} from '../redux/user/thunk'
import checkedIcon from '../assets/icon/checked.png';
import eye from '../assets/icon/eye.png';


function SettingEmailPassword () {
	const [visible, setVisible] = useState(false);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [passwordChange, setChangePassword] = useState("");
	const dispatch = useDispatch();
	const msg = useSelector(state => state.user.userInfos.msg);
    let alertmsg = <Alert className='alertDanger'>{msg}</Alert>

	const { register, handleSubmit } = useForm({
		mode: 'onSubmit',
		reValidateMode: 'onChange',
	});
	const onSubmit1 = async (values) => {
		const { email} = values;
		dispatch(updateEmailThunk(email));
	}
	const onSubmit2 = async (values) => {
		const { oldPassword, newPassword } = values;
		dispatch(updatePasswordThunk(oldPassword, newPassword));
	}
	
	const passwordRef = useRef(null);
	const passwordNewRef = useRef(null);
	
	const passwordToggle = (newPassword) => {
		if(newPassword){
			let type = passwordNewRef.current.type;
			type === 'password' ? type = 'text' : type = 'password';
			passwordNewRef.current.type = type;
		} else {
			let type = passwordRef.current.type;
			type === 'password' ? type = 'text' : type = 'password';
			passwordRef.current.type = type;
		}
	}

	const handleInputChange = (event) => {
    	const target = event.target;
    	const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		if(name === "email"){
			setEmail(value)
		} else if(name === "newPassword"){
			(value.length > 6 ) ? setVisible(false) : setVisible(true);
			setChangePassword(value);
		}
	}

	return (
		<div className="setting-profile-container">
			<div className="text-20 mt-6 mb-5">Set or change your email and password</div>
			<div className="bg-white bg-white-border p-5 mb-5">
				<div className="text-16 font-bold mb-4">Change your email</div>
				<Form onSubmit={handleSubmit(onSubmit1)}>
					<div className="mb-4">
						<div className="label mb-2">Email</div>
						<FormGroup className="input-container mb-1">
							<Input type="text"
							innerRef={register}
							placeholder="Example@gmail.com" 
							className="pr-6" 
							name="email" 
							/>
							<img src={checkedIcon} className="input-icon" />
						</FormGroup>
					</div>
					<div className="input-container btn-container-right d-flex">
						<Input className="btn-save-setting" type="submit" value="SAVE EMAIL" />
					</div>
				</Form>
				<div className="text-16 font-bold mt-5 mb-4">Change your password</div>
				<div>
				<Form onSubmit={handleSubmit(onSubmit2)}>
					<div className="mb-4">
						<div className="label mb-2">Old Password</div>
							<div className="input-container mb-1">
								<FormGroup className="p-relative">
									<Input 
									type="password" 
									name="oldPassword" 
									innerRef={register}
									onChange={handleInputChange} 
									innerRef={passwordRef} 
									placeholder="Enter your password" 
									className="pr-6" 
									/>
									<img src={eye} onClick={() => passwordToggle(false)} className="input-icon-2 eye" />
								</FormGroup>
							</div>
						<div className="label mb-2">New Password</div>
							<div className="input-container mb-1">
								<FormGroup className="p-relative">
									<Input 
									type="password" 
									name="newPassword" 
									innerRef={register}
									onChange={handleInputChange} 
									innerRef={passwordNewRef} 
									placeholder="Enter your password" 
									className="pr-6" 
									/>
									<img src={eye} onClick={() => passwordToggle(true)} className="input-icon-2 eye" />
								</FormGroup>
								<Alert className="mt-3" color="danger" isOpen={visible}>
									Password must have at least 6 characters
								</Alert>
								{!msg? msg : alertmsg}
							</div>
					</div>
					<div className="input-container btn-container-right d-flex">
						<Input className="btn-save-setting text-navy" type="submit" value="SAVE PASSWORD" />
					</div>
					</Form>
				</div>
			</div>
		</div>
	);
}

export default SettingEmailPassword;