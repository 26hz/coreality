import { Link } from 'react-router-dom';

import avatar from '../assets/img/avatar.png';

import fireIcon from '../assets/icon/fire.png';

const TagList = ({
		tags,
		type,
}) => {
	return (
		<div>
			{ Array.isArray(tags) && tags.map((tag, i) => 
				<div key={i} className="tags-container-tagpage d-flex align-items-center mb-3">
					<Link
						key={i} 
						to={{
							pathname: "/" + ( type === "project" ? "project/" + tag.tagName : type === "discussion" ? "discuss/"+ tag.tagName : "article/" + tag.tagName),
						}}
					>
						<div className="tag text-16 c-pointer mr-2">{tag.tagName}</div>
					</Link>
					<div className="mentioned text-14 text-gray">x{tag.tagCountNum}</div>
				</div>
			)}
		</div>
	);
}

export default TagList;