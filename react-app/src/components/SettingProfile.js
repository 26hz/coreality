import React , { useEffect, useState } from 'react';
import {useSelector, useDispatch } from 'react-redux'
import checkedIcon from '../assets/icon/checked.png';
import questionMarkIcon from '../assets/icon/question-mark.png';
import {userInfosThunk} from '../redux/user/thunk';
import { Form, FormGroup, Input, Alert } from "reactstrap";
import { useForm } from "react-hook-form";
import {updateUserProfileThunk} from "../redux/user/thunk"
import { setSyntheticLeadingComments } from 'typescript';
import wrongIcon from '../assets/icon/wrong.png';

export default function SettingProfile() {
	const { register, handleSubmit } = useForm({
		mode: 'onSubmit',
		reValidateMode: 'onChange',
	});
	const dispatch = useDispatch();
	const userInfos = useSelector(state => state.user.userInfos);
	const [skilltag, setSkillTag] = useState("");
	const [skilltags, setSkillTags] = useState([]);
	let [isTagsAboveMax, setIsTagsAboveMax] = React.useState(false);

	useEffect(()=>{
			dispatch(userInfosThunk());
	},[dispatch])
		
	const handleInputChange = (e) => {
		const target = e.target;
		const value = target.value;
		setSkillTag(value)
	}

	const handleTag = (e) =>{
		var tempSkillTags = skilltags;
		if(e.keyCode === 13){
			setSkillTag(skilltag.trim());
			if(tempSkillTags.length > 3){
				setIsTagsAboveMax(true);
			} else {
				setIsTagsAboveMax(false);
				if(skilltag !== ""){
					setSkillTags(tempSkillTags.concat(skilltag))
					setSkillTag("");
				}
			}
		} 
	}

	const removeTag = (e, i) => {
		setIsTagsAboveMax(false);
		skilltags.splice(i,1);
		setSkillTags(skilltags);
	}

	const checkKeyDown = (e) => {
    if (e.code === 'Enter') e.preventDefault();
  };
	
	const onSubmit = (data) => {
		const { username, displayname, role, headline, bio, website, facebook, twitter, linkedin,exp  } = data;
		dispatch(updateUserProfileThunk(username, displayname, role, headline, bio, website, facebook, twitter, linkedin,skilltags, exp ));
	};
		return (
			<div className="setting-profile-container">
				<div className="text-20 mt-6 mb-5">Edit your personal profile</div>
				<div className="bg-white bg-white-border p-5 mb-5">
					<div className="text-20 mb-4">Profile Information</div>
					<Form onSubmit={handleSubmit(onSubmit)} onKeyDown={(e) => checkKeyDown(e)}>
						<div className="mb-4">
							<div className="label mb-2">Username<span className="text-gray"> - Required</span></div>
							<FormGroup className="input-container mb-1">
								<Input type="text" innerRef={register} placeholder="coreality.com/username" defaultValue={userInfos.username} className="pr-6" name="username" />
								<img src={checkedIcon} className="input-icon-2" />
							</FormGroup>
						</div>
						<div className="mb-4">
							<div className="label mb-2">Name<span className="text-gray"> - Required</span></div>
							<FormGroup className="input-container mb-1">
								<Input type="text" innerRef={register} placeholder="John Doe" defaultValue={userInfos.displayname} className="pr-6" name="displayname" />
								<div className="tooltip">
									<img src={questionMarkIcon} className="input-icon-2" />
									<span className="tooltiptext">This is the name that people will see when they visit your public profile.</span>
								</div>
							</FormGroup>
						</div>
						<div className="mb-4">
							<div className="label mb-2">Your Role<span className="text-gray"> - Required</span></div>
							<div className="input-container mb-1">
								<Input type="text" innerRef={register} placeholder="UX Designer" defaultValue={userInfos.role} className="pr-6" name="role" />
								<img src={checkedIcon} className="input-icon-2" />
							</div>
						</div>
						<div className="mb-4">
							<div className="label mb-2">Your Headline</div>
							<FormGroup className="input-container mb-1">
								<Input type="text" innerRef={register} placeholder="Headline" defaultValue={userInfos.headline} className="pr-6" name="headline" />
								<img src={checkedIcon} className="input-icon-2" />
							</FormGroup>
						</div>
						<div className="mb-4">
							<div className="label mb-2">Your Biography</div>
							<FormGroup className="input-container mb-1">
								<Input type="text" innerRef={register} placeholder="Biography" defaultValue={userInfos.bio} className="pr-6" name="bio" />
								<img src={checkedIcon} className="input-icon-2" />
							</FormGroup>
						</div>
						<div>
							<div className="text-16 my-4">Links</div>
							<div className="mb-4">
								<div className="label mb-2">Personal Website</div>
								<FormGroup className="input-container mb-1">
									<Input type="text" innerRef={register} placeholder="http://www.example.com" defaultValue={userInfos.website} className="pr-6" name="website" />
									<img src={checkedIcon} className="input-icon-2" />
								</FormGroup>
							</div>
							<div className="mb-4">
								<div className="label mb-2">Facebook</div>
								<FormGroup className="input-container mb-1">
									<Input type="text" innerRef={register} placeholder="" defaultValue={userInfos.facebook} className="pr-6" name="facebook" />
									<img src={checkedIcon} className="input-icon-2" />
								</FormGroup>
							</div>
							<div className="mb-4">
								<div className="label mb-2">Twitter</div>
								<FormGroup className="input-container mb-1">
									<Input type="text" innerRef={register} placeholder="@" defaultValue={userInfos.twitter} className="pr-6" name="twitter" />
									<img src={checkedIcon} className="input-icon-2" />
								</FormGroup>
							</div>
							<div className="mb-4">
								<div className="label mb-2">Linkedin</div>
								<FormGroup className="input-container mb-1">
									<Input type="text" innerRef={register} placeholder="" defaultValue={userInfos.linkedin} className="pr-6" name="linkedin" />
									<img src={checkedIcon} className="input-icon-2" />
								</FormGroup>
							</div>

							<div className="mb-4">
								<div className="label mb-2">Software &#38; Skills<span className="text-gray"> - Required</span></div>
								<FormGroup className="input-container mb-1">
									<Input type="text" 
										onKeyUp={(e) => {handleTag(e) }} 
										value={skilltag} 
										onChange={(e) => { handleInputChange(e) }}
										className="pr-6" 
										name="skilltag" 
									/>
									<img src={checkedIcon} className="input-icon-2" />
								</FormGroup>
									<div className="tags-container w-100 mt-2 mb-5">
										{skilltags && skilltags.map((e, i) =>
											<div key={i} className="tag-input mr-2 mb-2">{e}<img src={wrongIcon} onClick={removeTag} className="ml-2 c-pointer"/></div>
										)}
										<Alert color="danger" className={isTagsAboveMax ? "d-block text-14 mt-2" : "d-none"}>Maximum tags amount is 4</Alert>
								</div>
							</div>
							<div className="mb-4">
								<div className="label mb-2">Your Experience</div>
								<FormGroup className="input-container mb-1">
									<Input type="number" innerRef={register} placeholder="Short description about your experience..." defaultValue={userInfos.exp} className="pr-6" name="exp" />
									<img src={checkedIcon} className="input-icon-2" />
								</FormGroup>
							</div>
							<div className="btn-container-right input-container d-flex">
								<Input type="submit" className="btn-save-setting" value="Save Profile" />
							</div>
							{userInfos.msg ? <Alert color="red">{userInfos.msg}</Alert> : ""}
						</div>
					</Form>
				</div>
			</div>
		);
}
