import React from 'react';
import { useSelector} from 'react-redux';
import { Redirect, Route } from 'react-router-dom';


const PrivateRoute = ({component: Component, ...rest}) => {
    const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

    if (isAuthenticated === null) {
        return null;
    }
    if (Component === null) {
        return null;    
    }
    return (
        <Route {...rest} render={props => (
            (isAuthenticated)?
                <Component {...props} />
            : <Redirect to="/login" />
        )} />
    );
};
 export default PrivateRoute;