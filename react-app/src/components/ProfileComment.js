import React, { useState, useEffect } from 'react';
import moment from 'moment';

import {disucsAllInfosThunk} from '../redux/discussion/thunk'
import {useDispatch, useSelector} from 'react-redux';
import { userInfosByVotesThunk } from '../redux/user/thunk';
import {projectAllInfosThunk} from '../redux/project/thunk';

function ProfileComment () {
	const dispatch = useDispatch();
	const allProjects = useSelector(state => state.proj.projInfos.allProject);
	const allProjComment = useSelector(state => state.proj.projInfos.projComments);
	const allProjCommentReply = useSelector(state => state.proj.projInfos.projCommentReplys);
	const allDiscussions = useSelector(state => state.discus.discusInfos.allDiscussion);
	const allDiscusComment = useSelector(state => state.discus.discusInfos.discusComments);
	const allDiscusCommentReply = useSelector(state => state.discus.discusInfos.discusCommentReplys);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const userInfos = useSelector(state => state.user.userInfos);

	useEffect(() => {
		dispatch(disucsAllInfosThunk());
		dispatch(projectAllInfosThunk());
		dispatch(userInfosByVotesThunk())
	},[dispatch])
	
	const [projects, setProjects] = useState([]);
	const [commentProjects, setCommentProjects] = useState([]);
	const [replyProjects, setReplyProjects] = useState([]);
	const [discussions, setDiscussions] = useState([]);
	const [commentDiscussions, setCommentDiscussions] = useState([]);
	const [replyDiscussions, setReplyDiscussions] = useState([]);
	const forceUpdate = React.useState()[1].bind(null, {});
	
	useEffect(() => {
		var dataDiscussion = allDiscussions;
		if(dataDiscussion !== undefined && dataDiscussion.length > 0) {
			var filteredData = dataDiscussion.filter( d => d.user_id === userInfos.id)
			setDiscussions(filteredData);
		}
	},[allDiscussions]);

	useEffect(() => {
		var dataProject = allProjects;
		if(dataProject !== undefined && dataProject.length > 0) {
			var filteredData = dataProject.filter( d => d.user_id === userInfos.id)
			setProjects(filteredData);
		}
	},[allProjects]);

	useEffect(() => {
		var dataDiscussionComments = allDiscusComment;
		if(dataDiscussionComments !== undefined && dataDiscussionComments.length > 0) {
			var filteredData = dataDiscussionComments.filter( d => d.user_id === userInfos.id);
			for(var i = 0; i < filteredData.length; i++){
				for(var j = 0; j < discussions.length; i++){
					if(discussions[j].id === filteredData.discus_id){
						filteredData[i].discussion = discussions[j];
					}
					console.log(filteredData[i], discussions[j], "?");
				}
			}
			setCommentDiscussions(filteredData);
			// console.log(filteredData, "?lol");
		}
	},[allDiscusComment])

	useEffect(() => {
		var dataProjComments = allProjComment;
		if(dataProjComments !== undefined && dataProjComments.length > 0) {
			var filteredData = dataProjComments.filter( d => d.user_id === userInfos.id)
			setCommentProjects(filteredData);
		}
	},[allProjComment])

	useEffect(() => {
		var dataDiscusCommentReply = allDiscusCommentReply;
		if(dataDiscusCommentReply !== undefined && dataDiscusCommentReply.length > 0) {
			var filteredData = dataDiscusCommentReply.filter( d => d.user_id === userInfos.id)
			setReplyDiscussions(filteredData);
		}
	},[allProjCommentReply]);

	useEffect(() => {
		var dataProjCommentReply = allProjCommentReply;
		if(dataProjCommentReply !== undefined && dataProjCommentReply.length > 0) {
			var filteredData = dataProjCommentReply.filter( d => d.user_id === userInfos.id)
			setReplyProjects(filteredData);
		}
	},[allProjCommentReply]);

	return (
		<div className="profile-content">
			<div className="text-20 mt-5 mb-4">Comments</div>
			<div className="comment-container">
			{
				commentDiscussions.map((comment, i) => 
					<div className="comments bg-white px-4 py-3 mb-3">
						<div className="comment-question text-20 mb-2">
							{discussions[i].title}	
						</div>
						<div className="comment-description text-gray mb-2">
							{discussions[i].description}
						</div>
						<div className="tags-container d-block mb-2">
							{ Array.isArray(discussions[i].tags) && discussions[i].tags.map((tag, j) =>
								<div key={j} className="tag mr-2 ">{tag}</div>
							)}
						</div>
						<div className="w-100 p-relative border-top my-3">
							<div className="d-flex align-items-center my-3">
								<img src={userInfos.avator} className="discussion-avatar mr-3" />
								<div className="d-flex">
									<div className="mr-3">
										<div className="text-16 mb-1">{userInfos.displayname}</div>
									</div>
									<div className="text-16 text-gray">
										{moment( new Date(comment.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
									</div>
								</div>
							</div>
							<div className="comment text-gray">
								{comment.description}
							</div>
							<div className="reply-btn px-3 py-2 mt-3">
								Reply
							</div>
						</div>
					</div>
				)
			}

			</div> 
	</div>
	);
}

export default ProfileComment;