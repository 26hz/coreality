import React from 'react';

import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';

function CommentInputReplyMobileModal(props){
	const {
		replyIndex,
		data,
		commentID,
		commentReplyUser,
		hideChildCommentModal,
		handleComment,
		submitComment,
	} = props;
	return (
		<div className="modal comment-modal d-block">
			<div className="modal-content comment-modal">
				<div onClick={hideChildCommentModal} className="close-modal-icon-container">
					<img src={wrongInvertedIcon} className="close-modal-icon"/>
				</div>
				<div className="text-16 my-4">Reply {commentReplyUser[replyIndex] && commentReplyUser[replyIndex].displayname}</div>
				<textarea className="mb-4" name="description" onChange={(e) => handleComment(e)} rows="4" />
				<div className="d-flex justify-content-end">
					<div className="btn-primary w-fit" 
						onClick={() => {
							submitComment("child", data.id, commentID);
							hideChildCommentModal();
						}}
					>REPLY</div>
				</div>
			</div>
		</div>
	);
}

export default CommentInputReplyMobileModal;