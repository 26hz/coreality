import React, { Component } from 'react';
import  { Link } from 'react-router-dom'

function SettingHeader (props) {
	return (
		<div className="header-setting bg-white d-flex justify-content-center">
			<Link to="/setting/profile">
				<button className={"text-14 text-center py-3 btn-nav-setting " + (props.displayContent === "profile" ? "active" : '')}>PROFILE	</button>
			</Link>
			<Link to="/setting/avatar">
				<button className={"text-14 text-center py-3 btn-nav-setting " + (props.displayContent === "avatar" ? "active" : '')}>AVATAR</button>
			</Link>
			<Link to="/setting/email">
				<button className={"text-14 text-center py-3 btn-nav-setting " + (props.displayContent === "email" ? "active" : '')}>EMAIL &#38; PASSWORD</button>
			</Link>
			{/* <Link to="/setting/account">
				<button className={"text-14 text-center py-3 btn-nav-setting " + (props.displayContent === "account" ? "active" : '')}>ACCOUNT</button>
			</Link> */}
		</div>		
	);
}

export default SettingHeader;