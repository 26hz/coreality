import moment from 'moment';
import { isMobile } from 'react-device-detect';

import CommentsList from '../components/CommentsList';

import avatar from '../assets/img/avatar.png';

import fireIcon from '../assets/icon/fire.png';
import editIcon from '../assets/icon/edit.png';

import purpleComment from '../assets/icon/purple-comment.png';
import purpleShare from '../assets/icon/purple-share.png';
import purpleUpvote from '../assets/icon/purple-upvote.png';

const DiscussionList = ({
	isAuthenticated,
	loggedUser,
	discussion,
	isFeatured,
	type,
	comments,
	reply,
	votes,
	authorUsers,
	commentUsers,
	commentReplyUsers,
	discusCommentCount,
	toggleUpvote,
	handleShare,
	displayMoreTags,
	displayComments,
	toggleComment,
	handleComment,
	submitComment,
	routeChange,
}) => {

	const avator = 	<img className="discussion-avatar mr-3" src={avatar} alt="default-avatar"/>
	const authAvator = <img className="discussion-avatar mr-3" src={loggedUser && loggedUser.avator} alt="user-avatar"/>

	return (
		<div>
			{ discussion.map((discussion, i) => 
				<div key={i}>
					<div className="discussion bg-white p-3 p-relative mobile-only mb-3" onClick={() => routeChange(discussion.id)}>
						<div className="d-flex align-items-center justify-content-between mb-3">
							<div className="d-flex align-items-center">
							{isFeatured ? 
								<img className="discussion-avatar mr-3" src={authorUsers !== undefined && authorUsers.featDiscusUsers !== undefined && authorUsers.featDiscusUsers[i] !== undefined && authorUsers.featDiscusUsers[i].avator} alt="default-avatar"/>
									: type === "discussion" ?
									<img className="discussion-avatar mr-3" src={authorUsers !== undefined && authorUsers.discusUsers !== undefined && authorUsers.discusUsers[i] !== undefined && authorUsers.discusUsers[i].avator} alt="default-avatar"/>
										: 
											avator
								}
								{isFeatured 
									? 
									<div className="text-16 mb-1 mr-3">{authorUsers !== undefined && authorUsers.featDiscusUsers !== undefined && authorUsers.featDiscusUsers[i] !== undefined && authorUsers.featDiscusUsers[i].displayname}</div>
										: type === "discussion" ?
										<div className="text-16 mb-1 mr-3">{authorUsers !== undefined && authorUsers.discusUsers !== undefined && authorUsers.discusUsers[i] !== undefined && authorUsers.discusUsers[i].displayname}</div>
											: 
											<div className="text-16 mb-1 mr-3">User Not Found</div>
								}
								<div className="text-16 text-gray">
									{moment( new Date(discussion.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
								</div>
							</div>
							{
								type==="featured" &&
								<img className="fire-icon" src={fireIcon} />
							}
						</div>
						<div className="discussion-question text-20 mb-2">{discussion.title}</div>
						<div className="discussion-description hide-in-mobile text-gray mt-2">{discussion.description}</div>
						<div className="tags-container d-block mobile-only-flex text-14 mt-3">
							{ discussion.tags.map((tag, j) =>
								<div key={j} className={"tag mr-2 " + (j > 1 ? "hide-in-mobile " : "")}>{tag}</div>
							)}
							<div className={"btn-more c-pointer tag px-3 " + (discussion.tags.length > 2 ? "mobile-only" : "d-none")} onClick={(e) => displayMoreTags(e, discussion.id, isFeatured)}>
								more
							</div>
						</div>
						<div className={"d-flex justify-content-between align-items-center mt-3 " + (discussion.showComment ? "pb-3 border-bottom" : "mb-1")}>
							<div className="d-flex">
								<div onClick={(e) => displayComments(e, discussion.id, isFeatured)} className={"comment-btn btn btn d-flex align-items-center mr-2 text-navy c-pointer " + (discussion.showComment ? "active" : "") }>
									<img src={purpleComment} className="preview-icon mr-2" />
									{isFeatured ? discusCommentCount && discusCommentCount.featCommentCountNums !== undefined && discusCommentCount.featCommentCountNums[i] : discusCommentCount && discusCommentCount.commentCountNums !== undefined && discusCommentCount.commentCountNums[i]}
								</div>
								<div onClick={(e) => handleShare(e, discussion.id, i, isFeatured)} className="share-btn btn d-flex align-items-center text-navy c-pointer">
									<img src={purpleShare} className="preview-icon mr-2" />
									<span className="text-12">Share</span>
								</div>
							</div>
							<div onClick={(e) => toggleUpvote(e, i, isFeatured)} className={"upvote-btn align-items-center text-navy c-pointer " + (votes[i] && votes[i][0].voted ? "active" : "") }>
								<img src={purpleUpvote} className="preview-icon mr-4" />
								{discussion.vote}
							</div>
						</div>
						<div className={"comment-list mt-3 " + (discussion.showComment ? "d-block" : "d-none")}>
							<CommentsList  
								isFeatured = {isFeatured}
								isAuthenticated = {isAuthenticated}
								loggedUser= {loggedUser}
								data = {discussion}
								comments = {comments}
								reply = {reply}
								commentUsers = {commentUsers}
								commentReplyUsers = {commentReplyUsers}
								type = {type}
								toggleComment = {toggleComment}
								handleComment = {handleComment}
								submitComment = {submitComment}
							/>
						</div>
					</div>
					<div className="hide-in-mobile discussion bg-white p-3 p-relative mb-3">
						<div className="d-flex align-items-center justify-content-between mb-3">
							<div className="d-flex align-items-center">
							{isFeatured ? 
								<img className="discussion-avatar mr-3" src={authorUsers !== undefined && authorUsers.featDiscusUsers !== undefined && authorUsers.featDiscusUsers[i] !== undefined && authorUsers.featDiscusUsers[i].avator !== undefined && authorUsers.featDiscusUsers[i].avator} alt="default-avatar"/>
									: type === "discussion" ?
									<img className="discussion-avatar mr-3" src={authorUsers !== undefined && authorUsers.discusUsers !== undefined && authorUsers.discusUsers[i] !== undefined && authorUsers.discusUsers[i].avator} alt="default-avatar"/>
										: 
											avator
								}
								<div className="d-flex">
									<div className="text-16 mr-3">
										{isFeatured ? 
											<div className="text-16 mr-3">{authorUsers !== undefined && authorUsers.featDiscusUsers !== undefined && authorUsers.featDiscusUsers[i] !== undefined && authorUsers.featDiscusUsers[i].displayname}</div>
											: type === "discussion" ?
											<div className="text-16 mr-3">{authorUsers !== undefined && authorUsers.discusUsers !== undefined && authorUsers.discusUsers[i] !== undefined && authorUsers.discusUsers[i].displayname}</div>
											: 
												avator
										}
									</div>
									<div className="text-16 text-gray">
										{moment( new Date(discussion.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
									</div>
								</div>
							</div>
							{
								type==="featured" &&
								<img className="fire-icon" src={fireIcon} />
							}
						</div>
						<div className="discussion-question text-20 mb-2">{discussion.title}</div>
						<div className="discussion-description hide-in-mobile text-gray mt-2">{discussion.description}</div>
						<div className="tags-container d-block mobile-only-flex text-14 mt-3">
							{ discussion.tags.map((tag, j) =>
								<div key={j} className={"tag mr-2 " + (j > 1 ? "hide-in-mobile" : "")}>{tag}</div>
							)}
							<div className={"btn-more c-pointer tag px-3 " + (discussion.tags.length > 2 ? "mobile-only" : "d-none")} onClick={(e) => displayMoreTags(e, discussion.id, isFeatured)}>
								more
							</div>
						</div>
						<div className={"d-flex justify-content-between align-items-center mt-3 " + (discussion.showComment ? "pb-3 border-bottom" : "mb-1")}>
							<div className="d-flex">
								<div onClick={(e) => displayComments(e, discussion.id, isFeatured)} className={"comment-btn btn btn d-flex align-items-center mr-2 text-navy c-pointer " + (discussion.showComment ? "active" : "") }>
									<img src={purpleComment} className="preview-icon mr-2" />
									{isFeatured && discusCommentCount !== undefined && discusCommentCount.featCommentCountNums !== undefined && discusCommentCount.featCommentCountNums[i] !== undefined ? discusCommentCount.featCommentCountNums[i] : discusCommentCount !== undefined && discusCommentCount.commentCountNums !== undefined && discusCommentCount.commentCountNums[i] !== undefined ? discusCommentCount.commentCountNums[i] : null}
								</div>
								<div onClick={(e) => handleShare(e, discussion.id, i, isFeatured)} className="share-btn btn d-flex align-items-center text-navy c-pointer">
									<img src={purpleShare} className="preview-icon mr-2" />
									<span className="text-12">Share</span>
								</div>
							</div>
							<div onClick={(e) => toggleUpvote(e, i, isFeatured)} className={"upvote-btn align-items-center text-navy c-pointer " +  (votes[i] && votes[i][0].voted && "active") }>
								<img src={purpleUpvote} className="preview-icon mr-4" />
								{discussion.vote}
							</div>
						</div>
						<div className={discussion.showComment && !isMobile ? "d-block bg-white" : "d-none"}>
							<div className="form-input-discussion-container d-flex hide-in-mobile align-items-center mt-3 mb-6 ">
							{isAuthenticated? (loggedUser.avator? authAvator: avator) : avator}
								<div className="input-discussion-container p-relative">
									<img src={editIcon} className="pen-discussion-input"/>
									<input type="text" name="myComment" className="input-discussion" onChange={(e) => handleComment(e)} placeholder="Start a new comment..." />
								</div>
								<button className="btn-inverted p-2 ml-2" onClick={(e) => submitComment("parent", discussion.id, null, null)}>
									Send
								</button>
							</div>
							<div className="comment-list">
								<CommentsList  
									isFeatured = {isFeatured}
									isAuthenticated = {isAuthenticated}
									loggedUser= {loggedUser}
									data = {discussion}
									comments = {comments}
									reply = {reply}
									commentUsers = {commentUsers}
									commentReplyUsers = {commentReplyUsers}
									type = {type}
									toggleComment = {toggleComment}
									handleComment = {handleComment}
									submitComment = {submitComment}
								/>
							</div>
						</div>
					</div>
				</div>
			)}
		</div>
	);
}

export default DiscussionList;