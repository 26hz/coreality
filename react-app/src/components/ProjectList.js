import React, {useState} from 'react';
import { isMobile } from 'react-device-detect';

import CommentsList from '../components/CommentsList';
import CommentInputReplyMobileModal from '../components/CommentInputReplyMobileModal';
import CommentInputMobileModal from '../components/CommentInputMobileModal';
import DetailProjectModal from '../components/DetailProjectModal';

import avatar from '../assets/img/avatar.png';

import fireIcon from '../assets/icon/fire.png';
import editIcon from '../assets/icon/edit.png';

import purpleComment from '../assets/icon/purple-comment.png';
import purpleShare from '../assets/icon/purple-share.png';
import purpleUpvote from '../assets/icon/purple-upvote.png';

const ProjectList = ({
	isAuthenticated,
	loggedUser,
	projects,
	type,
	isFeatured,
	comments,
	reply,
	votes,
	authorUsers,
	commentUsers,
	commentReplyUsers,
	projCommentCount,
	toggleUpvote,
	handleShare,
	displayMoreTags,
	displayDetail,
	preventDetail,
	displayComments,
	toggleComment,
	handleComment,
	submitComment,
	isProjectDetailShown,
	isDisplayShare,
	handleCloseShare,
	hideDetail,
	dataDetailModal,
}) => {
	const avator = 	<img className="discussion-avatar mr-3" src={avatar} alt="default-avatar"/>
	const authAvator = <img className="discussion-avatar mr-3" src={loggedUser && loggedUser.avator} alt="user-avatar"/>
	const [commentID, setCommentID] = useState(null);
	const [replyIndex, setReplyIndex] = useState(null);
	const [isCommentParentModalDisplay, setIsCommentParentModalDisplay] = useState(false);
	const [isCommentChildModalDisplay, setIsCommentChildModalDisplay] = useState(false);

	const displayParentCommentModal = (e) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		setIsCommentParentModalDisplay(true);
	}

	const hideParentCommentModal = () => {
		setIsCommentParentModalDisplay(false);
	}

	const displayChildCommentModal = (id, replyIndex) => {
		if(isMobile){
			setCommentID(id);
			setReplyIndex(replyIndex);
			setIsCommentChildModalDisplay(true);
		}
	}
	
	const hideChildCommentModal = () => {
		setIsCommentChildModalDisplay(false);
	}

	return (
		<div className="homeProjects">
			{
				isProjectDetailShown &&
				<DetailProjectModal
					isAuthenticated = {isAuthenticated}
					loggedUser = {loggedUser}
					isFeatured={isFeatured}
					type={type}
					data = {dataDetailModal}
					comments = {comments}
					reply = {reply}
					votes = {votes}
					projCommentCount={projCommentCount}
					authorUsers = {authorUsers}
					commentUsers = {commentUsers}
					commentReplyUsers = {commentReplyUsers}
					isDisplayShare = {isDisplayShare}
					handleShare = {handleShare}
					toggleUpvote = {toggleUpvote}
					hideDetail = {hideDetail}
					hideShare = {handleCloseShare}
					toggleComment = {toggleComment}
					handleComment = {handleComment}
					submitComment = {submitComment}
					displayMoreTags = {displayMoreTags}
				/>
			}
			{projects && projects.map((project, i) => 
				<div key={i}>
					{
						isCommentParentModalDisplay &&
						<CommentInputMobileModal
							data = {project}
							commentUser = {commentUsers}
							handleComment = {handleComment}
							submitComment = {submitComment}
							hideParentCommentModal = {hideParentCommentModal}
						/>
					}

					{
						isCommentChildModalDisplay &&
						<CommentInputReplyMobileModal
							data = {project}
							commentID = {commentID}
							replyIndex = {replyIndex}
							commentReplyUser = {commentReplyUsers}
							commentUsers = {commentUsers}
							handleComment = {handleComment}
							submitComment = {submitComment}
							hideChildCommentModal = {hideChildCommentModal}
						/>
					}
					<div className="mobile-only bg-white">
						<div className="preview-container mb-3 d-block align-items-end p-3 m-0 c-pointer" onClick={() => displayDetail(isFeatured, i)}>
							<div className="d-flex">
								<img className="new-project-thumbnail mr-3" src={project.imgThumb} alt="project-Thumbnail"/>
								<div>
									<div>
										<div className="text-16 mb-2">{project.title}</div>
										<div className="text-gray text-14 mb-2">{project.headline}</div>
									</div>
									{
										isFeatured==="featured" &&
										<img src={fireIcon} className="edit-icon-profile-discussion fire-icon" alt="coreality-fireIcon"/>
									}
								</div>
							</div>
							<div className="w-100">
								<div className="tags-container d-block mobile-only-flex text-14 mt-3">
									{ Array.isArray(project.tags) && project.tags.map((tag, j) =>
										<div key={j} className={"tag mr-2 " + (j > 1 ? "hide-in-mobile " : "")}>{tag}</div>
									)}
									<div className={"btn-more c-pointer tag px-3 " + ((project.tags?project.tags.length > 2:false) ? "mobile-only" : "d-none")} onClick={(e) => displayMoreTags(e, project.id, isFeatured)}>
										more
									</div>
								</div>
								<div className="d-flex justify-content-between align-items-center">
									<div className="d-flex"> 
										<div className="comment-btn btn d-flex mr-2 text-navy c-pointer" onClick={(e) => displayComments(e, project.id, isFeatured)}>
											<img src={purpleComment} className="preview-icon mr-2" alt="corealty-commentBtn"/>
											{ isFeatured ? projCommentCount && projCommentCount.featCommentCountNums[i] : projCommentCount && projCommentCount.commentCountNums[i]}
										</div>
										<div onClick={(e) => handleShare(e, project.id, i, isFeatured)} className="share-btn btn d-flex text-navy c-pointer">
											<img src={purpleShare} className="preview-icon mr-2" alt="coreality-shareBtn"/>
											<span className="text-12">Share</span>
										</div>
									</div>
									<div className={"text-navy upvote-btn c-pointer "+ (votes[i] !== undefined && votes[i][0].voted && "active")} onClick={(e) => toggleUpvote(e, i, isFeatured)}>
										<img src={purpleUpvote} className="preview-icon mr-2" alt="coreality-VoteBtn"/>
										{project.vote}
									</div>
								</div>
							</div>
							<div onClick={(e) => {displayParentCommentModal(e, project.id)}} className={"mt-4 c-default " + (project.showComment ? "d-block": "d-none")}>
								<div className="form-input-discussion-container d-flex align-items-center mb-6 ">
										{isAuthenticated? (loggedUser.avator? authAvator: avator) : avator}
									<div className="input-discussion-container p-relative">
										<input type="text" name="myComment" className="input-discussion" onChange={(e) => handleComment("project", e, i)} placeholder="Start a new comment..." />
									</div>
								</div>
								<div onClick={(e) => preventDetail(e)} className="comment-list">
									<CommentsList 
										index = {i}
										isFeatured={isFeatured}
										isAuthenticated = {isAuthenticated}
										loggedUser= {loggedUser}
										data = {project}
										comments = {comments}
										reply = {reply}
										type={type}
										commentUsers = {commentUsers}
										commentReplyUsers = {commentReplyUsers}
										displayChildCommentModal={displayChildCommentModal}
										toggleComment = {toggleComment}
										handleComment = {handleComment}
										submitComment = {submitComment}
									/>
								</div>
							</div>
						</div>
					</div>
					<div className="hide-in-mobile">
						<div className="preview-container p-3 mb-3 mt-0 bg-white c-pointer" onClick={() => displayDetail(isFeatured, i)}>
							<div className="d-flex align-items-center">
								<img className="new-project-thumbnail mr-3" src={project.imgThumb} alt="project-Thumbnail"/>
								<div className="w-100" >
									<div className="d-flex align-items-center justify-content-between">
										<div className="text-16 mb-2">{project.title}</div>
										{
											type==="featured" &&
											<img className="fire-icon" src={fireIcon} alt="coreality-FireIcon"/>
										}
									</div>
									<div className="text-gray text-14 mb-2">{project.headline}</div>
									<div className="tags-container d-block text-14 mt-3">
										{ Array.isArray(project.tags) && project.tags.map((tag, j) =>
											<div key={j} className="tag mr-2">{tag}</div>
										)}
									</div>
									<div className="d-flex justify-content-between align-items-center">
										<div className="d-flex"> 
											<div onClick={(e) => displayComments(e, project.id, isFeatured)} className="comment-btn btn d-flex mr-2 text-navy c-pointer">
												<img src={purpleComment} className="preview-icon mr-2" alt="coreality-CommentBtn"/>
												{ isFeatured ? projCommentCount && projCommentCount.featCommentCountNums[i] : projCommentCount && projCommentCount.commentCountNums[i]}
											</div>
											<div onClick={(e) => handleShare(e, project.id, i, isFeatured)} className="share-btn btn d-flex text-navy c-pointer">
												<img src={purpleShare} className="preview-icon mr-2" alt="coreality-ShareBtn"/>
												<span className="text-12">Share</span>
											</div>
										</div>
											<div className={"text-navy upvote-btn c-pointer " + (votes[i] !== undefined && votes[i][0].voted ? "active" : "")} onClick={(e) => toggleUpvote(e, i, isFeatured)}>
												<img src={purpleUpvote} className="preview-icon mr-2" alt="coreality-VoteBtn"/>
												{project.vote}
											</div>
									</div>
								</div>
							</div>
							<div onClick={(e) => preventDetail(e)} className={"c-default " + (project.showComment ? "d-block" : "d-none")}>
								<div className="form-input-discussion-container d-flex hide-in-mobile align-items-center mt-3 mb-6 ">
								{isAuthenticated? (loggedUser.avator? authAvator: avator) : avator}
									<div className="input-discussion-container p-relative">
										<img src={editIcon} className="pen-discussion-input" alt="coreality-editBtn"/>
										<input type="text" name="myComment" className="input-discussion" onChange={(e) => handleComment(e)} placeholder="Start a new comment..." />
									</div>
									<button className="btn-inverted p-2 ml-2" onClick={() => submitComment("parent", project.id)}>
										Send
									</button>
								</div>
								<div className="comment-list">
									<CommentsList 
										index = {i}
										isFeatured={isFeatured}
										isAuthenticated = {isAuthenticated}
										loggedUser= {loggedUser}
										data = {project}
										comments = {comments}
										reply = {reply}
										type={type}
										commentUsers = {commentUsers}
										commentReplyUsers = {commentReplyUsers}
										displayChildCommentModal={displayChildCommentModal}
										toggleComment = {toggleComment}
										handleComment = {handleComment}
										submitComment = {submitComment}
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
				)
			}
		</div>
	);
}

export default ProjectList;