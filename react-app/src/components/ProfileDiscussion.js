import React, { useState, useEffect } from 'react';
import {updateDiscusVoteThunk} from '../redux/vote/thunk';
import {discusInfosThunk, createDiscusCommentThunk, createDiscusCommentReplyThunk} from '../redux/discussion/thunk'
import {useDispatch, useSelector} from 'react-redux';
import { userInfosByVotesThunk } from '../redux/user/thunk';
import {useParams} from 'react-router-dom';

import DiscussionList from '../components/DiscussionList';
import ShareDialog from "../components/ShareDialog";
import MoreTagsModal from "../components/MoreTagsModal";

function ProfileDiscussion () {
	const dispatch = useDispatch();
	const allDiscussions = useSelector(state => state.discus.discusInfos.allDiscussion);
	const allDiscusUsers = useSelector(state => state.discus.discusInfos.allDiscusUsers);
	const allDiscusComment = useSelector(state => state.discus.discusInfos.discusComments);
	const allDiscusCommentsUsers = useSelector(state => state.discus.discusInfos.allDiscusCommentsUsers);
	const allDiscusCommentReply = useSelector(state => state.discus.discusInfos.discusCommentReplys);
	const allDiscusCommentReplysUsers = useSelector(state => state.discus.discusInfos.allDiscusCommentReplysUsers);
	const discusCommentCount = useSelector(state => state.discus.discusInfos.commentCount);
	const discusVotes = useSelector(state => state.discus.discusInfos.discusVotes);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const userInfos = useSelector(state => state.user.userInfos);
	let param = useParams();
	useEffect(() => {
		dispatch(discusInfosThunk(param));
		dispatch(userInfosByVotesThunk())
	},[dispatch])
	
	const [description, setDescription] = useState("");
	const [isDisplayShare, setShareShown] = useState(false);
	const [isMoreTagsShown, setTagShown] = useState(false);
	const [dataTags, setDataTags] = useState([]);
	const [shareData, setShareData] = useState({});
	const [discussions, setDiscussions] = useState([]);
	const [discussionUsers, setDiscussionUsers] = useState([]);
	const [comments, setComments] = useState([]);
	const [commentUsers, setCommentUsers] = useState([]);
	const [commentsCount, setCommentsCount] = useState([]);
	const [reply, setReply] = useState([]);
	const [replyUsers, setReplyUsers] = useState([]);
	const [votes, setVotes] = useState([]);
	const forceUpdate = React.useState()[1].bind(null, {});
	
	useEffect(() => {
		var dataDiscussion = allDiscussions;
		var tempData;
		if(dataDiscussion !== undefined && dataDiscussion.length > 0) {
			for(var i = 0; i < dataDiscussion.length; i++){
				if(discussions.length > 0){
					if(discussions[i].hasOwnProperty("showComment"))
						dataDiscussion[i].showComment = discussions[i].showComment;
				} else {
					tempData = {...dataDiscussion[i], ...{['showComment'] : false}};
					dataDiscussion[i] = tempData
				}
			}
			setDiscussions(dataDiscussion);
		}
		setDiscussionUsers(allDiscusUsers);
		setCommentsCount(discusCommentCount);
	},[allDiscussions])

	var once = true;
	useEffect(() => {
		if(once && discussions.length > 0){
			once = false;
			sortLatest();
		}
	},[discussions]);

	useEffect(() => {
		var dataDiscussionComments = allDiscusComment;
		if(dataDiscussionComments !== undefined && dataDiscussionComments.length > 0) {
			dataDiscussionComments && dataDiscussionComments.map((data, j) => {
				if(comments !== undefined && comments.length > 0){
					if(comments[j].hasOwnProperty("showInput"))
						dataDiscussionComments[j].showInput = comments[j].showInput;
				} else {
					dataDiscussionComments[j]= {...dataDiscussionComments[j], ...{['showInput'] : false}};
				}
			})
		}
		setComments(dataDiscussionComments);
		setCommentUsers(allDiscusCommentsUsers);
	},[allDiscusComment])

	useEffect(() => {
		setReply(allDiscusCommentReply);
		setReplyUsers(allDiscusCommentReplysUsers);
	},[allDiscusCommentReply])

	useEffect(() => {
		if(discusVotes !== undefined && discusVotes.length > 0) {
			setVotes(discusVotes);
		}
	},[discusVotes])

	const toggleUpvote = (e, index) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data = discusVotes;
		let newObj;
		if(data[index].length > 0){
			newObj = data[index][0];
			newObj.voted = !newObj.voted;
		} else {
			data[index].push({});
			newObj =  data[index][0];
			newObj.voted = true;
		}
		var discussion = discussions;
		if(newObj.voted){
			discussion[index].vote = parseInt(discussion[index].vote) + 1;
		} else {
			discussion[index].vote = parseInt(discussion[index].vote) - 1;
		}
		dispatch(updateDiscusVoteThunk(newObj.voted, discussion[index].id));
		setDiscussions(discussion);
		forceUpdate();
	}
	
	const handleShare = (e, id, index, type) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		if (navigator.share) {
			navigator.share({
				title: 'Coreality',
				url: window.location.href + "/discussions/" + id
			}).then(() => {
				console.log('Thanks for sharing!');
			})
			.catch(console.error);
		} else {
			var newObj = discussions[index];
			setShareShown(true);
			setShareData(newObj);
		}
	}
	
	const handleCloseShare = () => {
		setShareShown(false);
	}
	
	const displayMoreTags = (id) => {
		var data = discussions;
		const elementsIndex = data.findIndex(element => element.id === id );
		setTagShown(true);
		setDataTags(data[elementsIndex]);
	}
	
	const closeTagsModal = () => {
		setTagShown(false)
	}
	
	const sortLatest = () => {
		var data = discussions;
		var discusUser = discussionUsers;
		var comment = comments;
		var commentUser = commentUsers;
		var commentCount = commentsCount;
		var replies = reply;
		var replyUser = replyUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (new Date(data[i - 1].created_at) - Date.now() < new Date(data[i].created_at) - Date.now()) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//discussion author
					if(discusUser.discusUsers !== undefined){
						temp = discusUser.discusUsers[i - 1];
						discusUser.discusUsers[i - 1] = discusUser.discusUsers[i];
						discusUser.discusUsers[i] = temp;
					}
					//comments users
					if(commentUser.allDiscusCommentUsers[i]){
						temp = commentUser.allDiscusCommentUsers[i - 1];
						commentUser.allDiscusCommentUsers[i - 1] = commentUser.allDiscusCommentUsers[i];
						commentUser.allDiscusCommentUsers[i] = temp;
					}
					//coments
					if(comment[i]){
						temp = comment[i - 1];
						comment[i - 1] = comment[i];
						comment[i] = temp;
					}
					//replies
					if(replies[i]){
						temp = replies[i - 1];
						replies[i - 1] = replies[i];
						replies[i] = temp;
					}
					//reply users
					if(replyUser.allDiscusCommentUsersReplys[i]){
						temp = replyUser.allDiscusCommentUsersReplys[i - 1];
						replyUser.allDiscusCommentUsersReplys[i - 1] = replyUser.allDiscusCommentUsersReplys[i];
						replyUser.allDiscusCommentUsersReplys[i] = temp;
					}
					//comment counts
					if(commentCount !== undefined){
						temp = commentCount.commentCountNums[i - 1];
						commentCount.commentCountNums[i - 1] = commentCount.commentCountNums[i];
						commentCount.commentCountNums[i] = temp;
					}
				}
			}
		}
		setDiscussions(data);
		setDiscussionUsers(discusUser);
		setComments(comment);
		setCommentUsers(commentUser);
		setReply(replies);
		setReplyUsers(replyUser);
		setCommentsCount(commentCount);
		forceUpdate();
	}

	const displayComments = (e, id) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data = discussions;
		var index = data.findIndex(x => x.id === id);
		data[index].showComment = !data[index].showComment;
		setDiscussions(data);
		forceUpdate();
	}
	
	const toggleComment = (j, k, type) => {
		var data = comments;
		data[j][k].showInput = !data[j][k].showInput;
		setComments(data);
		forceUpdate();
	}
	
	const handleComment = (event) => {
		setDescription(event.target.value);
	}
	
	const submitComment = async (placename, discus_id, comment_id) => {
		if(placename === "child"){
			dispatch(createDiscusCommentReplyThunk(description, discus_id, comment_id));
		} else if(placename === "parent"){
			dispatch(createDiscusCommentThunk(description, discus_id));
		}
		setTimeout(() => { dispatch(discusInfosThunk()); }, 500);
		setDescription("");
		forceUpdate();
	}

	return (
		<div className="profile-content">
			{
				isDisplayShare &&
				<ShareDialog 
					data = {shareData}
					hideShare = {handleCloseShare}
				/>
			}

			{
				isMoreTagsShown &&
				<MoreTagsModal 
					data = {dataTags}
					hideModal = {closeTagsModal}
				/>
			}

			<div className="text-20 mt-5 mb-4">Discussions</div>
			<div className="discussion-container">
				<DiscussionList 
					discussion={discussions}
					isAuthenticated={isAuthenticated}
					loggedUser={userInfos}
					authorUsers = {discussionUsers}
					comments={comments}
					commentUsers = {allDiscusCommentsUsers}
					commentReplyUsers = {allDiscusCommentReplysUsers}
					reply={reply}
					votes={votes}
					discusCommentCount={commentsCount}
					type="discussion"
					toggleUpvote={toggleUpvote}
					displayComments = {displayComments}
					toggleComment = {toggleComment}
					handleComment = {handleComment}
					submitComment = {submitComment}
					handleShare={handleShare}
					handleCloseShare={handleCloseShare}
					displayMoreTags={displayMoreTags}
					isDisplayShare={isDisplayShare}
				/>
			</div>
	</div>
	);
}

export default ProfileDiscussion;