import moment from 'moment';
import purpleShare from '../assets/icon/purple-share.png';
import whiteUpvote from '../assets/icon/white-upvote.png';
import avatar from '../assets/img/avatar.png';
import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';
import InnerHTML from 'dangerously-set-html-content'

function DetailArticleModal (props) {
	
	const {
		data,
		avator,
		articleUsers,
		votes,
		toggleUpvote,
		handleShare,
		hideDetail,
		displayMoreTags,
	} = props;

	return (
		<div>
			<div className="modal d-block">
				<div className="modal-content project-detail">
					<div className="close-modal-icon-container mb-4">
						<img onClick={() => hideDetail(data.id)}  src={wrongInvertedIcon} className="close-modal-icon"/>
					</div>
					<div className="article-detail-container bg-white my-4 margin-auto p-5">
						<div className="d-flex justify-content-between">
							<div className="project-detail-content bg-white-border">
								<div className="project-detail-intro m-3 preview-container mb-0 d-flex align-items-center">
									<div className="mobile-only-flex align-items-center p-relative mb-4 w-100">
										<img src={articleUsers[data.index] !== undefined && articleUsers[data.index].avator ? articleUsers[data.index].avator : avatar} className="discussion-avatar mr-3" />
										<div className="d-flex">
											<div className="text-16 mr-3">{articleUsers[data.index] !== undefined && articleUsers[data.index].displayname}</div>
											<div className="text-16 text-gray">{moment( new Date(data.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}</div>
										</div>
									</div>
									<div className="w-100 hide-in-mobile">
										<div className="text-16 mb-2">{data.title}</div>
										<div className="text-gray text-14">{data.description}</div>
										<div className="d-flex align-items-center justify-content-between">
											<div className="tags-container d-block text-14 my-3 hide-in-mobile">
												{ data.tags && data.tags.map((tag, j) =>
													<div key={j} className={"tag mr-2 " + (j > 2 ? "d-none" : "")}>{tag}</div>
												)}
												<div className={"btn-more tag c-pointer px-3 " + ( data.tags && data.tags.length > 2 ? "" : "d-none" )} onClick={() => displayMoreTags("modal", data.id, data.isFeatured)}>
													more
												</div>
											</div>
											<div className="d-flex justify-content-between align-items-center project-detail-button-container">
												<div onClick={() => handleShare("modal", data.id, data.index)} className="share-btn btn d-flex text-navy c-pointer mr-2">
													<img src={purpleShare} className="preview-icon mr-2" />
													SHARE
												</div>
												<div className={"text-navy upvote-btn c-pointer "+ ((votes[data.index] !== undefined && votes[data.index][0].voted) ? "active" : "")} onClick={() => toggleUpvote("modal", data.index, false)}>
													<img src={whiteUpvote} className="preview-icon mr-2" />
													{data.vote}
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="detail-project-col-1 detail-project-cols pb-5 px-5">
								<InnerHTML html={data.content} />
								</div>
							</div>
							<div className="tags-container text-14 mt-3 w-100 mobile-only-flex mb-5">
								{ data.tags && data.tags.map((tag, j) =>
									<div key={j} className={"tag mr-2 w-100 " + (j < 3 ? "" : "d-none")}>{tag}</div>
								)}
								<div className={"btn-more tag c-pointer px-3 w-100 " + ( data.tags && data.tags.length > 1 ? "d-none" : "" )} onClick={() => displayMoreTags("modal", data.id, data.isFeatured)}>
									more
								</div>
							</div>
							<div className="justify-content-between align-items-center mobile-only-flex mb-6">
								<div onClick={() => handleShare("modal", data.id, data.index)} className="share-btn btn d-flex text-navy c-pointer mr-2">
									<img src={purpleShare} className="preview-icon mr-2" />
									SHARE
								</div>
								<div className={"text-navy upvote-btn c-pointer "+ ((votes[data.index] !== undefined && votes[data.index].voted) ? "active" : "")} onClick={() => toggleUpvote("modal", data.index, false)}>
									<img src={whiteUpvote} className="preview-icon mr-2" />
									{data.vote}
								</div>
							</div>
							<div className="ml-2 hide-in-mobile detail-project-col-2 detail-project-cols pt-0 px-3">
								<div className="bg-white-border p-3 user-info-container ">
									<div className="d-flex align-items-center mb-3">
										<img src={articleUsers && articleUsers[data.index] !== undefined && articleUsers[data.index].avator ? articleUsers[data.index].avator : avatar} className="discussion-avatar mr-3" />
											<div className="text-16 mb-1 mr-3">{articleUsers && articleUsers[data.index] !== undefined && articleUsers[data.index].displayname}</div>
									</div>
									<div className="text-16 text-gray">
										{articleUsers[data.index] !== undefined && articleUsers[data.index].headline}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default DetailArticleModal;