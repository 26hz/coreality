import React, { Component } from 'react';

function SettingAccount () {
	return (
		<div className="setting-profile-container">
			<div className="text-20 mt-6 mb-5">Account</div>
			<div className="bg-white bg-white-border p-5 mb-5">
				<div>
					<div className="font-bold text-16 mb-4">Details</div>
					<div className="text-16 mb-2">Member since</div>
					<div className="mb-5 text-gray mt-1">December 21st 2020</div>
				</div>
				
				<div className="mb-5">
					<div className="font-bold text-16 mb-2">Search Engines</div>
					<div className="input-container mt-1">
						<input
							className="mr-2"
							name="hideProfile"
							type="checkbox"
							defaultChecked={false} 
						/>
							Hide my profile from search engines
					</div>
				</div>

				<div className="mb-5">
					<div className="font-bold text-16 mb-2">Data</div>
					<div className="text-14 text-gray mt-1">
						If you would like your data deleted or would like access to the personal data Product Hunt holds about you, please contact us at <span className="text-navy">example@exmpl.com</span>
					</div>
				</div>

				<div className="mb-6">
					<div className="font-bold text-16 mb-2">Account</div>
					<div className="mt-1 mb-2 text-16">Sign out of all other sessions</div>
					<div className="text-gray text-14 mb-4">This will sign out of sessions in other browsers or on other computers.</div>
					<div className="text-navy c-pointer">SIGN OUT</div>
				</div>

				<div>
					<div className="font-bold text-16 mb-2 text-red">Delete account</div>
					<div className="mt-1 mb-3">Permanently delete your account and all of your content. Irreversible action</div>
					<div className="text-navy c-pointer">DELETE ACCOUNT</div>	
				</div>

			</div>
		</div>
	);
}

export default SettingAccount;