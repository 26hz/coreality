import React from 'react';

import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';

function MoreTagsModal(props){
	const {
		data,
		hideModal,
	} = props;
	return (
		<div className="modal tags-modal d-block">
			<div className="modal-content tags-modal">
				<div onClick={hideModal} className="close-modal-icon-container mb-4">
					<img src={wrongInvertedIcon} className="close-modal-icon"/>
				</div>
				<div className="text-24 my-4">Tags of "{data.title}"</div>
				<div className="tags-container d-block text-14 mt-3">
				{ data.tags && data.tags.map((tag, j) =>
					<div key={j} className="tag w-100 p-2 mr-2">{tag}</div>
				)}
				</div>
			</div>
		</div>
	);
}

export default MoreTagsModal;