import moment from 'moment';

import DetailArticleModal from '../components/DetailArticleModal';

import avatar from '../assets/img/avatar.png';

import fireIcon from '../assets/icon/fire.png';

import purpleShare from '../assets/icon/purple-share.png';
import purpleUpvote from '../assets/icon/purple-upvote.png';

const ArticleList = ({
	articles,
	avator,
	isFeatured,
	articleUsers,
	shareData,
	isDetailShown,
	isMoreTagsShown,
	isDisplayShare,
	data,
	votes,
	toggleUpvote,
	handleShare,
	handleCloseShare,
	displayMoreTags,
	closeTagsModal,
	displayDetail,
	hideDetail,
}) => {

	return (
		<div>
			{
				isDetailShown &&
				<DetailArticleModal
					data = {data}
					avator ={avator}
					articleUsers = {articleUsers}
					shareData = {shareData}
					isMoreTagsShown = {isMoreTagsShown}
					isDisplayShare = {isDisplayShare}
					votes = {votes}
					isDetailShown = {isDetailShown}
					toggleUpvote={toggleUpvote}
					handleShare={handleShare}
					hideShare = {handleCloseShare}
					displayMoreTags={displayMoreTags}
					closeTagsModal = {closeTagsModal}
					displayDetail={displayDetail}
					hideDetail = {hideDetail}
				/>
			}
			{ articles && articles.map((article, i) => 
				<div key={i} className="bg-white c-pointer my-3" onClick={() => displayDetail(isFeatured, i)}>
					<div className="d-flex hide-in-tablet preview-article-profile-container">
						<div className="mobile-only-flex justify-content-center p-relative">
							<div className="align-items-center d-flex w-100 p-3">
								<img src={avatar} className="discussion-avatar mr-3" />
								<div className="d-flex">
									<div className="text-16 mr-3">{articleUsers[i] !== undefined && articleUsers[i].displayname}</div>
									<div className="text-16 text-gray">
										{moment( new Date(article.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
									</div>
								</div>
							</div>
							{
								isFeatured &&
								<img src={fireIcon} className="edit-icon-profile-discussion mobile-only fire-icon" />
							}
						</div>
						<div className="article-thumbnail-container mr-3">
							<img className="article-thumbnail" src={article.imgThumb} />
						</div>
						<div className="article-top-profile p-relative w-100 py-3 pr-3">
							{
								isFeatured &&
								<img src={fireIcon} className="edit-icon-profile-discussion hide-in-mobile fire-icon" />
							}
							<div className="text-16 mb-2">{article.title}</div>
							<div className="text-14 text-gray mt-1 mb-3">{article.description}</div>
							<div className="tags-container d-block mobile-only-flex text-14 mt-3">
								{ article.tags && article.tags.map((tag, j) =>
									<div key={j} className={"tag mr-2 " + (j > 1 ? "hide-in-mobile" : "")}>{tag}</div>
								)}
								<div className={"btn-more c-pointer tag px-3 " + ((article.tags?article.tags.length > 2:true) ? "mobile-only" : "d-none")} onClick={(e) => displayMoreTags(e, article.id, isFeatured)}>
									more
								</div>
							</div>
							<div className="d-flex align-items-center hide-in-mobile mb-3">
								<img src={articleUsers[i] !== undefined && articleUsers[i].avator ? articleUsers[i].avator : avatar} className="discussion-avatar mr-3" />
								<div className="d-flex">
									<div className="text-16 mr-3">{articleUsers[i] !== undefined && articleUsers[i].displayname}</div>
										{moment( new Date(article.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
									<div className="text-16 text-gray"></div>
								</div>
							</div>
							<div className="justify-content-between align-items-center d-flex">
								<div onClick={(e) => handleShare(e, article.id, i, isFeatured)} className="share-btn btn d-flex text-navy c-pointer mr-2">
									<img src={purpleShare} className="preview-icon mr-2" />
									SHARE
								</div>
								<div className={"text-navy upvote-btn c-pointer "+ (votes[i].length > 0 && votes[i][0].voted ? "active" : "")} onClick={(e) => toggleUpvote(e, i, isFeatured)}>
									<img src={purpleUpvote} className="preview-icon mr-2" />
									{article.vote}
								</div>
							</div>
						</div>
					</div>
					<div className="tablet-only">
						<div className="d-flex p-relative">
							<div className="article-thumbnail-container mr-3">
								<img className="article-thumbnail" src={article.imgThumb} />
							</div>
							{
								isFeatured &&
								<img src={fireIcon} className="edit-icon-profile-discussion hide-in-mobile fire-icon pr-4" />
							}
							<div>
								<div className="text-16 mb-2">{article.title}</div>
								<div className="text-14 text-gray mt-1 mb-3">{article.description}</div>
							</div>
						</div>
						<div className="article-top-profile p-relative w-100 py-3 pr-3">
							<div className="tags-container d-block mobile-only-flex text-14 mt-3">
								{ Array.isArray(article.tags) && article.tags.map((tag, j) =>
									<div key={j} className={"tag mr-2 " + (j > 0 ? "hide-in-mobile " : "")}>{tag}</div>
								)}
								<div className="mobile-only btn-more c-pointer tag px-3" onClick={(e) => displayMoreTags(e, article.id, isFeatured)}>
									more
								</div>
							</div>
							<div className="d-flex align-items-center hide-in-mobile mb-3">
								<img src={avatar} className="discussion-avatar mx-3" />
								<div className="d-flex">
									<div className="text-16 mr-3">{articleUsers[i] !== undefined && articleUsers[i].displayname}</div>
									<div className="text-16 text-gray">{moment( new Date(article.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}</div>
								</div>
							</div>
							<div className="d-flex justify-content-between align-items-center">
								<div onClick={(e) => handleShare(e, article.id, i, isFeatured)} className="share-btn btn d-flex text-navy c-pointer pl-3">
									<img src={purpleShare} className="preview-icon mr-2" />
									<span className="text-12">Share</span>
								</div>
								<div className={"text-navy upvote-btn c-pointer "+ (article.isUpvoted ? "active" : "")} onClick={(e) => toggleUpvote(e, i, isFeatured)}>
									<img src={purpleUpvote} className="preview-icon mr-2" />
									{article.upvote}
								</div>
							</div>	
							<div className="justify-content-between align-items-center mobile-only-flex">
								<div onClick={(e) => handleShare(e, article.id, i, isFeatured)} className="share-btn btn d-flex text-navy c-pointer mr-2">
									<img src={purpleShare} className="preview-icon mr-2" />
									SHARE
								</div>
								<div className={"text-navy upvote-btn c-pointer "+ (votes[i].length > 0 && votes[i][0].voted ? "active" : "")} onClick={(e) => toggleUpvote(e, i, isFeatured)}>
									<img src={purpleUpvote} className="preview-icon mr-2" />
									{article.vote}
								</div>
							</div>
						</div>
					</div>
				</div>
			)}
		</div>
	)
}

export default ArticleList;