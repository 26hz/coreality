import moment from 'moment';
import { isMobile } from 'react-device-detect';

import avatar from '../assets/img/avatar.png';

import editIcon from '../assets/icon/edit.png';

const CommentsList = ({
	isFeatured,
	isAuthenticated,
	loggedUser,
	data,
	comments,
	reply,
	commentUsers,
	commentReplyUsers,
	type,
	handleComment,
	toggleComment,
	submitComment,
	isModal,
	displayChildCommentModal,
}) => {

	const avator = 	<img className="discussion-avatar mr-3" src={avatar} />
	const authAvator = <img className="discussion-avatar mr-3" src={loggedUser && loggedUser.avator} />
	
	return (
		<div>
			{
				comments && comments.map((dataObject, j) =>
					dataObject && Object.values(dataObject).map((comment, k) =>
					<div key={k} className={"user-comment " + (type === "project" && comment.proj_id === data.id ? "d-block" : type === "discussion" && comment.discus_id === data.id ? "d-block" : "d-none ")}>
						<div className="d-flex align-items-center">
							{
								isFeatured && commentUsers !== undefined && commentUsers.featAllProjCommentUsers !== undefined && commentUsers.featAllProjCommentUsers[j] !== undefined && commentUsers.featAllProjCommentUsers[j][k] !== undefined ? 
								<img src={commentUsers.featAllProjCommentUsers[j][k].avator? commentUsers.featAllProjCommentUsers[j][k].avator : avatar} className="discussion-avatar mr-3" /> 
								:
								isFeatured && commentUsers !== undefined && commentUsers.featAllDiscusCommentUsers !== undefined && commentUsers.featAllDiscusCommentUsers[j] !== undefined && commentUsers.featAllDiscusCommentUsers[j][k] !== undefined ? 
								<img src={commentUsers.featAllDiscusCommentUsers[j][k].avator? commentUsers.featAllDiscusCommentUsers[j][k].avator: avatar} className="discussion-avatar mr-3" /> 
								:
								type === "project" && commentUsers !== undefined && commentUsers.allProjCommentUsers !== undefined && commentUsers.allProjCommentUsers[j] !== undefined && commentUsers.allProjCommentUsers[j][k] !== undefined ?
								<img src={commentUsers.allProjCommentUsers[j][k].avator ? commentUsers.allProjCommentUsers[j][k].avator : avatar} className="discussion-avatar mr-3" /> 
								:
								type === "discussion" && commentUsers !== undefined && commentUsers.allDiscusCommentUsers !== undefined && commentUsers.allDiscusCommentUsers[j] !== undefined && commentUsers.allDiscusCommentUsers[j][k] !== undefined?
								<img src={commentUsers.allDiscusCommentUsers[j][k].avator} className="discussion-avatar mr-3" />
									:
									avator
							}
							<div className="d-flex">
								{
										isFeatured && commentUsers !== undefined && commentUsers.featAllProjCommentUsers !== undefined && commentUsers.featAllProjCommentUsers[j] !== undefined && commentUsers.featAllProjCommentUsers[j][k] !== undefined ? 
										<div className="text-16 mr-3">{commentUsers.featAllProjCommentUsers[j][k].displayname}</div>
										:
										isFeatured && commentUsers !== undefined && commentUsers.featAllDiscusCommentUsers !== undefined && commentUsers.featAllDiscusCommentUsers[j] !== undefined && commentUsers.featAllDiscusCommentUsers[j][k] !== undefined ? 
										<div className="text-16 mr-3">{commentUsers.featAllDiscusCommentUsers[j][k].displayname}</div>
										:
										type === "project" && commentUsers !== undefined && commentUsers.allProjCommentUsers !== undefined && commentUsers.allProjCommentUsers[j] !== undefined && commentUsers.allProjCommentUsers[j][k] !== undefined ?
										<div className="text-16 mr-3">{commentUsers.allProjCommentUsers[j][k].displayname}</div>
										:
										type === "discussion" && commentUsers !== undefined && commentUsers.allDiscusCommentUsers !== undefined && commentUsers.allDiscusCommentUsers[j] !== undefined && commentUsers.allDiscusCommentUsers[j][k] !== undefined?
										<div className="text-16 mr-3">{commentUsers.allDiscusCommentUsers[j][k].displayname}</div>
										:
										<div className="text-16 mr-3">User Not Found</div>
								}
								<div className="text-16 text-gray">
									{moment( new Date(comment.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
								</div>
							</div>
						</div>
						<div className="comment text-gray">
							{comment.description}
						</div>
						<div onClick={() => {!isMobile ? toggleComment(j, k, isFeatured) : displayChildCommentModal(comment.id, k, commentUsers)}} className="reply-btn px-3 py-2 mt-3">
							Reply
						</div>
						<div className="comment-reply mt-4 ml-6">
							{ reply && reply.map((replyObject, l) =>
								replyObject && Object.values(replyObject).map((reply, m) =>
									<div className={(reply.comment_id === comment.id ? "d-block" : "d-none")} key={m}>
										<div>
											<div className="d-flex align-items-center">
												{
													isFeatured && commentReplyUsers !== undefined && commentReplyUsers.featAllProjCommentUsersReplys !== undefined && commentReplyUsers.featAllProjCommentUsersReplys[l] !== undefined && commentReplyUsers.featAllProjCommentUsersReplys[l][m] !== undefined && commentReplyUsers.featAllProjCommentUsersReplys[l][m].avator !== undefined ?
													<img src={commentReplyUsers.featAllProjCommentUsersReplys[l][m].avator} className="discussion-avatar mr-3" />
													:
													isFeatured && commentReplyUsers !== undefined && commentReplyUsers.featAllDiscusCommentUsersReplys !== undefined && commentReplyUsers.featAllDiscusCommentUsersReplys[l] !== undefined && commentReplyUsers.featAllDiscusCommentUsersReplys[l][m] !== undefined && commentReplyUsers.featAllDiscusCommentUsersReplys[l][m].avator !== undefined ?
													<img src={commentReplyUsers.featAllDiscusCommentUsersReplys[l][m].avator} className="discussion-avatar mr-3" />
													:
													type === "project" && commentReplyUsers !== undefined && commentReplyUsers.allProjCommentUsersReplys !== undefined && commentReplyUsers.allProjCommentUsersReplys[l] !== undefined && commentReplyUsers.allProjCommentUsersReplys[l][m] !== undefined && commentReplyUsers.allProjCommentUsersReplys[l][m].avator !== undefined ?
													<img src={commentReplyUsers.allProjCommentUsersReplys[l][m].avator} className="discussion-avatar mr-3" />
													: type === "discussion" && commentReplyUsers !== undefined && commentReplyUsers.allDiscusCommentUsersReplys !== undefined && commentReplyUsers.allDiscusCommentUsersReplys[m] !== undefined && commentReplyUsers.allDiscusCommentUsersReplys[m].avator !== undefined ?
														<img src={commentReplyUsers.allDiscusCommentUsersReplys[m].avator} className="discussion-avatar mr-3" />
														: avator
												}
												<div className="d-flex">
													<div className="mr-3">
														{
															isFeatured && commentReplyUsers !== undefined && commentReplyUsers.featAllProjCommentUsersReplys !== undefined && commentReplyUsers.featAllProjCommentUsersReplys[l] !== undefined && commentReplyUsers.featAllProjCommentUsersReplys[l][m] !== undefined ?
															<div className="text-16">{commentReplyUsers.featAllProjCommentUsersReplys[l][m].displayname}</div>
																:
																isFeatured && commentReplyUsers !== undefined && commentReplyUsers.featAllDiscusCommentUsersReplys !== undefined && commentReplyUsers.featAllDiscusCommentUsersReplys[l] !== undefined && commentReplyUsers.featAllDiscusCommentUsersReplys[l][m] !== undefined ?
																	<div className="text-16">{commentReplyUsers.featAllDiscusCommentUsersReplys[l][m].displayname}</div>
																	:
																	type === "project" && commentReplyUsers !== undefined && commentReplyUsers.allProjCommentUsersReplys !== undefined && commentReplyUsers.allProjCommentUsersReplys[l] !== undefined && commentReplyUsers.allProjCommentUsersReplys[l][m] !== undefined ?
																		<div className="text-16">{commentReplyUsers.allProjCommentUsersReplys[l][m].displayname}</div>
																		: type === "discussion" && commentReplyUsers !== undefined && commentReplyUsers.allDiscusCommentUsersReplys !== undefined && commentReplyUsers.allDiscusCommentUsersReplys[l] !== undefined ?
																			<div className="text-16">{commentReplyUsers.allDiscusCommentUsersReplys[l].displayname}</div>
																			: <div className="text-16">User Not Found</div>
														}
													</div>
													<div className="text-16 text-gray">
														{moment( new Date(reply.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
													</div>
												</div>
											</div>
											<div className="comment text-gray">
												{reply.description}
											</div>
											{
												isMobile ?
												<div onClick={() => displayChildCommentModal(data.id, k, commentReplyUsers)} className="reply-btn px-3 py-2 mt-3">
													Reply
												</div> 
												: 
												<div onClick={() => toggleComment(j, k, isFeatured)} className="reply-btn px-3 py-2 mt-3">
													Reply
												</div>
											}
										</div>
									</div>
								)
							)}
								<div className={"form-input-discussion-container hide-in-mobile align-items-center my-3 " + (comment.showInput ? "d-flex" : "d-none")}>
									{isAuthenticated? authAvator : avator}
									<div className="input-discussion-container p-relative">
										<img src={editIcon} className="pen-discussion-input"/>
										<input type="text" name="myChildReplyComment" className="input-discussion" onChange={(e) => handleComment(e)} />
									</div>
									{
										isModal ?
										<button className="btn-inverted p-2 ml-2" onClick={() => submitComment("child", data.id, comment.id, "modal")}>
											Send
										</button>
										: 
										<button className="btn-inverted p-2 ml-2" onClick={() => submitComment("child", data.id, comment.id, null)}>
											Send
										</button>
									}
								</div>
							</div>
						</div>
					)
				)
			}
		</div>
	);
}

export default CommentsList;