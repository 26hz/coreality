import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import {projectInfosThunk, createProjCommentThunk, createProjCommentReplyThunk} from '../redux/project/thunk';
import {updateProjVoteThunk} from '../redux/vote/thunk';
import {useSelector, useDispatch} from 'react-redux';
import { userInfosByVotesThunk} from '../redux/user/thunk';
import {useParams} from 'react-router-dom';

import ShareDialog from "../components/ShareDialog";
import MoreTagsModal from "../components/MoreTagsModal";
import ProjectList from "../components/ProjectList";

import plusIcon from '../assets/icon/plus.png';

function ProfileProject () {
	const dispatch = useDispatch();
	const allProjects = useSelector(state => state.proj.projInfos.allProject);
	const allProjUsers = useSelector(state => state.proj.projInfos.allProjUsers);
	const allProjComment = useSelector(state => state.proj.projInfos.projComments);
	const allProjCommentsUsers = useSelector(state => state.proj.projInfos.allProjCommentsUsers);
	const allProjCommentReply = useSelector(state => state.proj.projInfos.projCommentReplys);
	const allProjCommentReplysUsers = useSelector(state => state.proj.projInfos.allProjCommentReplysUsers);
	const projCommentCount = useSelector(state => state.proj.projInfos.commentCount);
	const projVotes = useSelector(state => state.proj.projInfos.projVotes);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const userInfos = useSelector(state => state.user.userInfos);
	const userProfileInfos = useSelector(state => state.user.userProfileInfos);
	let param = useParams();
	useEffect(() => {
		dispatch(projectInfosThunk(param));
		dispatch(userInfosByVotesThunk())
	},[dispatch])

	const [featured, setFeatured] = useState([]);
	const [description, setDescription] = useState("");
	const [isDisplayShare, setShareShown] = useState(false);
	const [isProjectDetailShown, setProjectDetailShown] = useState(false);
	const [dataDetailModal, setDataDetailModal] = useState({});
	const [isMoreTagsShown, setTagShown] = useState(false);
	const [dataTags, setDataTags] = useState([]);
	const [shareData, setShareData] = useState({});
	const [projects, setProjects] = useState([]);
	const [projectUsers, setProjectUsers] = useState([]);
	const [comments, setComments] = useState([]);
	const [commentUsers, setCommentUsers] = useState([]);
	const [commentsCount, setCommentsCount] = useState([]);
	const [reply, setReply] = useState([]);
	const [replyUsers, setReplyUsers] = useState([]);
	const [votes, setVotes] = useState([]);
	const forceUpdate = React.useState()[1].bind(null, {});
	
	useEffect(() => {
		var dataProject = allProjects;
		var tempData;
		if(dataProject !== undefined && dataProject.length > 0) {
			for(var i = 0; i < dataProject.length; i++){
				if(projects.length > 0){
					if(projects[i].hasOwnProperty("showComment"))
						dataProject[i].showComment = projects[i].showComment;
				} else {
					tempData = {...dataProject[i], ...{['showComment'] : false}};
					dataProject[i] = tempData
				}
			}
			setProjects(dataProject);
		}
		setProjectUsers(allProjUsers);
		setCommentsCount(projCommentCount);
	},[allProjects])
	
	var once = true;
	useEffect(() => {
		if(once && projects.length > 0){
			once = false;
			sortLatest();
		}
	},[projects])

	useEffect(() => {
		var dataProjectComments = allProjComment;
		if(dataProjectComments !== undefined && dataProjectComments.length > 0) {
			dataProjectComments && dataProjectComments.map((data, j) => {
				if(comments !== undefined && comments.length > 0){
					if(comments[j].hasOwnProperty("showInput"))
						dataProjectComments[j].showInput = comments[j].showInput;
				} else {
					dataProjectComments[j]= {...dataProjectComments[j], ...{['showInput'] : false}};
				}
			})
		}
		setComments(dataProjectComments);
		setCommentUsers(allProjCommentsUsers);
	},[allProjComment])

	useEffect(() => {
		setReply(allProjCommentReply);
		setReplyUsers(allProjCommentReplysUsers);
	},[allProjCommentReply])

	useEffect(() => {
		var dataVotes = projVotes;
		if(dataVotes !== undefined && dataVotes.length > 0) {
			setVotes(dataVotes);
		}
	},[projVotes])
	
	const toggleUpvote = (e, index) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data = projVotes;
		let newObj;
		if(data[index].length > 0){
			newObj = data[index][0];
			newObj.voted = !newObj.voted;
		} else {
			data[index].push({});
			newObj =  data[index][0];
			newObj.voted = true;
		}
		var project = projects;
		if(newObj.voted){
			project[index].vote = parseInt(project[index].vote) + 1;
		} else {
			project[index].vote = parseInt(project[index].vote) - 1;
		}
		dispatch(updateProjVoteThunk(newObj.voted, project[index].id));
		setProjects(projects)
		forceUpdate();
	}
	
	const handleShare = (e, id, index, type) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		if (navigator.share) {
			navigator.share({
				title: 'Coreality',
				url: window.location.href + "/" + id
			}).then(() => {
				console.log('Thanks for sharing!');
			})
			.catch(console.error);
		} else {
			var newObj;
			setShareShown(true);
			if(type === "project"){
				newObj = projects[index];
			} else {
				newObj = featured[index];
			}
			setShareData(newObj);
		}
	}
	
	const handleCloseShare = () => {
		setShareShown(false);
	}
	
	const displayMoreTags = (e, id, type) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data;
		if(type === "feature"){
			data = featured;
		} else {
			data = projects;
		}
		const elementsIndex = data.findIndex(element => element.id === id );
		setTagShown(true);
		setDataTags(data[elementsIndex]);
	}
	
	const closeTagsModal = () => {
		setTagShown(false)
	}
	
	const preventDetail = (e) => {
		e.stopPropagation();
	}

	const hideDetail = () =>{
		setProjectDetailShown(false)
	}

	const displayDetail = (type, index) => {
		var data1;
		if(type === "featured"){
			data1 = featured;
		} else {
			data1 = projects;
		}
		var data2 = data1[index];
		data2.type = type;
		data2.index = index;
		
		setDataDetailModal(data2);
		setProjectDetailShown(true);
	}

	const displayComments = (e, id) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data = projects;
		var index = data.findIndex(x => x.id === id);
		data[index].showComment = !data[index].showComment;
		setProjects(data);
		forceUpdate();
	}

	const toggleComment = (j, k) => {
		var data = comments;
		data[j][k].showInput = !data[j][k].showInput;
		setComments(data);
		forceUpdate();
	}

	const handleComment = (event) => {
		setDescription(event.target.value);
	}

	const submitComment = async (placename, proj_id, comment_id) => {
		if(placename === "child"){
			dispatch(createProjCommentReplyThunk(description, proj_id, comment_id));
		} else if(placename === "parent"){
			dispatch(createProjCommentThunk(description, proj_id));
		}
		setTimeout(() => { dispatch(projectInfosThunk()); }, 500);
		setDescription("");
		forceUpdate();
	}

	const sortLatest = () => {
		var data = projects;
		var projUser = projectUsers;
		var comment = comments;
		var commentUser = commentUsers;
		var commentCount = commentsCount;
		var replies = reply;
		var replyUser = replyUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (new Date(data[i - 1].created_at) - Date.now() < new Date(data[i].created_at) - Date.now()) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//project author
					if(projUser.projUsers !== undefined){
						temp = projUser.projUsers[i - 1];
						projUser.projUsers[i - 1] = projUser.projUsers[i];
						projUser.projUsers[i] = temp;
					}
					//comments users
					if(commentUser.allProjCommentUsers[i]){
						temp = commentUser.allProjCommentUsers[i - 1];
						commentUser.allProjCommentUsers[i - 1] = commentUser.allProjCommentUsers[i];
						commentUser.allProjCommentUsers[i] = temp;
					}
					//coments
					if(comment[i]){
						temp = comment[i - 1];
						comment[i - 1] = comment[i];
						comment[i] = temp;
					}
					//replies
					if(replies[i]){
						temp = replies[i - 1];
						replies[i - 1] = replies[i];
						replies[i] = temp;
					}
					//reply users
					if(replyUser.allProjCommentUsersReplys[i]){
						temp = replyUser.allProjCommentUsersReplys[i - 1];
						replyUser.allProjCommentUsersReplys[i - 1] = replyUser.allProjCommentUsersReplys[i];
						replyUser.allProjCommentUsersReplys[i] = temp;
					}
					//comment counts
					if(commentCount !== undefined){
						temp = commentCount.commentCountNums[i - 1];
						commentCount.commentCountNums[i - 1] = commentCount.commentCountNums[i];
						commentCount.commentCountNums[i] = temp;
					}
				}
			}
		}
		setProjects(data);
		setProjectUsers(projUser);
		setComments(comment);
		setCommentUsers(commentUser);
		setReply(replies);
		setReplyUsers(replyUser);
		setCommentsCount(commentCount);
		forceUpdate();
	}
	const addProject = 				
	<NavLink to="/new-project" className="link">
	<button className="new-project-btn align-items-center d-flex px-3 py-2">
		<img className="mr-2" src={plusIcon} />
		ADD NEW PROJECT
	</button>
	</NavLink>

	return (
		<div className="profile-content">
			{
				isDisplayShare && 
				<ShareDialog 
					data = {shareData}
					hideShare = {handleCloseShare}
				/>
			}

			{
				isMoreTagsShown &&
				<MoreTagsModal 
					data = {dataTags}
					hideModal = {closeTagsModal}
				/>
			}

			<div className="d-flex flex-direction-column-mobile justify-content-between mt-5 mb-4">
				<span className="text-20">Projects</span>
				{(userInfos.id == userProfileInfos.id)? addProject : ''}
			</div>
			
			<ProjectList
				isAuthenticated={isAuthenticated}
				loggedUser={userInfos} 
				projects={projects}
				authorUsers = {projectUsers}
				comments={comments}
				commentUsers = {allProjCommentsUsers}
				commentReplyUsers = {allProjCommentReplysUsers}
				reply={reply}
				votes={votes}
				projCommentCount={commentsCount}
				type="project"
				toggleUpvote={toggleUpvote}
				displayComments = {displayComments}
				toggleComment = {toggleComment}
				handleComment = {handleComment}
				submitComment = {submitComment}
				handleShare={handleShare}
				handleCloseShare={handleCloseShare}
				displayMoreTags={displayMoreTags}
				preventDetail={preventDetail}
				displayDetail={displayDetail}
				hideDetail={hideDetail}
				dataDetailModal={dataDetailModal}
				isProjectDetailShown={isProjectDetailShown}
				isDisplayShare={isDisplayShare}
			/>
		</div>
	);
}

export default ProfileProject;