import React from 'react';
import { Link } from 'react-router-dom';

import avatar from '../assets/img/default-avatar.png';

import wrongInvertedIcon from '../assets/icon/wrong-inverted.png';

function signOptionModal(props){
	const {
		hideSignOptionModal,
	} = props;
	return (
		<div className="modal sign-option-modal d-block">
			<div className="modal-content sign-option-modal p-6">
				<div onClick={hideSignOptionModal} className="close-modal-icon-container mb-4">
					<img src={wrongInvertedIcon} className="close-modal-icon"/>
				</div>
				<img src={avatar} className="sign-option-avatar mb-5"/>
				<div className="text-36 text-center mb-3">
					To keep enjoying <div className="text-navy">Coreality</div>
				</div>
				<div className="text-16 text-center mb-5">
					Don’t forget to join our community to keep discovering and enjoying the coreality experience.
				</div>
				<Link to="/signup">
					<div className="btn btn-primary font-light mb-3">
						SIGN UP
					</div>
				</Link>
				<Link to="/login">
					<div className="btn btn-inverted font-light">
						SIGN IN
					</div>
				</Link>
			</div>
		</div>
	);
}

export default signOptionModal;