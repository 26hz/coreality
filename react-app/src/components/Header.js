import React, { useState, useEffect, useRef } from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { push } from "connected-react-router";
import avatar from '../assets/img/avatar.png';
import logo from '../assets/img/logo.png';
import burger from '../assets/icon/burger-menu.png';
import close from '../assets/icon/close.png';
import expand from '../assets/icon/expand.png';
import minimize from '../assets/icon/minimize.png';
import {logoutThunk} from '../redux/auth/thunk';
import {userInfosThunk} from '../redux/user/thunk';

const Header =()=> {
		const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
		const username = useSelector(state => state.user.userInfos.username);
		const avator = useSelector(state => state.user.userInfos.avator);
		const [isMenuShown, toggleMenu] = useState(false);
		const [isMenuProfileShown, toggleMenuProfile] = useState(false);
		const [isMenuProfileSettingShown, toggleMenuProfileSetting] = useState(false);
		const dispatch = useDispatch();
		const [isProjectShown, setActiveProject] = useState(false);
		const [isDiscussionShown, setActiveDiscussion] = useState(false);
		const [isArticleShown, setActiveArticle] = useState(false);
		
		var location;
		useEffect(() => {
			location = window.location.pathname;
			dispatch(userInfosThunk());
			if(location === "" || location === "/"){
				setActiveProject(true);
				setActiveDiscussion(false);
				setActiveArticle(false);
			} else if(location === "/discussions"){
				setActiveProject(false);
				setActiveDiscussion(true);
				setActiveArticle(false);
			} else if(location === "/articles"){
				setActiveProject(false);
				setActiveDiscussion(false);
				setActiveArticle(true);
			} else {
				setActiveProject(false);
				setActiveDiscussion(false);
				setActiveArticle(false);
			}
		},[dispatch])

		function useOutsideAlerter(ref) {
			useEffect(() => {
				function handleClickOutside(event) {
					if (ref.current && !ref.current.contains(event.target) || event.target.getAttribute("class") !== null && event.target.getAttribute("class").includes("btn-change-content")) {
						toggleMenu(false);
						toggleMenuProfile(false);
						toggleMenuProfileSetting(false);
					}
				}

				// Bind the event listener
				document.addEventListener("click", handleClickOutside);
				return () => {
					// Unbind the event listener on clean up
					document.removeEventListener("click", handleClickOutside);
				};
		}, [ref]);
	}

	const wrapperRef = useRef(null);

	useOutsideAlerter(wrapperRef);

	// need to refresh header as well
	const logoutOnSubmit = () =>{
		toggleMenuProfile(false)
		setActiveDiscussion(false);
		setActiveArticle(false);
		dispatch(logoutThunk());
		dispatch(push("/"));
	}
	
	return (
		<header className="px-6 py-1 bg-white" ref={wrapperRef}>
			<div className="w-100 hide-in-mobile d-flex justify-content-between align-items-center">
				<img src={logo} className="avatar-image visibility-hidden" alt="avatar" />
				<div className="d-flex">
				<NavLink to="/">
					<div 
						className={"btn-nav " + (isProjectShown ? "active" : "")}
						onClick={ () => {
							setActiveProject(true);
							setActiveDiscussion(false);
							setActiveArticle(false);
						}}>
							<span>Project</span>
					</div>
				</NavLink>
				<NavLink to="/discussions">
					<div 
						className={"btn-nav " + (isDiscussionShown ? "active" : "")}
						onClick={ () => {
							setActiveProject(false);
							setActiveDiscussion(true);
							setActiveArticle(false);
						}}>
							<span>Discussions</span>
					</div>
				</NavLink>
				<NavLink to="/articles">
					<div 
						className={"btn-nav " + (isArticleShown ? "active" : "")}
						onClick={ () => {
							setActiveProject(false);
							setActiveDiscussion(false);
							setActiveArticle(true);
						}}>
							<span>Articles</span>
					</div>
				</NavLink>
				</div>
				{isAuthenticated ?
					<img 
						src={avator? avator : avatar}
						alt="avatar"
						className={"avatar-image c-pointer " + (isMenuProfileShown ? "active" : "" ) }
						onClick={ () => {
							toggleMenuProfile(true);
							toggleMenu(false);
							setActiveProject(false);
							setActiveDiscussion(false);
							setActiveArticle(false);
						}}
					/>
					:
					<div className="session-container">
						<NavLink 
							to="/login" 
							className="btn-nav"
							onClick={ () => {
								setActiveProject(false);
								setActiveDiscussion(false);
								setActiveArticle(false);
							}}
						>
							<span>						
								Login
							</span>
						</NavLink>
						<NavLink 
							to="/signup" 
							className="btn-nav"
							onClick={ () => {
								setActiveProject(false);
								setActiveDiscussion(false);
								setActiveArticle(false);
							}}
						>
							<span>						
								Signup
							</span>
						</NavLink>
					</div>
				}
			</div>

			<div className="w-100 mobile-only-flex d-flex justify-content-between align-items-center">
				<img src={isMenuShown ? close : burger} 
					onClick={ () => {
						toggleMenu(prev => !prev);
						toggleMenuProfile(false)
					}}
					className="burger-icon"
				/>
				<div className="text-24">Coreality</div>
				{isAuthenticated ?
					<img src={avator}
						onClick={ () => {
							toggleMenuProfile(prev => !prev);
							toggleMenu(false)
						}}
						className={"avatar-image c-pointer " + (isMenuProfileShown ? "active" : "" ) }
						alt="avatar" 
					/> :
					<NavLink to="/login" className="link">
						<div className="text-14 btn-inverted text-center py-2">SIGN IN</div>
					</NavLink>
				}
			</div>

			<div className={"navbar-items left-navbar bg-white text-navy px-3 " + (isMenuShown ? "d-block" : "d-none")}>
				<div className={"btn-change-content text-20 p-3 " + (isProjectShown ? "active" : "")}
						onClick={ () => {
							setActiveProject(true);
							setActiveDiscussion(false);
							setActiveArticle(false);
						}}>
					<NavLink to="/">
						<span>Projects</span>
					</NavLink>
				</div>
				<NavLink 
					to="/discussions" 
					className={"btn-change-content text-20 p-3 " + (isDiscussionShown ? "active" : "")} 
					onClick={ () => {
						setActiveProject(false);
						setActiveDiscussion(true);
						setActiveArticle(false);
					}}
				>
					<div>Discussion</div>
				</NavLink>
				<NavLink 
					to="/articles" 
					className={"btn-change-content text-20 p-3 border-bottom " + (isArticleShown ? "active" : "")}
					onClick={ () => {
						setActiveProject(false);
						setActiveDiscussion(false);
						setActiveArticle(true);
					}}
				>
					<div>Articles</div>
				</NavLink>
				<div className="text-20 p-3">About Us</div>
				<div className="text-20 p-3">Terms of Service</div>
				<div className="text-20 p-3">Privacy Policy</div>
				<div className="text-20 p-3 mb-3">Cookie Policy</div>
			</div>
			<div className={"navbar-items bg-white text-navy px-3 " + (isMenuProfileShown ? "d-block" : "d-none")}>
				<NavLink to={"/profile/" + username}>
					<div className="btn-change-content text-20 p-3">							
						See	Profile
					</div>
				</NavLink>
				<div
					onClick={ () => {
						toggleMenuProfileSetting(prev => !prev);
					}} 
					className={"setting-parent-menu d-flex align-items-center justify-content-between border-bottom c-pointer " + (isMenuProfileSettingShown ? "active" : "")  }>
					<div className="text-20 p-3">Settings</div>
					<img src={isMenuProfileSettingShown ? expand : minimize} alt="expand" />
				</div>
				<div className={"setting-dropdown " + (isMenuProfileSettingShown ? "d-block" : "d-none")}>
					<NavLink to="/setting/profile">
						<div className="btn-change-content setting-dropdown-child text-20 py-3 px-5">
							Profile
						</div>
					</NavLink>
					<NavLink to="/setting/avatar">
						<div className="btn-change-content setting-dropdown-child text-20 py-3 px-5">
							Avatar
						</div>
					</NavLink>
					<NavLink to="/setting/email">
						<div className="btn-change-content setting-dropdown-child text-20 py-3 px-5">
							Email &amp;	Password
						</div>
					</NavLink>
					{/* <NavLink to="/setting/account">
						<div className="btn-change-content setting-dropdown-child text-20 py-3 px-5 border-bottom">
							Account
						</div>
					</NavLink> */}
				</div>
				<div 
					className="btn-change-content text-20 p-3 c-pointer" 
					onClick={logoutOnSubmit}
					>Logout</div>
			</div>
		</header>		
	);
}

export default Header;