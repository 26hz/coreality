export const getDiscus_Info = 'GETDISCUS_INFO';
export const createDiscus_Success = 'CREATEDISCUS_SUCCESS';
export const createDiscusComment_Success = 'CREATEDISCUSCOMMENT_SUCCESS';
export const createDiscusCommentReply_Success = 'CREATEDISCUSCOMMENTREPLY_SUCCESS';

export function getDiscusInfo(discusInfos, msg){
    return {
        type: getDiscus_Info,
        discusInfos,
        msg
    }
};

export function createDiscusSuccess(msg){
    return {
        type: createDiscus_Success,
        msg
    }
};

export function createDiscusCommentSuccess(msg){
    return {
        type: createDiscusComment_Success,
        msg
    }
};

export function createDiscusCommentReplySuccess(msg){
    return {
        type: createDiscusCommentReply_Success,
        msg
    }
};