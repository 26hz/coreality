import { getDiscus_Info, createDiscus_Success ,createDiscusCommentReply_Success, createDiscusComment_Success } from './action';

const initialState = {
    discusInfos: [],
    msg: ''
}

export function discusReducer(state = initialState, action ) {
    switch(action.type){
        case getDiscus_Info:
            return {
                ...state,
                discusInfos: action.discusInfos,
                msg: action.msg
            };  
        case createDiscus_Success:
            return {
                ...state,
                msg: action.msg
            };
        case createDiscusComment_Success:
            return {
                ...state,
                msg: action.msg
            }; 
        case createDiscusCommentReply_Success:
            return {
                ...state,
                msg: action.msg
            }; 
    default:        
    return state;
    }
};
