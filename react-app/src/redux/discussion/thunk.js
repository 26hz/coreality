import { getDiscusInfo, createDiscusSuccess, createDiscusCommentSuccess, createDiscusCommentReplySuccess } from './action';

export function createDiscussionThunk(title, description, tags) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch(`http://localhost:8080/api/v1/createdisccusion`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    title,
                    description, 
                    tags
                })
            }
        );
        const result = await res.json();
        dispatch(createDiscusSuccess(result.msg));
    };
}

export function discusInfosThunk(param) {
    return async (dispatch) => {
        let username = param.username
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/discussionInfos`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ token, username }),
            }
        );
        const result = await res.json();
        dispatch(getDiscusInfo(result));
    };
}

export function discusInfosByTagThunk(tag) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/discussionInfosByTag`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ token, tag }),
            }
        );
        const result = await res.json();
        dispatch(getDiscusInfo(result));
    };
}

export function disucsAllInfosThunk() {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/disucsAllInfos`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ token }),
            }
        );
        const result = await res.json();
        dispatch(getDiscusInfo(result));
    };
}

export function createDiscusCommentReplyThunk( description, discus_id , comment_id) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");

        const res = await fetch( `http://localhost:8080/api/v1/creatediscuscommentreply`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    description,
                    discus_id, 
                    comment_id
                })
            }
        );
        const result = await res.json();
        dispatch(createDiscusCommentReplySuccess(result.msg));
    };
}

export function createDiscusCommentThunk( description, discus_id ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/creatediscuscomment`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    description, 
                    discus_id
                })
            }
        );
        const result = await res.json();
        dispatch(createDiscusCommentSuccess(result.msg));
    };
}