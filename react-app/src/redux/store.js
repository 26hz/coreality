import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import { authReducer } from './auth/reducer';
import { userReducer } from './user/reducer';
import {connectRouter, routerMiddleware} from 'connected-react-router'
import thunk from 'redux-thunk';
import {logger} from 'redux-logger';
import { createBrowserHistory } from 'history';
import { projReducer } from './project/reducer';
import { artReducer } from './article/reducer';
import {discusReducer} from './discussion/reducer';
import { composeWithDevTools } from "redux-devtools-extension";
import {voteReducer} from './vote/reducer'
export const history = createBrowserHistory();

// IRootAction
// rootReducers
const rootReducers = combineReducers({
    auth: authReducer,
    user: userReducer,
    proj: projReducer,
    discus: discusReducer,
    art: artReducer,
    vote: voteReducer,
    router: connectRouter(history)
});
const composeEnhancers = (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose) && composeWithDevTools(
    {
    trace: true,
    traceLimit: 25,});

// createStore
export default createStore(
    rootReducers, 
    composeEnhancers(applyMiddleware(logger),
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history)))
    );
