export const getProj_Info = 'GETPROJ_INFO';
export const createProj_Success = 'CREATEPROJ_SUCCESS';
export const createProjComment_Success = 'CREATEPROJCOMMENT_SUCCESS';
export const createProjCommentReply_Success = 'CREATEPROJCOMMENTREPLY_SUCCESS';

export function getProjInfo(projInfos){
    return {
        type: getProj_Info,
        projInfos
    }
};

export function createProjSuccess(msg){
    return {
        type: createProj_Success,
        msg
    }
};

export function createProjCommentSuccess(msg){
    return {
        type: createProjComment_Success,
        msg
    }
};

export function createProjCommentReplySuccess(msg){
    return {
        type: createProjCommentReply_Success,
        msg
    }
};