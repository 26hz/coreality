import { getProjInfo, createProjSuccess, createProjCommentReplySuccess, createProjCommentSuccess } from './action';

export function createProjectThunk(title, headline, status, description, link, videoLink, galleryFiles, selectedFile, tags ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append('title', title);
        formData.append('headline', headline);
        formData.append('status', status);
        formData.append('description', description);
        formData.append('link', link);
        formData.append('videoLink', videoLink);
        formData.append('imgThumb', selectedFile);
        for (let i = 0; i < tags.length; i++) {
            formData.append('tags[]', tags[i]);
        }
        // for (let i = 0; i < participants.length; i++) {
        //     formData.append('participated_user_username', participants[i]);
        // }
        formData.append('token', token);
        for (let i=0; i<galleryFiles.length; i++){
            formData.append('imgDetail', galleryFiles[i]);
        }
        const res = await fetch( `http://localhost:8080/api/v1/createproject`,
            {
                method: "POST",
                headers: {
                    "enctype": "multipart/form-data"
                },
                body: formData
            }
        );
        const result = await res.json();
        dispatch(createProjSuccess(result.msg));
    };
}

export function projectInfosThunk(param) {
    return async (dispatch) => {
        let username = param.username
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/projectInfos`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ token, username }),
            }
        );
        const result = await res.json();
        dispatch(getProjInfo(result));
    };
}

export function projectInfosByTagThunk(tag) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/projectInfosByTag`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ token, tag }),
            }
        );
        const result = await res.json();
        dispatch(getProjInfo(result));
    };
}

export function projectAllInfosThunk() {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/projectAllInfos`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ token }),
            }
        );
		const result = await res.json();
        dispatch(getProjInfo(result));
    };
}

export function createProjCommentReplyThunk( description, proj_id, comment_id ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/createprojcommentreply`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    description, 
                    proj_id, 
                    comment_id
                })
            }
        );
        const result = await res.json();
        dispatch(createProjCommentReplySuccess(result));
    };
}

export function createProjCommentThunk( description, proj_id ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/createprojcomment`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    description, 
                    proj_id
                })
            }
        );
        const result = await res.json();
        dispatch(createProjCommentSuccess(result));
    };
}