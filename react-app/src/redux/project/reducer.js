import { getProj_Info, createProj_Success, createProjComment_Success, createProjCommentReply_Success } from './action';

const initialState = {
    projInfos: [],
    msg: ''
}

export function projReducer(state = initialState, action ) {
    switch(action.type){
        case getProj_Info:
            return {
                ...state,
                projInfos: action.projInfos,
            }; 
        case createProj_Success:
            return {
                ...state,
                msg: action.msg
            };
        case createProjComment_Success:
            return {
                ...state,
                msg: action.msg
            }; 
        case createProjCommentReply_Success:
            return {
                ...state,
                msg: action.msg
            }; 
    default:        
    return state;
    }
};
