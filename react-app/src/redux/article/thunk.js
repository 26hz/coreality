import { getArticleInfo } from './action';
import { createArticleSuccess} from './action'

export function createArticleThunk(title, description, content, tags ,selectedFile) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append('title', title);
        formData.append('content', content);
        formData.append('description', description);
        formData.append('imgThumb', selectedFile);
        for (let i = 0; i < tags.length; i++) {
            formData.append('tags[]', tags[i]);
        }
        formData.append('token', token);
        const res = await fetch( `http://localhost:8080/api/v1/createarticle`,
            {
                method: "POST",
                headers: {
                    "enctype": "multipart/form-data"
                },
                body: formData
            }
        );
        const result = await res.json();
        dispatch(createArticleSuccess(result.msg));
    };

}

export function articleInfosThunk(param) {
    return async (dispatch) => {
        let username = param.username
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/article?username=${encodeURIComponent(username)}`,
            {
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + token
                }
            }
        );
        const result = await res.json();
        dispatch(getArticleInfo(result));
    };
}

export function articleInfosByTagThunk(tag) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/article?tag=${encodeURIComponent(tag)}`,
            {
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + token
                },
            }
        );
        const result = await res.json();
        dispatch(getArticleInfo(result));
    };
}

export function articleAllInfosThunk() {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/article/all`,
            {
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + token
                }
            }
        );
        const result = await res.json();
        dispatch(getArticleInfo(result))
    };
}