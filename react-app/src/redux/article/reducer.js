import { getArticle_Info } from './action';
import { createArticle_Success } from './action';

const initialState = {
    ArticleInfos: [],
    msg: ''
}

export function artReducer(state = initialState, action ) {
    switch(action.type){
        case getArticle_Info:
            return {
                ...state,
                ArticleInfos: action.ArticleInfos,
                msg: action.msg
            }; 
        case createArticle_Success:
            return {
                ...state,
                msg: action.msg
            };  
    default:        
    return state;
    }
};
