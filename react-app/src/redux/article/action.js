export const getArticle_Info = 'GETARTICLE_INFO';
export const createArticle_Success = 'CREATEARTICLE_SUCCESS';

export function getArticleInfo(ArticleInfos, msg){
    return {
        type: getArticle_Info,
        ArticleInfos,
        msg
    }
};
export function createArticleSuccess(msg){
    return {
        type: createArticle_Success,
        msg
    }
};

