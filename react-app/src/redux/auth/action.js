export const LOGIN_PROCESSING = '@@auth/LOGIN_PROCESSING';
export const LOGIN_SUCCESS = '@@auth/LOGIN_SUCCESS';
export const LOGIN_FAIL = '@@auth/LOGIN_FAIL';


export function loginSuccess(){
    return {
        type: LOGIN_SUCCESS
    }
};

export function loginFailed(msg){
    return {
        type: LOGIN_FAIL,
        msg
    }
}

export function logout(){
    return {
        type: LOGIN_PROCESSING
    }
}