import { loginFailed, loginSuccess, logout } from "./action";
import { push } from "connected-react-router";

export function loginThunk(email, password) {
    return async (dispatch) => {
        const res = await fetch(
            `http://localhost:8080/api/v1/login`,
            {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({
                    email,
                    password,
                }),
            }
        );

        const result = await res.json();
        if (res.status === 401) {
            dispatch(loginFailed(result.msg));
        } else {
            dispatch(loginSuccess());
            localStorage.setItem("token", result.token); 
            dispatch(push("/"));
        }
    };
}

export function logoutThunk() {
    return async (dispatch) => {
        dispatch(logout());
        localStorage.removeItem("token");
    };
}

export function loginFacebookThunk(accessToken) {
    return async (dispatch) => {
        const res = await fetch(
            `http://localhost:8080/api/v1/login/facebook`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ accessToken }),
            }
        );
        const result = await res.json();

        if (res.status !== 200) {
            dispatch(loginFailed(result.msg));
        } else {
            localStorage.setItem("token", result.token);
            dispatch(loginSuccess());
            dispatch(push("/profile"));
        }
    };
}

export function loginGoogleThunk(accessToken) {
    return async (dispatch) => {
        const res = await fetch(
            `http://localhost:8080/api/v1/login/google`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ accessToken }),
            }
        );
        const result = await res.json();
        
        if (res.status !== 200) {
            dispatch(loginFailed(result.msg));
        } else {
            localStorage.setItem("token", result.token);
            dispatch(loginSuccess());
            dispatch(push("/"));
        }
    };
}

export default function signupThunk(email, username, password, displayname, role, exp, skilltag) {
    
    return async (dispatch) => {
        const res = await fetch(
            `http://localhost:8080/api/v1/signup`,
            {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({
                    email,
                    username,
                    password,
                    displayname,
                    role,
                    exp,
                    skilltag
                }),
            }
        );

            const result = await res.json();
            if (res.status === 401 || res.status === 500) {
                dispatch(loginFailed(result.msg));
            } else {
                dispatch(loginSuccess());
                localStorage.setItem("token", result.token); 
                dispatch(push("/"));
            }
    };
}

export function signupFacebookThunk(accessToken) {
    return async (dispatch) => {
        const res = await fetch(
            `http://localhost:8080/api/v1/login/facebook`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ accessToken }),
            }
        );
        const result = await res.json();

        if (res.status !== 200) {
            dispatch(loginFailed(result.msg));
        } else {
            localStorage.setItem("token", result.token);
            dispatch(loginSuccess());
            dispatch(push("/profile"));
        }
    };
}

export function signupGoogleThunk(accessToken) {
    return async (dispatch) => {
        const res = await fetch(
            `http://localhost:8080/api/v1/login/google`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ accessToken }),
            }
        );
        const result = await res.json();
        
        if (res.status !== 200) {
            dispatch(loginFailed(result.msg));
        } else {
            localStorage.setItem("token", result.token);
            dispatch(loginSuccess());
            dispatch(push("/"));
        }
    };
}
