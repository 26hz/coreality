import { LOGIN_PROCESSING, LOGIN_SUCCESS, LOGIN_FAIL } from './action';

const initialState = {
    isAuthenticated: localStorage.getItem('token') !== null,
    msg: '',
}

export function authReducer(state = initialState, action ) {
    switch(action.type){
        case LOGIN_PROCESSING:
            return {
                ...state,
                isAuthenticated: false,
                isProcessing: true,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                isProcessing: false,
            };
        case LOGIN_FAIL:
            return {
                ...state,
                isProcessing: false,
                isAuthenticated: false,
                msg: action.msg,                
            }
    default:        
    return state;
    }
};