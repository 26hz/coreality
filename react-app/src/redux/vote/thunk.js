import { getProjVoteInfo, getDiscusVoteInfo, getArtVoteInfo } from './action';

export function getProjVoteThunk() {
    return async (dispatch) => {
        const res = await fetch( `http://localhost:8080/api/v1/projAllVoteinfos`,
            {
                method: "GET",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                }
            }
        );
        const result = await res.json();
    };
}

export function getDiscusVoteThunk() {
    return async (dispatch) => {
        const res = await fetch( `http://localhost:8080/api/v1/discusAllVoteinfos`,
            {
                method: "GET",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                }
            }
        );
        const result = await res.json();
        dispatch(getDiscusVoteInfo(result));
    };
}

export function getArtVoteThunk() {
    return async (dispatch) => {
        const res = await fetch( `http://localhost:8080/api/v1/artAllVoteinfos`,
            {
                method: "GET",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                }
            }
        );
        const result = await res.json();
        dispatch(getArtVoteInfo(result));
    };
}

export function updateProjVoteThunk( voted, proj_id ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/updateProjVote`,
            {
                method: "PUT",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    voted, 
                    proj_id
                })
            }
        );
        const result = await res.json();
        dispatch(getProjVoteInfo(result));
    };
}
export function updateDiscusVoteThunk( voted, discus_id ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/updateDiscusVote`,
            {
                method: "PUT",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    voted, 
                    discus_id
                })
            }
        );
        const result = await res.json();
        dispatch(getDiscusVoteInfo(result));
    };
}
export function updateArtVoteThunk( voted, art_id ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/updateArtVote`,
            {
                method: "PUT",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,
                    voted, 
                    art_id
                })
            }
        );
        const result = await res.json();
        dispatch(getArtVoteInfo(result));
    };
}