export const getProjVote_Info = 'GETPROJVOTE_INFO';
export const getDiscusVote_Info = 'GETDISCUSVOTE_INFO';
export const getArtVote_Info = 'GETARTJVOTE_INFO';

export function getProjVoteInfo(getProjVoteInfo){
    return {
        type: getProjVote_Info,
        getProjVoteInfo
    }
};

export function getDiscusVoteInfo(getDiscusVoteInfo){
    return {
        type: getDiscusVote_Info,
        getDiscusVoteInfo
    }
};

export function getArtVoteInfo(getArtVoteInfo){
    return {
        type: getArtVote_Info,
        getArtVoteInfo
    }
};
