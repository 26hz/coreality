import { getProjVote_Info, getDiscusVote_Info, getArtVote_Info } from './action';

const initialState = {
    getProjVoteInfo: [],
    getDiscusVoteInfo:[],
    getArtVoteInfo:[]
}

export function voteReducer(state = initialState, action ) {
    switch(action.type){
        case getProjVote_Info:
            return {
                ...state,
                getProjVoteInfo: action.getProjVoteInfo,
            };
        case getDiscusVote_Info:
            return {
                ...state,
                getDiscusVoteInfo: action.getDiscusVoteInfo,
            }; 
        case getArtVote_Info:
            return {
                ...state,
                getArtVoteInfo: action.getArtVoteInfo,
            }; 
    default:
    return state;
    }
};
