export const getUser_Info = 'GETUSER_INFO';
export const getUser_Avator = 'GETUSER_AVATOR'
export const getXRCreators_Info = 'GETXRCREATORS_INFO'
export const getTag_Infos = 'GETTAGINFOS'
export const getUserProfile_Info = 'GETUSERPROFILE_INFO';

export function getUserInfo(userInfos, msg){
    return {
        type: getUser_Info,
        userInfos,
        msg
    }
};

export function getUserProfileInfo(userProfileInfos, msg){
    return {
        type: getUserProfile_Info,
        userProfileInfos,
        msg
    }
};

export function getXRCreatorsInfo(XRCreatorsInfos){
    return {
        type: getXRCreators_Info,
        XRCreatorsInfos,
    }
};

export function getTagInfos(tagInfos){
    return {
        type: getTag_Infos,
        tagInfos,
    }
};

export function getUserAvator(avator, msg){
    return {
        type: getUser_Avator,
        avator,
        msg
    }
};
