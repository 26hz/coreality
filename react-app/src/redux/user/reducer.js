import { getUser_Info } from './action';
import { getUser_Avator } from './action';
import { getXRCreators_Info } from './action';
import { getTag_Infos } from './action';
import { getUserProfile_Info } from './action'

const initialState = {
    userInfos: [],
    XRCreatorsInfos:[],
    userProfileInfos:[],
    avator: [],
    msg: ''
}

export function userReducer(state = initialState, action ) {
    switch(action.type){
        case getUser_Info:
            return {
                ...state,
                userInfos: action.userInfos,
                msg: action.msg
            }; 
        case getUserProfile_Info:
            return {
                ...state,
                userProfileInfos: action.userProfileInfos,
                msg: action.msg
            }; 
        case getXRCreators_Info:
            return {
                ...state,
                XRCreatorsInfos: action.XRCreatorsInfos,
            }; 
        case getTag_Infos:
            return {
                ...state,
                tagInfos: action.tagInfos,
            };   
        case getUser_Avator:
            return {
                ...state,
                avator: action.avator,
                msg: action.msg
            };     
    default:        
    return state;
    }
};
