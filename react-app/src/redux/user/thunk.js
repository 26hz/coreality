import { getTagInfos, getUserInfo, getUserProfileInfo, getUserAvator, getXRCreatorsInfo } from "./action";

export function userInfosThunk() {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/userInfos`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ token }),
            }
        );
        const result = await res.json();
        dispatch(getUserInfo(result));
    };
}

export function updateUserProfileThunk(username, displayname, role, headline, bio, website, facebook, twitter, linkedin, skilltag, exp  ) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/updateprofile`,
            {
                method: "PUT",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,                     
                    username,
                    displayname, 
                    role, 
                    headline, 
                    bio, 
                    website, 
                    facebook, 
                    twitter, 
                    linkedin,
                    skilltag,
                    exp 
                }),
            }
        );
        const result = await res.json();
        dispatch(getUserInfo(result));
    };
}

export function uploadAvatorThunk(avator) {
    return async (dispatch) => {

    let token = localStorage.getItem("token");
    const formData = new FormData();
    formData.append('avator', avator);
    formData.append('token', token);
    const res = await fetch( `http://localhost:8080/api/v1/updateavator`,
    {
        method: "PUT",
        headers: {
            "enctype": "multipart/form-data"
        },
        body: formData
    }
);
    const uploadedFiles = await res.json();
    dispatch(getUserAvator(uploadedFiles));
    };
};

export function updateEmailThunk(email) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/updateEmail`,
            {
                method: "PUT",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,                     
                    email
                }),
            }
        );
        const result = await res.json();
        dispatch(getUserInfo(result));
    };
}

export function updatePasswordThunk(oldPassword, newPassword) {
    return async (dispatch) => {
        let token = localStorage.getItem("token");
        const res = await fetch( `http://localhost:8080/api/v1/updatePassword`,
            {
                method: "PUT",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    token,                     
                    oldPassword,
                    newPassword
                }),
            }
        );
        const result = await res.json();
        dispatch(getUserInfo(result));
    };
}
export function userInfosByVotesThunk() {
    return async (dispatch) => {
        const res = await fetch( `http://localhost:8080/api/v1/getUserByVotes`,
            {
                method: "GET",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                }
            }
        );
        const result = await res.json();
        dispatch(getXRCreatorsInfo(result));
    };
}

export function tagInfos() {
    return async (dispatch) => {
        const res = await fetch( `http://localhost:8080/api/v1/getTags`,
            {
                method: "GET",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                }
            }
        );
        const result = await res.json();
        dispatch(getTagInfos(result));
    };
}

export function userProfileInfosThunk(param) {
    return async (dispatch) => {
        let username = param.username
        const res = await fetch( `http://localhost:8080/api/v1/userProfileInfos`,
            {
                method: "POST",
                headers: {
                    "Accept": "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ username }),
            }
        );
        const result = await res.json();
        dispatch(getUserProfileInfo(result));
    };
}