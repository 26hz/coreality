import React from 'react';
import {ConnectedRouter} from 'connected-react-router';
import Header from "./components/Header";
import {Route, Switch} from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import AllUsers from './pages/AllUsers';
import TagsPage from './pages/TagsPage';
import Profile from './pages/ProfilePage';
import NewProject from './pages/NewProjectPage';
import Login from './pages/LoginPage';
import NewArticlePage from './pages/NewArticlePage';
import SettingPage from './pages/SettingPage';
import HomeArticle from './pages/HomeArticle';
import HomePage from './pages/HomePage';
import SignupPage from './pages/SignupPage';
import HomeDiscussion from './pages/HomeDiscussion';
import DetailDiscussionMobile from './pages/DetailDiscussionMobile';
import {history } from './redux/store';
import './App.css';

function App() {
    return (
      <div className="App">
        <ConnectedRouter history={history}>
					<Header />
          <Switch>
            <Route path="/tags/:type" component={TagsPage} />
            <Route path="/users" component={AllUsers} />
            <Route path="/discussions" exact={true} component={HomeDiscussion} />
            <Route path='/discussions/:discussionID' exact={true} component={HomeDiscussion}/>
            <Route path='/discuss/:tagInput' children={<HomeDiscussion />}/>
            <Route path='/discussion/:id/' render={props => <DetailDiscussionMobile {...props}/>}/>
            <Route path="/articles" exact={true} component={HomeArticle} />
            <Route path="/articles/:articleID" exact={true} component={HomeArticle}/>
            <Route path="/article/:tagInput" children={<HomeArticle />} />
            <Route path="/" exact={true} component={HomePage} />
            <Route path="/project/:tagInput" children={<HomePage />} />
            <Route path="/projects/:projectID" exact={true} component={HomePage} />
            <Route path="/profile/:username" component={Profile} />
            <PrivateRoute path="/new-project" exact={true} component={NewProject} />
            <Route path="/login" component={Login} />
            <Route path="/signup" component={SignupPage} />   
            <PrivateRoute path="/new-article" exNewArticlePageact={true} component={NewArticlePage} />
            <PrivateRoute path='/setting/:displayContent' component={SettingPage} exact/>
          </Switch>
        </ConnectedRouter>
			</div>
    );
  }


export default App;