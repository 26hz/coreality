import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {updateDiscusVoteThunk} from '../redux/vote/thunk';
import {disucsAllInfosThunk, createDiscusCommentThunk, createDiscusCommentReplyThunk} from '../redux/discussion/thunk'
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';

import ShareDialog from "../components/ShareDialog";
import MoreTagsModal from "../components/MoreTagsModal";
import SignOptionModal from '../components/SignOptionModal';
import CommentInputReplyMobileModal from '../components/CommentInputReplyMobileModal';
import CommentInputMobileModal from '../components/CommentInputMobileModal';

import avatar from '../assets/img/avatar.png';

import back from '../assets/icon/back.png';

import purpleComment from '../assets/icon/purple-comment.png';
import purpleShare from '../assets/icon/purple-share.png';
import purpleUpvote from '../assets/icon/purple-upvote.png';

function DetailDiscussionMobile (props) {
	const dispatch = useDispatch();
	const allDiscussions = useSelector(state => state.discus.discusInfos.allDiscussion);
	const allDiscusUsers = useSelector(state => state.discus.discusInfos.allDiscusUsers);
	const allDiscusComment = useSelector(state => state.discus.discusInfos.discusComments);
	const allDiscusCommentsUsers = useSelector(state => state.discus.discusInfos.allDiscusCommentsUsers);
	const allDiscusCommentReply = useSelector(state => state.discus.discusInfos.discusCommentReplys);
	const allDiscusCommentReplysUsers = useSelector(state => state.discus.discusInfos.allDiscusCommentReplysUsers);
	const discusCommentCount = useSelector(state => state.discus.discusInfos.commentCount);
	const discusVotes = useSelector(state => state.discus.discusInfos.discusVotes);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const userInfos = useSelector(state => state.user.userInfos);
	
	useEffect(() => {
		dispatch(disucsAllInfosThunk());
	},[dispatch])
	
	const [description, setDescription] = useState("");
	const [isSignOptionModalShown, setIsSignOptionModalShown] = useState(false);
	const [isDisplayShare, setShareShown] = useState(false);
	const [isMoreTagsShown, setTagShown] = useState(false);
	const [dataTags, setDataTags] = useState([]);
	const [shareData, setShareData] = useState({});
	const [discussion, setDiscussion] = useState([]);
	const [index, setIndex] = useState(null);
	const [discussionUsers, setDiscussionUsers] = useState([]);
	const [comments, setComments] = useState([]);
	const [commentUsers, setCommentUsers] = useState([]);
	const [commentsCount, setCommentsCount] = useState([]);
	const [reply, setReply] = useState([]);
	const [replyUsers, setReplyUsers] = useState([]);
	const [votes, setVotes] = useState([]);
	const [commentID, setCommentID] = useState(null);
	const [replyIndex, setReplyIndex] = useState(null);
	const [isCommentParentModalDisplay, setIsCommentParentModalDisplay] = useState(false);
	const [isCommentChildModalDisplay, setIsCommentChildModalDisplay] = useState(false);
	const forceUpdate = React.useState()[1].bind(null, {});

	useEffect(() => {
		const { id } = props.match.params;
		if(allDiscussions !== undefined && allDiscussions.length > 0){
			var indexElement = allDiscussions.findIndex(element => element.id === parseInt(id));
			setIndex(indexElement);
			setDiscussion(allDiscussions[indexElement]);
			setDiscussionUsers(allDiscusUsers.discusUsers[indexElement]);
			setCommentsCount(discusCommentCount.commentCountNums[indexElement]);
		}
	},[allDiscussions])
	
	useEffect(() => {
		if(allDiscusComment && allDiscusComment.length > 0){
			var dataDiscussionComments = allDiscusComment[index];
			if(dataDiscussionComments !== undefined && dataDiscussionComments.length > 0) {
				setComments(dataDiscussionComments);
				setCommentUsers(allDiscusCommentsUsers.allDiscusCommentUsers[index]);
			}
		}
	},[allDiscusComment])

	useEffect(() => {
		if(allDiscusCommentReply && allDiscusCommentReply.length > 0){
			setReply(allDiscusCommentReply);
			setReplyUsers(allDiscusCommentReplysUsers.allDiscusCommentUsersReplys);
		}
	},[allDiscusCommentReply])

	useEffect(() => {
		if(discusVotes !== undefined && discusVotes.length > 0 && index !== null) {
			setVotes(discusVotes[index][0]);
		}
	},[discusVotes, index])
	
	const toggleUpvote = () => {
		if (isAuthenticated){
			var data = votes;
			let newObj;
			if(data[index].length > 0){
				newObj = data[index][0];
				newObj.voted = !newObj.voted;
			} else {
				data[index].push({});
				newObj =  data[index][0];
				newObj.voted = true;
			}
			var dataDiscussion = discussion;
			if(newObj.voted){
				dataDiscussion.vote = parseInt(dataDiscussion.vote) + 1;
			} else {
				dataDiscussion.vote = parseInt(dataDiscussion.vote) - 1;
			}
			dispatch(updateDiscusVoteThunk(newObj.voted, newObj.discus_id));
			setDiscussion(dataDiscussion)
			forceUpdate();
		} else {
			displaySignOptionModal()
		}
	}
	
	const handleShare = (e, id) => {
		if (navigator.share) {
			navigator.share({
				title: 'Coreality',
				url: window.location.href + "/discussions/" + id
			}).then(() => {
				console.log('Thanks for sharing!');
			})
			.catch(console.error);
		} else {
			var newObj;
			setShareShown(true);
			newObj = discussion[index];
			setShareData(newObj);
		}
	}
	
	const handleCloseShare = () => {
		setShareShown(false);
	}
	
	const displayMoreTags = () => {
		setTagShown(true);
		setDataTags(discussion);
	}
	
	const closeTagsModal = () => {
		setTagShown(false)
	}
	
	const handleComment = (event) => {
		setDescription(event.target.value);
	}
	
	const submitComment = async (placename, discus_id, comment_id) => {
		if (isAuthenticated){
			if(placename === "child"){
				dispatch(createDiscusCommentReplyThunk(description, discus_id, comment_id));
			} else if(placename === "parent"){
				dispatch(createDiscusCommentThunk(description, discus_id));
			}
			setTimeout(() => { dispatch(disucsAllInfosThunk()); }, 500);
			setDescription("");
			forceUpdate();
		} else {
			displaySignOptionModal()
		}
	}
	
	const displaySignOptionModal = () => {
		setIsSignOptionModalShown(true)
	}
	
	const hideSignOptionModal = () => {
		setIsSignOptionModalShown(false)
	}

	const displayParentCommentModal = () => {
		setIsCommentParentModalDisplay(true);
	}

	const hideParentCommentModal = () => {
		setIsCommentParentModalDisplay(false);
	}

	const displayChildCommentModal = (id, replyIndex) => {
		setCommentID(id);
		setReplyIndex(replyIndex);
		setIsCommentChildModalDisplay(true);
	}

	const hideChildCommentModal = () => {
		setIsCommentChildModalDisplay(true);
	}

	const avator = 	<img className="discussion-avatar mr-3" src={avatar} alt="default-avatar"/>
	const authAvator = <img className="discussion-avatar mr-3" src={userInfos && userInfos.avator} alt="user-avatar"/>

	return (
		<div className="overflow-x-hidden">
			{
				isDisplayShare &&
				<ShareDialog 
					data = {shareData}
					hideShare = {handleCloseShare}
				/>
			}

			{
				isMoreTagsShown &&
				<MoreTagsModal 
					data = {dataTags}
					hideModal = {closeTagsModal}
				/>
			}

			{
				isSignOptionModalShown &&
				<SignOptionModal
					hideSignOptionModal = {hideSignOptionModal}
				/>
			}

			{
				isCommentParentModalDisplay &&
				<CommentInputMobileModal
					data = {discussion}
					commentUser = {commentUsers}
					handleComment = {handleComment}
					submitComment = {submitComment}
					hideParentCommentModal = {hideParentCommentModal}
				/>
			}

			{
				isCommentChildModalDisplay &&
				<CommentInputReplyMobileModal
					data = {discussion}
					commentID = {commentID}
					replyIndex = {replyIndex}
					commentReplyUser = {replyUsers}
					handleComment = {handleComment}
					submitComment = {submitComment}
					hideChildCommentModal = {hideChildCommentModal}
				/>
			}

			<div className="discussion-detail mt-5 mx-3 bg-white p-3">
				<Link to="/discussions">
					<div className="mb-3">
						<img src={back} className="back-modal-icon"/>
					</div>
				</Link>
				<div className="d-flex align-items-center justify-content-between mb-3">
					<div className="d-flex align-items-center">
						<img src={discussionUsers.avator} className="discussion-avatar mr-3" />
						<div className="d-flex">
							<div className="text-16 mr-3">{discussionUsers.displayname}</div>
							<div className="text-16 text-gray">
								{moment( new Date(discussion.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
							</div>
						</div>
					</div>
				</div>
				<div className="discussion-question text-20 mb-2">{discussion.title}</div>
				<div className="discussion-description text-gray mt-3">{discussion.description}</div>
				<div className="tags-container d-block text-14 mt-3 w-100">
					{ discussion.tags && discussion.tags.map((tag, j) =>
						<div key={j} className={"tag w-100 p-2 mr-2 " + (j > 1 ? "hide-in-mobile" : "")}>{tag}</div>
					)}
					<div className={"btn-more w-100 tag px-3 " + (discussion.tags && discussion.tags.length > 2 ? "d-block" : "d-none")} onClick={displayMoreTags}>
						more
					</div>
				</div>
				<div className="d-flex justify-content-between align-items-center mt-3 ">
					<div className="comment-btn btn btn d-flex align-items-center mr-2 text-navy c-pointer">
						<img src={purpleComment} className="preview-icon mr-2" />
						{commentsCount}
					</div>
					<div onClick={(e) => handleShare(e, discussion.id)} className="share-btn btn d-flex align-items-center text-navy c-pointer">
						<img src={purpleShare} className="preview-icon mr-2" />
						<span className="text-14 font-bold">SHARE</span>
					</div>
				</div>
				<div className="d-flex justify-content-end align-items-center mt-4">
					<div onClick={() => toggleUpvote("discussion")} className={"upvote-btn text-navy c-pointer " + (votes.voted ? "active" : "") }>
						<img src={purpleUpvote} className="preview-icon mr-4" />
						{discussion.vote}
					</div>
				</div>
				<div className="d-block mt-5">
					<div onClick={() => displayParentCommentModal(discussion.id)} className="form-input-discussion-container d-flex align-items-center mt-3 mb-6 ">
						{isAuthenticated ? authAvator : avator}
						<div className="input-discussion-container p-relative">
							<input type="text" name="myComment" className="input-discussion" placeholder="Start a new comment..." />
						</div>
					</div>
					<div className="comment-list">
						<div className="my-5 text-20">
							Comments
						</div>
						{ comments && comments.map((comment, j) =>
							<div key={j} className="user-comment">
								<div className="d-flex align-items-center">
									{
										commentUsers !== undefined && commentUsers[j] !== null && commentUsers[j] !== undefined ?
											<img src={commentUsers[j] && commentUsers[j].avator} className="discussion-avatar mr-3" />
											:
											avator
									}
									<div className="d-flex">
										{
												commentUsers !== undefined && commentUsers[j] !== null && commentUsers[j] !== undefined ?
												<div className="text-16 mr-3">{commentUsers[j] && commentUsers[j].displayname}</div>
												:
												<div className="text-16 mr-3">User Not Found</div>
										}
										<div className="text-16 text-gray">
											{moment( new Date(comment.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
										</div>
									</div>
								</div>
								<div className="comment text-gray">
									{comment.description}
								</div>
								<div onClick={() => displayParentCommentModal()} className="reply-btn px-3 py-2 mt-3">
									Reply
								</div>
								<div className="comment-reply mt-4 ml-6">
									{ reply && reply.map((reply, k) =>
										<div className={(reply.comment_id === comment.id ? "d-block" : "d-none")} key={k}>
											<div>
												<div className="d-flex align-items-center">
													{
														replyUsers[k] !== undefined && replyUsers[k] !== null ?
															<img src={replyUsers[k].avator} className="discussion-avatar mr-3" />
															: avator
													}
													<div className="d-flex">
														<div className="mr-3">
															{
																replyUsers[k] !== undefined && replyUsers[k] !== null?
																	<div className="text-16">{replyUsers[k].displayname}</div>
																		: <div className="text-16">User Not Found</div>
															}
														</div>
														<div className="text-16 text-gray">
															{moment( new Date(comment.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}
														</div>
													</div>
												</div>
												<div className="comment text-gray">
													{reply.description}
												</div>
												<div onClick={() => displayChildCommentModal(comment.id, k)} className="reply-btn px-3 py-2 mt-3">
													Reply
												</div>
											</div>
										</div>
									)}
									</div>
								</div>
							)
						}
					</div>
				</div>
			</div>
		</div>
	);
};

export default DetailDiscussionMobile;