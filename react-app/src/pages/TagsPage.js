import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux'
import TagList from '../components/TagList';
import {tagInfos} from '../redux/user/thunk'

import search from '../assets/icon/search.png';
import searchIcon from '../assets/icon/search-icon.png';

import { setEmitFlags } from 'typescript';

function TagsPage (props) {
	const dispatch = useDispatch();
	const [tags, setTags] = useState([])
	const [tagsAll, setTagAll] = useState([])
	const [popularTags1, setPopularTags1] = useState([]);
	const [popularTags2, setPopularTags2] = useState([]);
	const [isSelectDisplay, setIsSelectDisplay] = useState(false);
	const unfilteredTags = useSelector(state => state.user.tagInfos);
	var { type } = props.match.params;
	const [typeName, setTypeName] = useState(type);

	useEffect(()=>{
		dispatch(tagInfos());
	},[dispatch])
	
	useEffect(()=>{
		//set active users data
		if(unfilteredTags !== undefined){
			checkDuplicate()
		}
	},[unfilteredTags, typeName])
	
	const checkDuplicate = () => {
		const resultArray = [];
		var tempTags;
		if(typeName === "project"){
			tempTags = unfilteredTags[0];
		} else if(typeName === "discussion"){
			tempTags = unfilteredTags[1];
		} else {
			tempTags = unfilteredTags[2]
		}
		tempTags && tempTags.map( item => {
			//for each item in arrayOfObjects check if the object exists in the resulting array
			if(resultArray.find(object => {
				if(object.tagName.toLowerCase() === item.tagName.toLowerCase()) {
					object.tagCountNum = object.tagCountNum + item.tagCountNum;
					return true;
					//if it does not return false
				} else {
					return false;
				}
			})){
			} else {
				//if the object does not exists push it to the resulting array and set the times count to 1
				resultArray.push(item);
			}
		})
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < resultArray.length; i += 1) {
				if (resultArray[i - 1].tagCountNum < resultArray[i].tagCountNum) {
					done = false;
					var tmp = resultArray[i - 1];
					resultArray[i - 1] = resultArray[i];
					resultArray[i] = tmp;
				}
			}
		}

		setTags(resultArray);
		setTagAll(resultArray);

		var slicedArray = resultArray.slice(0, 4).map((data, i) => {
			return data
		})
		setPopularTags1(slicedArray);

		slicedArray = resultArray.slice(4, 8).map((data, i) => {
			return data
		})
		setPopularTags2(slicedArray);
	}

	const handleInputChange = (e)=>{
		const target = e.target;
		const value = target.value.trim();
		 
		var temp  = tags.filter((tag) => {
			if(tag.tagName.includes(value)){
				return tag;
			}
		})

		if(value === ""){
			setTags(tagsAll);
		} else {
			setTags(temp)
		}
	}

	const displaySelect = () => {
		setIsSelectDisplay(true)
	}

	const changeTags = (typeName) => {
		setTypeName(typeName);
		setIsSelectDisplay(false)
	}

	return (
		<div>
			<div className="popular-creator margin-auto">
				<div className="p-relative d-flex align-items-center justify-content-between">
					<div className="text-20 font-bold mb-4">Popular Tags</div>
					<div className={"d-flex align-items-center c-pointer filter-btn " + (isSelectDisplay ? "active" : "")} onClick={displaySelect}>
						<div className="text-14 font-bold ml-2 text-navy" onClick={displaySelect}>SEARCH BY</div>
						<img src={search} className="search-icon"/>
					</div>
					<div className={"select-modal " + (isSelectDisplay ? "d-block" : "d-none")}>
						<div onClick={() => changeTags("project")} className={"select-child " + (typeName === "project" ? "active" : "")}>
							PROJECTS
						</div>
						<div onClick={() => changeTags("discussion")} className={"select-child " + (typeName === "discussion" ? "active" : "")}>
							DISCUSSIONS
						</div>
						<div onClick={() => changeTags("article")} className={"select-child " + (typeName === "article" ? "active" : "")}>
							ARTICLES
						</div>
					</div>
				</div>
				<div className="bg-white bg-white-border px-3 pt-3">
					<div className="row px-3">
						<div className="columns">
							<TagList 
								tags = {popularTags1}
								type = {typeName}
							/>
						</div>
						<div className="columns">
							<TagList 
								tags = {popularTags2}
								type = {typeName}
							/>
						</div>
					</div>
				</div>
				<div className="text-20 font-bold mt-6 mb-4 mt-5">All XR Creators</div>
				<div className="bg-white bg-white-border px-3 pt-3 mb-6">
					<div className="input-tag-container p-relative mb-6">
						<input
							type="text"
							component="input"
							placeholder="What are you looking for?" 
							onChange={handleInputChange} 
							className="pr-6 w-100" 
							name="title" 
						/>
						<img src={searchIcon} className="input-icon-checklist" />
					</div>
					<TagList 
						tags = {tags}
						type = {typeName}
					/>
				</div>
			</div>
		</div>
	);
}

export default TagsPage;