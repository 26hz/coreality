import React, { useState } from 'react';
import squareThumbnail from '../assets/img/square-thumbnail.png';
import wrongIcon from '../assets/icon/wrong.png';
import checkedIcon from '../assets/icon/checked.png';
import questionMarkIcon from '../assets/icon/question-mark.png';
import purpleComment from '../assets/icon/purple-comment.png';
import purpleShare from '../assets/icon/purple-share.png';
import purpleUpvote from '../assets/icon/purple-upvote.png';
import rectangleThumbnail from '../assets/img/rectangle-thumbnail.png';
import { useForm } from "react-hook-form";
import { Form, Input, CustomInput, FormGroup, Alert } from "reactstrap";
import { createProjectThunk } from '../redux/project/thunk';
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from "react-router-dom";
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'

function useForceUpdate(){
	const [value, setValue] = useState(0); // integer state
	return () => setValue(value => value + 1); // update the state to force render
}

export default function ProfileProject() {
    let history = useHistory();
	let msg = useSelector(state => state.proj.msg)
    let alertmsg = <Alert className='alertSuccess'>{msg}</Alert>
	// step1
	const [thumb, setThumb] = React.useState(null);
	const [selectedFile, setSelectedFile] = React.useState();
	const [isFilePicked1, setIsFilePicked1] = React.useState(false);
	const [projectStatus, setProjectStatus] = React.useState(true);
	const [projectName, setProjectName] = React.useState('Insert Title');
	const [headline, setHeadline] = React.useState('Insert Headline');
	let [tag, setTag] = React.useState('');
	let [tags, setTags] = React.useState([]);
	const forceUpdate = useForceUpdate();

	//step2
	const [isFilePicked2, setIsFilePicked2] = React.useState(false);
	const [isFilePicked3, setIsFilePicked3] = React.useState(false);
	let [galleryURL, setgalleryURL] = React.useState([]);
	let [galleryFiles, setgalleryFiles] = React.useState([null]);
	const [videoLink, setvideoLink] = React.useState(null);
	const [description, setdescription] = React.useState('Insert Description');
	let [isModalShown, setisModalShown] = React.useState(false);
	const [projectLink, setprojectLink] = React.useState('Insert project Link');
	// let [participant, setParticipant] = React.useState('');
	// let [participants, setParticipants] = React.useState([]);
	let [isImagesAboveMax, setIsImagesAboveMax] = React.useState(false);
	let [isTagsAboveMax, setIsTagsAboveMax] = React.useState(false);
	const [editor, setEditor] = React.useState(BraftEditor.createEditorState(null));

	const controls = [
		'undo', 'redo', 'separator',
		'font-size', 'line-height', 'letter-spacing', 'separator',
		'text-color', 'bold', 'italic', 'underline', 'strike-through', 'separator',
		'superscript', 'subscript', 'remove-styles', 'emoji',  'separator', 'text-indent', 'text-align', 'separator',
		'headings', 'list-ul', 'list-ol', 'blockquote', 'code', 'separator',
		'separator', 'hr', 'separator',
		'separator',
		'clear'
	]

	// step1
	const changeSingleHandler = (e) => {
		setSelectedFile(e.target.files[0]);
		setIsFilePicked1(true);
		if (e.target.files[0] == null){
			return null
		} else{
			let imgvalue = URL.createObjectURL(e.target.files[0])
			setThumb(imgvalue)
		}
	};

	const removeTag = (e, i) => {
		setIsTagsAboveMax(false);
		tags.splice(i,1);
		setTags(tags);
		forceUpdate();
	}

	const handleInputChange1 = (e)=>{
		const target = e.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		if (target.name === 'status'){
			setProjectStatus(value);
		} else if (target.name === "title"){
			setProjectName(value)
		} else if (target.name ==='headline') {
			setHeadline(value)
		} else {
			setTag(value)
		}
	}

	const handleTag = (e) =>{
		if(e.keyCode === 13){
			tag = tag.trim();
			if(tags.length > 3){
				setIsTagsAboveMax(true);
			} else {
				setIsTagsAboveMax(false);
				if(tag !== ""){
					setTags(tags.concat(tag))
					tag = '';
					setTag(tag)
				}
			}
		} 
	}
	
	// step 2
	let fileObj = [];
	let fileArray = [];
	const uploadMultipleFiles = (e)=> {
		setIsFilePicked2(true);
		setIsImagesAboveMax(false)
		if (e.target.files[0] == null){
			return null
		} else if(e.target.files.length > 4){
			setIsImagesAboveMax(true)
		} else {
			fileObj.push(e.target.files);
			for (let i = 0; i < fileObj[0].length; i++) {
				fileArray.push(URL.createObjectURL(fileObj[0][i]))
			}
			setgalleryURL( galleryURL = fileArray)
			setgalleryFiles( galleryFiles = fileObj[0])
		}
	}
	const handleEditorChange = (editorState) => {
        setEditor(editorState)
    }

	const handleInputChange2 = (e) => {
		const target = e.target;
		const value = target.value;
		if (target.name === 'projectLink'){
			setprojectLink(value);
		}
		// else {
		// 	setParticipant(value)
		// }
	}

	// const handleParticipant = (e) =>{
	// 	if(e.keyCode === 13){
	// 		participant = participant.trim();
	// 		setParticipants(participants.concat(participant))
	// 		participant = '';
	// 		setParticipant(participant)
	// 	} 
	// }
	
	// const removeParticipant = (e, i)=>{
	// 	participants.splice(i,1);
	// 	setParticipants(participants);
	// 	forceUpdate();
	// }

	const { register, handleSubmit } = useForm({
		mode: 'onSubmit',
		reValidateMode: 'onChange',
	});

	const dispatch = useDispatch();

	const onSubmit = (data) => {
		const { title, headline, status, link, videoLink  } = data;
		const description = editor.toHTML()
		dispatch(createProjectThunk(title, headline, status, description, link, videoLink, galleryFiles, selectedFile, tags ));
		// setTimeout(function(){history.goBack()}, 3000);
		// setTimeout(function(){
		// 	alertmsg = ''
		// }, 6000);
	};
	const checkKeyDown = (e) => {
		if (e.code === 'Enter') e.preventDefault();
	  };

	let imgPreview;
	let imgDefault = <img className="new-project-thumbnail mr-3" src={squareThumbnail} alt='Project thumb' />
	if (thumb) {
		imgPreview = <img className="new-project-thumbnail mr-3" alt="avatar" src={thumb} alt='Project thumb' />;
	}

	const handleInputChangeVideo = (e) =>{
		const target = e.target;
		const value = target.value;
		if (target.name === 'upload-video'){
			if (e.target.files[0] == null){
				setIsFilePicked3(false);
				return null
			} else{
				setvideoLink(e.target.files[0])
				setIsFilePicked3(true);
			}
		} else if (target.name ==='video-link') {
			setvideoLink(value)
			setIsFilePicked3(false);
		} 
	}

		return (
			<div>
				<Form onSubmit={handleSubmit(onSubmit)} onKeyDown={(e) => checkKeyDown(e)} id="projectForm">
				<div className="d-flex new-project-container justify-content-center mx-6 flex-direction-column-mobile">
				<div className="content-container pt-6 pb-2">
					<div className="text-24 mb-4">Tell us about your project</div>
					<div className="content bg-white p-4 pr-6 mb-4">
						<div>
							<div className="label mb-2">Name of the project</div>
							<FormGroup className="input-container mb-1">
								<Input 
								type="text"
								innerRef={register} 
								component="input"
								placeholder="Project Name" 
								onChange={handleInputChange1} 
								className="pr-6" 
								name="title" 
								/>
								<img src={checkedIcon} className="input-icon-checklist" />
							</FormGroup>
							<div className="text-12 text-gray">Assistive text</div>
						</div>
						<div className="mt-4">
							<div className="label mb-2">Headline</div>
							<FormGroup className="input-container mb-1">
								<Input  
								type="text" 
								innerRef={register}
								component="input"
								name="headline" 
								onChange={handleInputChange1} 
								className="pr-6" 
								placeholder="Add a descriptive headline for your project" 
								/>
								<div className="tooltip">
									<img src={questionMarkIcon} className="input-icon-question" />
									<span className="tooltiptext text-left">Tell us what the product does</span>
								</div>
							</FormGroup>
							<div className="text-12 text-gray">Assistive text</div>
						</div>

						<div className="mt-4">
							<div className="label mb-2">Tags</div>
							<div className="input-container mb-1">
								<Input 
								type="text"
								component="input"
								name="tag" 
								onKeyUp={handleTag} 
								value={tag} 
								onChange={handleInputChange1} 
								placeholder="Add tags related to your project" 
								/>
									<div className="tooltip">
										<img src={questionMarkIcon} className="input-icon-question" />
										<span className="tooltiptext text-left">Select the tags that best describe the project. Maximum 4</span>
									</div>
							</div>
								<div className="text-12 text-gray">Assistive text</div>
								<div className="tags-container w-100 mt-2 mb-5">
									{tags.map((e, i) =>
										<div key={i} className="tag-input mr-2 mb-2">{e}<img src={wrongIcon} onClick={removeTag} className="ml-2 c-pointer"/></div>
									)}
									<Alert color="danger" className={isTagsAboveMax ? "d-block text-14 mt-2" : "d-none"}>Maximum tags amount is 4</Alert>
							</div>
						</div>

						<div className="d-flex flex-direction-column-mobile mt-1 mb-4">
							<img className="mr-2 new-article-default-image" src={squareThumbnail}/>
							<div className="d-flex new-article-image-inputs justify-content-between flex-direction-column">
								<Input 
								type="file" 
								component="input"
								id="files"
								hidden 
								onChange={changeSingleHandler} 
								/>
								<label htmlFor="files" className="text-navy mr-1">Upload an image</label>
								<div>
									<div className="text-gray mb-2">
										Recommended Size  760 x 760 px
									</div>
									<div className="text-gray">
										JPEG, PNG size: 2MB
									</div>
								</div>
							</div>
						</div>

						<div className="mt-4">
							<div className="label mb-2">Project Status</div>
							<FormGroup className="input-container mb-2 d-flex align-items-center">
								<Input
									className="mr-2"
									innerRef={register}
									component="input"
									name="status"
									onChange={handleInputChange1}
									type="checkbox"
									checked={projectStatus}
									/>
									This product is available           
							</FormGroup>
						</div>
					</div>
				</div>

				<div className="preview-container preview-new-project hide-in-mobile d-flex align-items-center p-3 ml-4 bg-white">
							{imgPreview? imgPreview : imgDefault }
						<div>
							<div className="text-16 mb-2">{projectName !== '' ? projectName : "Project Name"}</div>
							<div className="text-gray text-14 mb-2">{headline}</div>
							<div className={"tags-container mb-2 " + (tags.length > 0 ? "d-none" : "d-block")}>
								<div className="tag mr-1">JavaScript</div>
								<div className="tag">webAR</div>
							</div>
							<div className={"tags-container mb-2 " + (tags.length > 0 ? "d-block" : "d-none")}>
								{
									tags.length > 0 && 
									tags.map((e, i) =>
										<div key={i} className="tag mb-2 mr-2">{e}</div>
									)
								}
							</div>
							<div className="d-flex justify-content-between align-items-center w-100">
								<div className="d-flex">
									<div className="comment-btn btn d-flex mr-2 text-navy c-pointer">
										<img src={purpleComment} className="preview-icon mr-2" />
										0
									</div>
									<div className="share-btn btn d-flex text-navy c-pointer">
										<img src={purpleShare} className="preview-icon mr-2" />
										0
									</div>
								</div>
								<div className="text-navy c-pointer upvote-btn">
									<img src={purpleUpvote} className="preview-icon mr-2" />
									0
								</div>
							</div>
						</div>
					</div>
			</div>					
			<div className="d-flex new-project-container justify-content-center mx-6">
				
				<div className="content-container pt-6 pb-2 mr-4 new-project-col">
					<div className="text-24 mb-4">Show your best work</div>
					<div className="content bg-white p-4 pr-6 mb-4 p-relative">
						<div className="d-flex justify-content-between align-items-center">
							<div className="text-20 mb-2">Gallery</div>
							<div className="tooltip">
								<img src={questionMarkIcon} className="input-icon-2" />
								<span className="tooltiptext">The first image will be used as a preview on social media</span>
							</div>
						</div>
						<div className="text-16 text-gray mt-1">Recommended Size  1270 x 760 px</div>
						<div className="text-16 my-2">Upload images, videos or 3D assets to show in your gallery</div>
						
						<div className="w-100 preview-default">
							<div className="text-16 my-2 d-flex justify-content-center align-items-center flex-direction-column main-preview-input">
								<div className="text-navy mb-1 d-flex">
									<label className="text-navy mr-1">
										<Input 
										type="file" 
										name="galleryFiles" 
										id="files" 
										multiple={true} 
										hidden 
										onChange={uploadMultipleFiles} 
										/>
										Upload an image
									</label>
									{/* <span className="text-gray mx-2">or</span> */}
									{/* <div className="c-pointer" onClick={displayModal} >Paste video URL</div> */}
								</div>
								<span>Upload up to a max of 4 files</span>
							</div>
							<Alert color="danger" className={isImagesAboveMax ? "d-block mb-2" : "d-none"}>Maximum images amount is 4</Alert>
							<div className="preview-images-container d-flex justify-content-between mt-1 preview-images">
								<img src={rectangleThumbnail} />
								<img src={rectangleThumbnail} />
								<img src={rectangleThumbnail} />
								<img src={rectangleThumbnail} />
							</div>
							<div className="mt-4 w-100">
								<div className="label mb-2">Paste video URL or Upload your own video</div>
								<div className="d-flex">
									<div className="mr-2 w-100" >
									{isFilePicked3==false?
									<Input 
										type="text"
										innerRef={register}
										onChange={handleInputChangeVideo}
										id="video-link" 
										name="video-link"
										className="pr-1" 
										placeholder="http://www.example.com" 
									/>:
									<Input 
										type="text"
										value=""
										className="pr-1" 
										disabled="disabled"
									/>									
									}
									</div>
									<div className="ml-2 w-100">
										<CustomInput 
											type="file" 
											innerRef={register} 
											onChange={handleInputChangeVideo}
											id="upload-video" 
											name="upload-video" 
											label="Upload video"
										/>
									</div>
								</div>
							</div>
							<FormGroup className="mt-4">
								<div className="label mb-2">Description</div>
								<div className="my-component">
								<BraftEditor
								value={editor}
								controls={controls}
								ref={register} 
								onChange={handleEditorChange}
								onSave={onSubmit}
								name="description"
								language="en"
								/>
							</div>
							</FormGroup>
							<div className="mt-4">
								<div className="label mb-2">Project Link</div>
								<FormGroup className="input-container mb-1">
									<Input 
									type="text"
									innerRef={register} 
									name="link" 
									onChange={handleInputChange2} 
									className="pr-6" 
									placeholder="http://www.example.com" 
									/>
									<img src={questionMarkIcon} className="input-icon-question-project" />
								</FormGroup>
							</div>
							{/* <div className="mt-4">
								<div className="label mb-2">Who participated in this project?</div>
								<div className="input-container mb-1">
									<Input 
									type="text" 
									name="participant" 
									className="pr-6"  
									placeholder="Who participated in the project" 
									onKeyUp={handleParticipant} 
									value={participant} 
									onChange={handleInputChange2} 
									/>
								</div>
								<div className="tag-container d-block align-items-center mt-2">
									{ participants.map((e, i) =>
										<div key={i} className="participants align-items-center mr-2"><img src={avatar} className="avatar-small-icon mr-2" /> {e}<img src={wrongIcon} onClick={removeParticipant} className="ml-2 c-pointer"/></div>
									)}
									</div>
								</div> */}
							</div>
							{!msg? msg : alertmsg}
							<div className="mt-4 mb-3 buttons-container">
							<Input type="submit" className="btn-next ml-3" value="Publish"/>
							</div>
					</div>
				</div>

				<div className="preview-container hide-in-mobile p-3 ml-4 bg-white bg-white-border new-project-col p-5 mr-5">
					<img src={galleryURL[0] == null ? rectangleThumbnail : galleryURL[0]} className="main-preview-image w-100 mb-2" />
						{ galleryURL[0] == null ?
							<div className="d-flex justify-content-between mt-1 preview-images">
								<img src={rectangleThumbnail} />
								<img src={rectangleThumbnail} />
								<img src={rectangleThumbnail} />
								<img src={rectangleThumbnail} />
							</div>
							:
							<div className="d-flex justify-content-between multi-preview mt-1 preview-images">
								{(galleryURL || []).map((url, i) => (
									<img key={i} src={url} alt="preview" />
								))}
							</div>
						}
					<div className="mt-4">
						{/* <video 
						controls 
						autoPlay
						src={videoSrc} 
					/> */}
					</div>
					<div className="mt-4 mb-1">
						{description === '' ? "consectetur....Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consectetur...." : description}
					</div>
					<div className="mt-2 mb-5">Do you have any questions? Contact <span className="text-navy">@username</span></div>
				</div>
			</div>
			</Form>
			</div>
		);
	
}
