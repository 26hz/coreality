import React, { useState, useEffect } from 'react';
import { userInfosByVotesThunk} from '../redux/user/thunk'
import {useSelector, useDispatch} from 'react-redux'
import UserList from '../components/UserList';

import search from '../assets/icon/search.png';


function AllUsers () {
	const [activeUsers, setActiveUsers] = useState([]);
	const dispatch = useDispatch();
	const users = useSelector(state => state.user.XRCreatorsInfos)

	useEffect(()=>{
		setActiveUsers(users);
	},[users])

	useEffect(()=>{
		dispatch(userInfosByVotesThunk());
	},[dispatch])

	return (
		<div>
			<div className="popular-creator margin-auto">
				<div className="d-flex align-items-center justify-content-between">
					<div className="text-20 font-bold mb-4">Popular XR Creators</div>
					<div className="d-flex align-items-center c-pointer">
						<img src={search} className="search-icon"/>
						<div className="text-14 font-bold ml-2 text-navy">SEARCH BY</div>
					</div>
				</div>
				<div className="bg-white bg-white-border px-3 pt-3">
						<UserList 
							users = {activeUsers}
							type = "activeUsers"
						/>
				</div>
				<div className="text-20 font-bold mt-6 mb-4 mt-5">All XR Creators</div>
				<div className="bg-white bg-white-border px-3 pt-3">
					<UserList 
						users = {users}
						type = ""
					/>
				</div>
			</div>
		</div>
	);
}

export default AllUsers;