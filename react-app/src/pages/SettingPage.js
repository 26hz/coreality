import React from 'react';

import SettingHeader from '../components/SettingHeader';
import SettingAlert from '../components/SettingAlert';
import SettingProfile from '../components/SettingProfile';
import SettingAvatar from '../components/SettingAvatar';
import SettingEmailPassword from '../components/SettingEmailPassword';
import SettingAccount from '../components/SettingAccount';

function SettingPage (props){
	const { displayContent } = props.match.params;
	return (
		<div>
			<SettingHeader displayContent={displayContent} />
			{(() => {
				switch (displayContent) {
					case 'profile':
						return (
							<SettingProfile />
						)
					case 'avatar':
						return (
							<SettingAvatar />
						)
					case 'email':
						return (
							<SettingEmailPassword />
						)
					case 'account':
						return (
							<SettingAccount />
						)
					default:
						return (
							<SettingProfile />
						)
				}
			})()}
		</div>
	);
}

export default SettingPage;