import React, { useState, useEffect } from 'react';
import avatar from '../assets/img/avatar.png';
import squareThumbnail from '../assets/img/square-thumbnail.png';
import editIcon from '../assets/icon/edit.png';
import checkedIcon from '../assets/icon/checked.png';
import wrongIcon from '../assets/icon/wrong.png';
import questionMarkIcon from '../assets/icon/question-mark.png';
import { useSelector, useDispatch } from 'react-redux'
import { Form, Input, FormGroup, NavLink, Alert } from "reactstrap";
import {createArticleThunk} from "../redux/article/thunk"
import { useForm } from "react-hook-form";
import {userInfosThunk} from "../redux/user/thunk"
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
// import { useHistory } from "react-router-dom";

function useForceUpdate(){
	const [value, setValue] = useState(0); // integer state
	return () => setValue(value => value + 1); // update the state to force render
}

export default function NewArticlePage() {
    // let history = useHistory();
	let msg = useSelector(state => state.art.msg)
    let alertmsg = <Alert className='alertSuccess'>{msg}</Alert>
	const [coverImage, setCoverImage] = React.useState(null);
	const [selectedFile, setSelectedFile] = React.useState();
	const [isFilePicked, setIsFilePicked] = React.useState(false);
	const [articleName, setArticleName] = React.useState('');
	const [description, setdescription] = React.useState('');
	const [editor, setEditor] = React.useState(BraftEditor.createEditorState(null));
	
	let [isModalShown, setisModalShown] = React.useState(false);
	let [tag, setTag] = React.useState('');
	let [tags, setTags] = React.useState([]);
	let [isTagsAboveMax, setIsTagsAboveMax] = React.useState(false);
	const forceUpdate = useForceUpdate();

	const handleInputChange = (e)=>{
		const target = e.target;
		const value = target.value;
		if (target.name === 'title'){
			setArticleName(value);
		} else if (target.name === "description"){
			setdescription(value)
		}  else {
			setTag(value)
		}
	}

	const handleEditorChange = (editorState) => {
        setEditor(editorState)
    }

	const handleTag = (e) =>{
		if(e.keyCode === 13){
			tag = tag.trim();
			if(tags.length > 3){
				setIsTagsAboveMax(true);
			} else {
				setIsTagsAboveMax(false);
				if(tag !== ""){
					setTags(tags.concat(tag))
					tag = '';
					setTag(tag)
				}
			}
		} 
	}
	
	const removeTag = (e, i) => {
		setIsTagsAboveMax(false);
		tags.splice(i,1);
		setTags(tags);
		forceUpdate();
	}

	const onChangeFiles = (e) => {
		setSelectedFile(e.target.files[0]);
		setIsFilePicked(true);
		if (e.target.files[0] == null){
			return null
		} else{
			let imgvalue = URL.createObjectURL(e.target.files[0])
			setCoverImage(imgvalue)
		}
	}

	const { register, handleSubmit } = useForm({
		mode: 'onSubmit',
		reValidateMode: 'onChange',
	});

	const dispatch = useDispatch();
	const onSubmit = (data) => {
		const { title, description} = data;
		const content = editor
		// console.log(content)
		dispatch(createArticleThunk(title, description, content, tags ,selectedFile));
		// setTimeout(function(){history.goBack()}, 3000);
		// setTimeout(function(){
		// 	alertmsg = ''
		// }, 6000);
	};

	useEffect(()=>{
        dispatch(userInfosThunk());
	},[dispatch])
	const userInfos = useSelector(state => state.user.userInfos);

	const checkKeyDown = (e) => {
		if (e.code === 'Enter') e.preventDefault();
	};

	const controls = [
		'undo', 'redo', 'separator',
		'font-size', 'line-height', 'letter-spacing', 'separator',
		'text-color', 'bold', 'italic', 'underline', 'strike-through', 'separator',
		'superscript', 'subscript', 'remove-styles', 'emoji',  'separator', 'text-indent', 'text-align', 'separator',
		'headings', 'list-ul', 'list-ol', 'blockquote', 'code', 'separator',
		'separator', 'hr', 'separator',
		'separator',
		'clear'
	]

		return (
			<Form onSubmit={handleSubmit(onSubmit)} onKeyDown={(e) => checkKeyDown(e)} className="d-flex new-article-container px-5 py-6">
				<div className="mx-5 new-article-col new-article-col-1">
					<div className="text-20 mb-4">Tell us abour your article</div>
					<div className="bg-white p-5">
						<div className="mb-4">
							<div className="label mb-2">Title of the article</div>
							<FormGroup className="input-container mb-1">
								<Input 
								type="text" 
								innerRef={register} 
								className="pr-6" 
								name="title" 
								placeholder="Insert title"
								onChange={handleInputChange} 
								/>
								<img src={checkedIcon} className="input-icon" />
								<div className="text-12 text-gray d-flex justify-content-between">
								</div>
							</FormGroup>
						</div>
						<div className="mb-4">
							<div className="label mb-2">Description</div>
							<FormGroup className="input-container mb-1">
								<textarea 
								ref={register} 
								placeholder="Short description of your article" 
								onChange={handleInputChange} 
								name="description" 
								/>
							</FormGroup>
						</div>
						<div className="mb-4">
							<div className="label mb-2">Write your content</div>
							<FormGroup className="input-container mb-1">
							<div className="my-component">
							<BraftEditor
								value={editor}
								ref={register}
								controls={controls}
								onChange={handleEditorChange}
								onSave={onSubmit}
								name="content"
								language="en"
							/>
							</div>
							</FormGroup>
						</div>
						<div className="label mb-2">Cover Image</div>
						<div className="d-flex flex-direction-column-mobile mt-1 mb-4">
							<img className="mr-2 new-article-default-image" src={squareThumbnail}/>
							<div className="d-flex new-article-image-inputs justify-content-between flex-direction-column">
								<div className="text-gray d-flex">
								<FormGroup>
								<Input 
								type="file" 
								id="files" 
								name="coverImage" 
								hidden 
								onChange={onChangeFiles} 
								/>
								</FormGroup>
								<label htmlFor="files" className="text-navy mr-1">Upload an image</label>
								</div>
								<div>
									<div className="text-gray mb-2">
										Recommended Size  760 x 760 px
									</div>
									<div className="text-gray">
										JPEG, PNG size: 2MB
									</div>
								</div>
							</div>
						</div>
						<div className="mb-4">
							<div className="label mb-2 text-gray">Tags</div>
							<div className="input-container mb-1">
								<input type="text" name="tags" className="pr-6" placeholder="Add tags related to your article" onKeyUp={(e) => handleTag(e)} value={tag} onChange={(e) =>  handleInputChange(e)} />
								<img src={checkedIcon} className="input-icon" />
							</div>
							<div className="tags-container w-100 d-block mt-2 mb-5">
								{ tags.map((e, i) =>
									<div key={i} className="tag-input mr-2 mb-2">{e}<img src={wrongIcon} onClick={(e) => removeTag(e, i)} className="ml-2 c-pointer"/></div>
								)}
								<Alert color="danger" className={isTagsAboveMax ? "d-block text-14 mt-2" : "d-none"}>Maximum tags amount is 4</Alert>
							</div>
						</div>
						<div className="mt-4 mb-3 buttons-container">
							<NavLink to="/profile">
							<button className="btn-cancel p-2">CANCEL</button>
							</NavLink>
							<Input className="btn-primary ml-3" type="submit" value="PUBLISH" />
						</div>
						{!msg? msg : alertmsg}
					</div>
				</div>
				<div className="hide-in-mobile new-article-col-2">
					<div className="text-20 mb-4">Preview</div>
					<div className="preview-article-profile-container d-flex bg-white my-3">
						<img 
							className="article-thumbnail mr-3" 
							src={coverImage ? coverImage : squareThumbnail} />
						<div className="article-top-profile p-relative w-100 py-3 pr-5">
							<img src={editIcon} className="edit-icon-profile-discussion" />
							<div className="text-16 mb-2">{articleName !== '' ? articleName : "What’s the best strategy to reduce your Startup’s operating expenses?"}</div>
							<div className="text-14 text-gray mt-1 mb-3">{description !== '' ? description : "Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit...Lorem ipsum dolor sit amet..."}</div>
							<div className={"tags-container mb-2 " + (tags.length > 0 ? "d-none" : "d-block")}>
								<div className="tag mr-1">JavaScript</div>
								<div className="tag">webAR</div>
							</div>
							<div className={"tags-container mb-2 " + (tags.length > 0 ? "d-block" : "d-none")}>
								{
									tags.length > 0 && 
									tags.map((e, i) =>
										<div key={i} className="tag mb-2 mr-2">{e}</div>
									)
								}
							</div>
							<div className="d-flex align-items-center mb-3">
								<img src={userInfos.avator} className="discussion-avatar mr-3" />
								<div className="d-flex">
									<div className="text-16 mr-3">{userInfos.displayname}</div>
									<div className="text-16 text-gray">
										5d
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</Form>
		);
	}
