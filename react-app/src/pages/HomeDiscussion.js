import React, { useState, useEffect } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import {Input, Form, FormGroup, Alert} from 'reactstrap';
import { useForm } from "react-hook-form";
import { isMobile } from 'react-device-detect';

import {updateDiscusVoteThunk} from '../redux/vote/thunk';
import {disucsAllInfosThunk, discusInfosByTagThunk, createDiscussionThunk, createDiscusCommentThunk, createDiscusCommentReplyThunk} from '../redux/discussion/thunk'
import {useDispatch, useSelector} from 'react-redux';
import { userInfosByVotesThunk, tagInfos } from '../redux/user/thunk';

import DiscussionList from '../components/DiscussionList';
import AddDiscussionMobileModal from '../components/AddDiscussionMobileModal';
import UserList from '../components/UserList';
import PopularTagsList from '../components/PopularTagsList';

import ShareDialog from "../components/ShareDialog";
import MoreTagsModal from "../components/MoreTagsModal";
import SignOptionModal from '../components/SignOptionModal';
import DiscussionDetailModal from '../components/DiscussionDetailModal';

import wrongIcon from '../assets/icon/wrong.png';
import whitePlus from '../assets/icon/white-plus.png';

function HomeDiscussion (props) {
	const dispatch = useDispatch();
	const allDiscussions = useSelector(state => state.discus.discusInfos.allDiscussion);
	const allDiscusUsers = useSelector(state => state.discus.discusInfos.allDiscusUsers);
	const allDiscusComment = useSelector(state => state.discus.discusInfos.discusComments);
	const allDiscusCommentsUsers = useSelector(state => state.discus.discusInfos.allDiscusCommentsUsers);
	const allDiscusCommentReply = useSelector(state => state.discus.discusInfos.discusCommentReplys);
	const allDiscusCommentReplysUsers = useSelector(state => state.discus.discusInfos.allDiscusCommentReplysUsers);
	const discusCommentCount = useSelector(state => state.discus.discusInfos.commentCount);
	const discusVotes = useSelector(state => state.discus.discusInfos.discusVotes);
	const featAllDiscussions = useSelector(state => state.discus.discusInfos.featAllDiscussion);
	const featAllDiscusUsers = useSelector(state => state.discus.discusInfos.featAllDiscusUsers);
	const featAllDiscusComment = useSelector(state => state.discus.discusInfos.featDiscusComments);
	const featAllDiscusCommentsUsers = useSelector(state => state.discus.discusInfos.featAllDiscusCommentsUsers);
	const featAllDiscusCommentReply = useSelector(state => state.discus.discusInfos.featDiscusCommentReplys);
	const featAllDiscusCommentReplysUsers = useSelector(state => state.discus.discusInfos.featAllDiscusCommentReplysUsers);
	const featCommentCount = useSelector(state => state.discus.discusInfos.featCommentCount);
	const featDiscusVotes = useSelector(state => state.discus.discusInfos.featDiscusVotes);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const userInfos = useSelector(state => state.user.userInfos);
	const discussionTags = useSelector(state => state.user.tagInfos);
	const XRCreatorsInfos = useSelector(state => state.user.XRCreatorsInfos);

	useEffect(() => {
		dispatch(disucsAllInfosThunk());
		dispatch(userInfosByVotesThunk());
		dispatch(tagInfos());
	},[dispatch])

	var discussionID;
	if(props.match !== undefined){
		discussionID  = props.match.params.discussionID;
	}
	
	let { tagInput } = useParams();

	useEffect(() => {
		if(tagInput !== undefined){
			dispatch(discusInfosByTagThunk(tagInput));
		}
		dispatch(disucsAllInfosThunk());
		dispatch(userInfosByVotesThunk());
		dispatch(tagInfos());
	},[dispatch])

	const { register, handleSubmit } = useForm({
		mode: 'onSubmit',
		reValidateMode: 'onChange',
	});
	
	const [activeUsers, setActiveUsers] = useState([]);
	const [description, setDescription] = useState("");
	const [isSignOptionModalShown, setIsSignOptionModalShown] = useState(false);
	const [inputs, setInputs] = useState({
		title: "",
		description: "",
		tags: [],
	});
	const [tag, setTag] = useState("");
	const [isDisplayShare, setShareShown] = useState(false);
	const [isMoreTagsShown, setTagShown] = useState(false);
	const [isShowAddDiscussionModal, setIsShowAddDiscussionModal] = useState(false);	
	const [dataTags, setDataTags] = useState([]);
	const [shareData, setShareData] = useState({});
	const [sort, setSorting] = useState("trending");
	const [tags, setTags] = useState([]);
	const [discussions, setDiscussions] = useState([]);
	const [discussionUsers, setDiscussionUsers] = useState([]);
	const [comments, setComments] = useState([]);
	const [commentUsers, setCommentUsers] = useState([]);
	const [commentsCount, setCommentsCount] = useState([]);
	const [reply, setReply] = useState([]);
	const [replyUsers, setReplyUsers] = useState([]);
	const [votes, setVotes] = useState([]);
	const [featured, setFeatured] = useState([]);
	const [featuredUsers, setFeaturedUsers] = useState([]);
	const [featuredComments, setFeaturedComments] = useState([]);
	const [featuredCommentUsers, setFeaturedCommentUsers] = useState([]);
	const [featuredCommentsCount, setFeaturedCommentsCount] = useState([]);
	const [featuredReply, setFeaturedReply] = useState([]);
	const [featuredReplyUsers, setFeaturedReplyUsers] = useState([]);
	const [featuredVotes, setFeaturedVotes] = useState([]);
	let [isTagsAboveMax, setIsTagsAboveMax] = React.useState(false);
	const [first, setFirst] = useState(true);
	const forceUpdate = React.useState()[1].bind(null, {});
	const [isDetailShown, setIsDetailShown] = useState(false);
	const [discussionDetail, setDiscussionDetail] = useState(false);
	const [isDetailFromLinkShown, setIsDetailFromLinkShown] = useState(false);
	const [url, setUrl] = useState("");
	
	useEffect(() => {
		var dataDiscussion = allDiscussions;
		var tempData;
		if(dataDiscussion !== undefined && dataDiscussion.length > 0) {
			for(var i = 0; i < dataDiscussion.length; i++){
				if(discussions !== undefined && discussions[i] !== undefined){
					if(discussions[i].hasOwnProperty("showComment")){
						dataDiscussion[i].showComment = discussions[i].showComment;
					}
				} else {
					tempData = {...dataDiscussion[i], ...{['showComment'] : false}};
					dataDiscussion[i] = tempData
				}
			}
			setDiscussions(dataDiscussion);
		}
		setDiscussionUsers(allDiscusUsers);
		setCommentsCount(discusCommentCount);
	},[allDiscussions])

	useEffect(() => {
		var dataFeatured = featAllDiscussions;
		var tempData;
		if(dataFeatured !== undefined && dataFeatured.length > 0) {
			for(var i = 0; i < dataFeatured.length; i++){
				if(featured && featured[i]){
					if(featured[i].hasOwnProperty("showComment"))
					dataFeatured[i].showComment = featured[i].showComment;
				} else {
					tempData = {...dataFeatured[i], ...{['showComment'] : false}};
					dataFeatured[i] = tempData
				}
			}
			setFeatured(dataFeatured);
		}
		setFeaturedUsers(featAllDiscusUsers);
		setFeaturedCommentsCount(featCommentCount);
	},[featAllDiscussions])

	var once = true;
	useEffect(() => {
		if(once && discussions.length > 0){
			once = false;
			sortTrending();
		}
	},[discussions]);

	useEffect(() => {
		if(tagInput !== undefined){
			dispatch(discusInfosByTagThunk(tagInput));
		}
	},[tagInput])
	
	useEffect(() => {
		var dataDiscussionComments = allDiscusComment;
		if(dataDiscussionComments !== undefined && dataDiscussionComments.length > 0) {
			dataDiscussionComments && dataDiscussionComments.map((data, j) => {
				if(comments !== undefined && comments[j] !== undefined){
					if(comments[j].hasOwnProperty("showInput"))
						dataDiscussionComments[j].showInput = comments[j].showInput;
				} else {
					dataDiscussionComments[j]= {...dataDiscussionComments[j], ...{['showInput'] : false}};
				}
			})
			setComments(dataDiscussionComments);
		}
		setCommentUsers(allDiscusCommentsUsers);
	},[allDiscusComment]);

	useEffect(() => {
		var dataFeaturedComments = featAllDiscusComment;
		if(dataFeaturedComments !== undefined && dataFeaturedComments.length > 0) {
			dataFeaturedComments && dataFeaturedComments.map((data, j) => {
				if(featuredComments !== undefined && featuredComments[j]){
					if(featuredComments[j].hasOwnProperty("showInput"))
					dataFeaturedComments[j].showInput = featuredComments[j].showInput;
				} else {
					dataFeaturedComments[j]= {...dataFeaturedComments[j], ...{['showInput'] : false}};
				}
			})
			setFeaturedComments(dataFeaturedComments);
		}
		setFeaturedCommentUsers(featAllDiscusCommentsUsers);
	},[featAllDiscusComment])

	useEffect(() => {
		setReply(allDiscusCommentReply);
		setReplyUsers(allDiscusCommentReplysUsers);
	},[allDiscusCommentReply])

	useEffect(() => {
		setFeaturedReply(featAllDiscusCommentReply);
		setFeaturedReplyUsers(featAllDiscusCommentReplysUsers);
	},[featAllDiscusCommentReply])

	useEffect(() => {
		if(discusVotes !== undefined && discusVotes.length > 0) {
			setVotes(discusVotes);
		}
	},[discusVotes])

	useEffect(() => {
		var dataVotes = featDiscusVotes;
		if(dataVotes !== undefined && dataVotes.length > 0) {
			setFeaturedVotes(dataVotes);
		}
	},[featDiscusVotes])

	useEffect(() => {
		setActiveUsers(XRCreatorsInfos)
	},[XRCreatorsInfos]);

	useEffect(()=>{
		if(discussionID && discussions !== undefined){
			setIsDetailFromLinkShown(true);
			displayDetail(false, null);
		}
	},[discussionID, discussions]);

	useEffect(()=>{
		if(discussionTags !== undefined && first){
			setFirst(false);
			setTags(discussionTags[1]);
			setTimeout(() => { checkDuplicate(); }, 500);
		}
	},[discussionTags]);

	const checkDuplicate = () => {
		const resultArray = [];
		const tempTags = discussionTags[1];
		Array.isArray(tempTags) && tempTags.map( item => {
			//for each item in arrayOfObjects check if the object exists in the resulting array
			if(resultArray.find(object => {
				if(object.tagName === item.tagName) {
					object.tagCountNum = object.tagCountNum + item.tagCountNum;
					return true;
					//if it does not return false
				} else {
					return false;
				}
			})){
			} else {
				//if the object does not exists push it to the resulting array and set the times count to 1
				resultArray.push(item);
			}
		})
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < resultArray.length; i += 1) {
				if (resultArray[i - 1].tagCountNum < resultArray[i].tagCountNum) {
					done = false;
					var tmp = resultArray[i - 1];
					resultArray[i - 1] = resultArray[i];
					resultArray[i] = tmp;
				}
			}
		}
		var slicedArray = resultArray.slice(0, 6).map((data, i) => {
			return data
		})
		setTags(slicedArray);
	}

	const toggleUpvote = (e, index, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		if (isAuthenticated){
			var data;
			if(isFeatured){
				data = featDiscusVotes;
			} else {
				data = discusVotes;
			}
			let newObj;
			if(data[index].length > 0){
				newObj = data[index][0];
				newObj.voted = !newObj.voted;
			} else {
				data[index].push({});
				newObj =  data[index][0];
				newObj.voted = true;
			}
			var data2;
			if(isFeatured){
				data2 = featured;
			} else {
				data2= discussions;
			}
			if(newObj.voted){
				data2[index].vote = parseInt(data2[index].vote) + 1;
			} else {
				data2[index].vote = parseInt(data2[index].vote) - 1;
			}
			dispatch(updateDiscusVoteThunk(newObj.voted, data2[index].id));
			if(isFeatured){
				setFeatured(data2);
			} else {
				setDiscussions(data2);
			}
			if(e === "modal"){
				data2[index].index = index;
				data2[index].isFeatured = isFeatured;
				setDiscussionDetail(data2[index]);
			}
			forceUpdate();
		} else {
			displaySignOptionModal();
		}
	}
	
	const handleShare = (e, id, index, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var temp;
		if(discussionID && isDetailFromLinkShown){
			temp = window.location.href;
			temp = temp.substr(0, temp.lastIndexOf("/")+1);
			setUrl(temp + "discussions/" +discussionID);
		} else {
			temp = window.location.href;
			temp = temp.substr(0, temp.lastIndexOf("/")+1);
			setUrl(temp + "discussions/" + id);
		}
		setTimeout(() => { 
			if (navigator.share) {
				navigator.share({
					title: 'Coreality',
					url: url
				}).then(() => {
					console.log('Thanks for sharing!');
				})
				.catch(console.error);
			} else {
				var newObj;
				if(isFeatured){
					newObj = featured[index];
				} else {
					newObj = discussions[index];
				}
				newObj.url = url;
				setShareShown(true);
				setShareData(newObj);
			}
		}, 500);
	}
	
		const handleCloseShare = () => {
		setShareShown(false);
	}
	
	const displayMoreTags = (e, id, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data;
		if(isFeatured){
			data = featured;
		} else {
			data = discussions;
		}
		const elementsIndex = data.findIndex(element => element.id === id );
		setTagShown(true);
		setDataTags(data[elementsIndex]);
	}
	
	const closeTagsModal = () => {
		setTagShown(false)
	}
	
	const sortTrending = () => {
		var data = discussions;
		var discusUser = discussionUsers;
		var comment = comments;
		var commentUser = commentUsers;
		var commentCount = commentsCount;
		var replies = reply;
		var replyUser = replyUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (data[i - 1].vote < data[i].vote) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					//votes discussion
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//discussion author
					if(discusUser.discusUsers !== undefined){
						temp = discusUser.discusUsers[i - 1];
						discusUser.discusUsers[i - 1] = discusUser.discusUsers[i];
						discusUser.discusUsers[i] = temp;
					}
					//comments users
					if(commentUser.allDiscusCommentUsers[i]){
						temp = commentUser.allDiscusCommentUsers[i - 1];
						commentUser.allDiscusCommentUsers[i - 1] = commentUser.allDiscusCommentUsers[i];
						commentUser.allDiscusCommentUsers[i] = temp;
					}
					//coments
					if(comment[i]){
						temp = comment[i - 1];
						comment[i - 1] = comment[i];
						comment[i] = temp;
					}
					//replies
					if(replies[i]){
						temp = replies[i - 1];
						replies[i - 1] = replies[i];
						replies[i] = temp;
					}
					//reply users
					if(replyUser.allDiscusCommentUsersReplys[i]){
						temp = replyUser.allDiscusCommentUsersReplys[i - 1];
						replyUser.allDiscusCommentUsersReplys[i - 1] = replyUser.allDiscusCommentUsersReplys[i];
						replyUser.allDiscusCommentUsersReplys[i] = temp;
					}
					//comment counts
					if(commentCount !== undefined){
						temp = commentCount.commentCountNums[i - 1];
						commentCount.commentCountNums[i - 1] = commentCount.commentCountNums[i];
						commentCount.commentCountNums[i] = temp;
					}
				}
			}
		}
		setDiscussions(data);
		setDiscussionUsers(discusUser);
		setComments(comment);
		setCommentUsers(commentUser);
		setReply(replies);
		setReplyUsers(replyUser);
		setCommentsCount(commentCount);
		setSorting("trending");
		forceUpdate();
	}
	
	const sortLatest = () => {
		var data = discussions;
		var discusUser = discussionUsers;
		var comment = comments;
		var commentUser = commentUsers;
		var commentCount = commentsCount;
		var replies = reply;
		var replyUser = replyUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (new Date(data[i - 1].created_at) - Date.now() < new Date(data[i].created_at) - Date.now()) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//discussion author
					if(discusUser.discusUsers !== undefined){
						temp = discusUser.discusUsers[i - 1];
						discusUser.discusUsers[i - 1] = discusUser.discusUsers[i];
						discusUser.discusUsers[i] = temp;
					}
					//comments users
					if(commentUser.allDiscusCommentUsers[i]){
						temp = commentUser.allDiscusCommentUsers[i - 1];
						commentUser.allDiscusCommentUsers[i - 1] = commentUser.allDiscusCommentUsers[i];
						commentUser.allDiscusCommentUsers[i] = temp;
					}
					//coments
					if(comment[i]){
						temp = comment[i - 1];
						comment[i - 1] = comment[i];
						comment[i] = temp;
					}
					//replies
					if(replies[i]){
						temp = replies[i - 1];
						replies[i - 1] = replies[i];
						replies[i] = temp;
					}
					//reply users
					if(replyUser.allDiscusCommentUsersReplys[i]){
						temp = replyUser.allDiscusCommentUsersReplys[i - 1];
						replyUser.allDiscusCommentUsersReplys[i - 1] = replyUser.allDiscusCommentUsersReplys[i];
						replyUser.allDiscusCommentUsersReplys[i] = temp;
					}
					//comment counts
					if(commentCount !== undefined){
						temp = commentCount.commentCountNums[i - 1];
						commentCount.commentCountNums[i - 1] = commentCount.commentCountNums[i];
						commentCount.commentCountNums[i] = temp;
					}
				}
			}
		}
		setDiscussions(data);
		setDiscussionUsers(discusUser);
		setComments(comment);
		setCommentUsers(commentUser);
		setReply(replies);
		setReplyUsers(replyUser);
		setCommentsCount(commentCount);
		setSorting("latest");
	}

	const handleInputChange = (event) => {
		event.persist();
		setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
	}
	
	const handleDelimeter = (e) => {
		if(e.keyCode === 13){
			if(inputs.tags.length > 3){
				setIsTagsAboveMax(true);
			} else {
				setIsTagsAboveMax(false);
				var tagTemp = tag;
				tagTemp = tagTemp.trim();
				if(tagTemp !== ""){
					var tagsNew = inputs.tags;
					tagsNew.push(tagTemp);
					setInputs(inputs => ({...inputs, tags: tagsNew}));
					tagTemp = '';
					setTag(tagTemp);
				}
			}
			forceUpdate()
		} 
	}

	const changeTagValue = (e) => {
		setTag(e.target.value);
	}
	
	const removeTag = (i) => {
		setIsTagsAboveMax(false);
		let tagsTemp = inputs.tags;
		tagsTemp.splice(i,1);
		setInputs(inputs => ({...inputs, tags: tagsTemp}));
	}

	const displayComments = (e, id, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data;
		if(isFeatured){
			data = featured;
		} else {
			data = discussions;
		}
		var index = data.findIndex(x => x.id === id);
		data[index].showComment = !data[index].showComment;
		if(isFeatured){
			setFeatured(data);
		} else {
			setDiscussions(data);
		}
		if(e === "modal"){
			data[index].index = index;
			data[index].isFeatured = isFeatured;
			setDiscussionDetail(data[index]);
		}
		forceUpdate();
	}
	
	const toggleComment = (j, k, isFeatured) => {
		var data;
		if(isFeatured){
			data = featuredComments;
		} else {
			data = comments;
		}
		data[j][k].showInput = !data[j][k].showInput;
		if(isFeatured){
			setFeaturedComments(data)
		} else {
			setComments(data);
		}
		forceUpdate();
	}
	
	const handleComment = (event) => {
		if(event.target && event.target.value){
			setDescription(event.target.value);
		}
	}
	
	const submitComment = async (placename, discus_id, comment_id, e) => {
		if (isAuthenticated){
			if(placename === "child"){
				dispatch(createDiscusCommentReplyThunk(description, discus_id, comment_id));
			} else if(placename === "parent"){
				dispatch(createDiscusCommentThunk(description, discus_id));
			}
			setTimeout(() => { dispatch(disucsAllInfosThunk()); }, 500);
			setDescription("");
			forceUpdate();
		} else {
			displaySignOptionModal()
		}
		if(e === "modal"){
			var index;
			setTimeout(() => { 
				index = discussions.findIndex(x => parseInt(x.id) === parseInt(discus_id)); 
				setDiscussionDetail(discussions[index]);
			}, 800);
		}
	}
	
	const displaySignOptionModal = () => {
		setIsSignOptionModalShown(true)
	}
	
	const hideSignOptionModal = () => {
		setIsSignOptionModalShown(false)
	}

	const displayAddMobileModal = () => {
		setIsShowAddDiscussionModal(true)
	}
	
	const hideAddMobileModal = () => {
		setIsShowAddDiscussionModal(false)
	}

	const onSubmit = (data) => {
		const { title, description} = data;
		dispatch(createDiscussionThunk(title, description, inputs.tags));
	};

	const checkKeyDown = (e) => {
		if (e.code === 'Enter') e.preventDefault();
	};

	let msg = useSelector(state => state.discus.msg)
	let alertmsg = <Alert className='alertDanger'>{msg}</Alert>

	const displayDetail = (isFeatured = false, index) => {
		var data1;
		if(isFeatured){
			data1 = featured;
		} else {
			data1 = discussions;
		}
		var data2;
		if(discussionID && isDetailFromLinkShown){
			var indexElement = data1.findIndex(x => parseInt(x.id) === parseInt(discussionID));
			data2 = data1[indexElement];
			data2 = {...data2, ...{['isFeatured'] : false}, ...{['index'] : indexElement}};
			setIsDetailFromLinkShown(false);
		} else {
			data2 = data1[index];
			data2 = {...data2, ...{['isFeatured'] : isFeatured}, ...{['index'] : index}};
		}
		setDiscussionDetail(data2);
		setIsDetailShown(true);
		
		forceUpdate();
	}

	const closeDetail = () => {
		setIsDetailShown(false);
	}

	const history = useHistory();

  const routeChange = (id) =>{
		if(isMobile){
			let path = `/discussion/`+id; 
			history.push(path);
		}
  }

	return (
		<div className="overflow-x-hidden">
			{
				isDisplayShare &&
				<ShareDialog 
					url = {url}
					data = {shareData}
					hideShare = {handleCloseShare}
				/>
			}

			{
				isMoreTagsShown &&
				<MoreTagsModal 
					data = {dataTags}
					hideModal = {closeTagsModal}
				/>
			}

			{
				isSignOptionModalShown &&
				<SignOptionModal
					hideSignOptionModal = {hideSignOptionModal}
				/>
			}

			{
				isDetailShown && 
				<DiscussionDetailModal
					discussion = {discussionDetail}
					isAuthenticated = {isAuthenticated}
					loggedUser = {userInfos}
					type = {"discussion"}
					isFeatured = {false}
					comments = {comments}
					reply = {reply}
					votes = {votes}
					authorUsers = {discussionUsers}
					commentUsers = {allDiscusCommentsUsers}
					commentReplyUsers = {allDiscusCommentReplysUsers}
					discusCommentCount = {discusCommentCount}
					toggleUpvote = {toggleUpvote}
					closeDetail = {closeDetail}
					handleShare = {handleShare}
					displayMoreTags = {displayMoreTags}
					displayComments = {displayComments}
					toggleComment = {toggleComment}
					handleComment = {handleComment}
					submitComment = {submitComment}
				/>
			}

			{
				isShowAddDiscussionModal &&
				<AddDiscussionMobileModal
					tag = {tag}
					inputs = {inputs}
					msg = {msg}
					alertmsg = {alertmsg}
					hideModal = {hideAddMobileModal}
					handleInputChange = {handleInputChange}
					handleDelimeter = {handleDelimeter}
					changeTagValue = {changeTagValue}
					removeTag = {removeTag}
					onSubmit = {onSubmit}
					checkKeyDown = {checkKeyDown}
					register = {register}
					handleSubmit = {handleSubmit}
					routeChange = {routeChange}
				/>
			}

			<div className={"fab-discussion " + (isShowAddDiscussionModal ? "d-none" : "")} onClick={() => displayAddMobileModal()}>
				<img src={whitePlus} />
			</div>

			<div className="d-flex home-container justify-content-between px-6">
				<div className="profile-col-1 profile-cols">
					<div className="text-20 mb-4">XR Creators</div>
					<div className="bg-white px-3 pt-3">
						<UserList 
							users = {activeUsers}
							type = "activeUsers"
						/>
						<Link to="/users">
							<div className="text-navy pb-4 text-center c-pointer">SEE MORE USERS</div>
						</Link>
					</div>
				</div>

				<div className="profile-col-2 profile-cols mx-5">
					<div className={tagInput !== undefined ? "d-none" : "d-block"}>
						<div className="text-20 mb-4">Featured Discussion</div>
						<DiscussionList 
							discussion={featured}
							isAuthenticated={isAuthenticated}
							loggedUser={userInfos}
							authorUsers = {featuredUsers}
							comments={featuredComments}
							commentUsers = {featuredCommentUsers}
							reply={featuredReply}
							commentReplyUsers = {featuredReplyUsers}
							votes={featuredVotes}
							discusCommentCount={featuredCommentsCount}
							type={"discussion"}
							isFeatured={true}
							toggleUpvote={toggleUpvote}
							displayComments = {displayComments}
							toggleComment = {toggleComment}
							handleComment = {handleComment}
							submitComment = {submitComment}
							handleShare={handleShare}
							handleCloseShare={handleCloseShare}
							displayMoreTags={displayMoreTags}
							isDisplayShare={isDisplayShare}
							routeChange = {routeChange}
						/>
					</div>
					
					<div className="text-20 mt-4 mb-2">Discussion</div>
					<div className="d-flex text-black homepage-project-menu-container mb-3">
						<div className={"homepage-project-menu " + ( sort === "trending" ? "active" : "")} onClick={() => sortTrending("discussion")}>Trending</div>
						<div className={"homepage-project-menu " + ( sort === "latest" ? "active" : "")} onClick={() => sortLatest("discussion")}>Latest</div>
					</div>

					<Form onSubmit={handleSubmit(onSubmit)} onKeyDown={(e) => checkKeyDown(e)} className="w-100 bg-white bg-white-border p-3 mb-3 hide-in-mobile">
						<div className="mb-3">
							<div className="label mb-1">Discussion Title</div>
							<FormGroup className="input-container mw-100-important">
								<Input 
								innerRef={register}
								type="text" 
								placeholder="Write a descriptive title of your discussion" 
								className="mw-100-important" 
								name="title" 
								onChange={(e) => handleInputChange(e)} 
								value={inputs.title}/>
							</FormGroup>
						</div>
						<div className="mb-3">
							<div className="label mb-1">What is your discussion about?</div>
							<FormGroup className="input-container mw-100-important">
								<textarea 
								ref={register} 
								className="mw-100-important" 
								placeholder="Write a short and clear description" 
								onChange={(e) => handleInputChange(e)} 
								name="description" 
								value={inputs.description}/>
							</FormGroup>
						</div>
						<div className="mb-4">
							<div className="label mb-1">Add Tags</div>
							<div className="input-container mw-100-important">
								<input type="text" name="tags" className="pr-6 mw-100-important" placeholder="Add tags related to your discussion" onKeyUp={(e) => handleDelimeter(e)} onChange={(e) => changeTagValue(e)} value={tag} />
							</div>
							<div className="tags-container w-100 mt-3 d-block">
								{ inputs.tags.length > 0 && inputs.tags.map((e, i) =>
									<div key={i} className={"tag-input mr-2 mb-2" + (i < 4 ? "d-block" : "d-none")}>{e}<img src={wrongIcon} onClick={() => removeTag(i)} className="ml-2 c-pointer"/></div>
								)}
								<Alert color="danger" className={isTagsAboveMax ? "d-block text-14 mt-2" : "d-none"}>Maximum tags amount is 4</Alert>
							</div>
						</div>
						<div className="mb-4 buttons-container">
							<Input className="btn-primary discussion-btn ml-3" type="submit" value="PUBLISH" />
						</div>
						{!msg? msg : alertmsg}
					</Form>

					<DiscussionList 
						discussion={discussions}
						isAuthenticated={isAuthenticated}
						loggedUser={userInfos}
						authorUsers = {discussionUsers}
						comments={comments}
						commentUsers = {allDiscusCommentsUsers}
						commentReplyUsers = {allDiscusCommentReplysUsers}
						reply={reply}
						votes={votes}
						discusCommentCount={commentsCount}
						type="discussion"
						isFeatured={false}
						isDisplayShare={isDisplayShare}
						toggleUpvote={toggleUpvote}
						displayComments = {displayComments}
						toggleComment = {toggleComment}
						handleComment = {handleComment}
						submitComment = {submitComment}
						handleShare={handleShare}
						handleCloseShare={handleCloseShare}
						displayMoreTags={displayMoreTags}
						routeChange = {routeChange}
					/>
				</div>

				<div className="profile-col-3 profile-cols">
					<div className="text-20 mb-4">Popular Tags</div>
					<div className="bg-white p-4">
						<PopularTagsList 
							tags={tags}
							type="discussion"
						/>
						<Link to="/tags/discussion">
							<div className="text-navy text-center c-pointer border-top pt-4">SEE MORE TAGS</div>
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
};

export default HomeDiscussion;