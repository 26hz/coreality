import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import {articleAllInfosThunk, articleInfosByTagThunk} from '../redux/article/thunk';
import {updateArtVoteThunk} from '../redux/vote/thunk';
import {useSelector, useDispatch} from 'react-redux';
import { userInfosByVotesThunk, tagInfos } from '../redux/user/thunk'

import ArticleList from "../components/ArticleList";
import UserList from '../components/UserList';
import PopularTagsList from '../components/PopularTagsList';

import ShareDialog from "../components/ShareDialog";
import MoreTagsModal from "../components/MoreTagsModal";
import SignOptionModal from "../components/SignOptionModal";

function HomeArticle (props) {
	const dispatch = useDispatch();
	const allArticle = useSelector(state => state.art.ArticleInfos.allArticle);
	const allArticleUsers = useSelector(state => state.art.ArticleInfos.allArtUsers);
	const artVotes = useSelector(state => state.art.ArticleInfos.artVotes);
	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const featAllArticles = useSelector(state => state.art.ArticleInfos.featAllArticle);
	const featAllArtUsers = useSelector(state => state.art.ArticleInfos.featAllArtUsers);
	const featArtVotes = useSelector(state => state.art.ArticleInfos.featArtVotes);
	const articleTags = useSelector(state => state.user.tagInfos);
	const userInfos = useSelector(state => state.user.userInfos);
	const XRCreatorsInfos = useSelector(state => state.user.XRCreatorsInfos);

	var articleID;
	if(props.match !== undefined){
		articleID  = props.match.params.articleID;
	}
	
	let { tagInput } = useParams();

	useEffect(() => {
		if(tagInput !== undefined){
			dispatch(articleInfosByTagThunk(tagInput));
		}
		dispatch(articleAllInfosThunk());
		dispatch(userInfosByVotesThunk());
		dispatch(tagInfos());
	},[dispatch])

	const [activeUsers, setActiveUsers] = useState([]);
	const [articles, setArticles] = useState([]);
	const [votes, setVotes] = useState([]);
	const [articleUsers, setArticleUsers] = useState([]);
	const [isDetailShown, setDetailShown] = useState(false);
	const [isSignOptionModalShown, setIsSignOptionModalShown] = useState(false);
	const [dataDetailModal, setDataDetailModal] = useState({});
	const [isDisplayShare, setShareShown] = useState(false);
	const [isMoreTagsShown, setTagShown] = useState(false);
	const [dataTags, setDataTags] = useState([]);
	const [shareData, setShareData] = useState({});
	const [sort, setSorting] = useState("trending");
	const [tags, setTags] = useState([]);
	const [featured, setFeatured] = useState([]);
	const [featuredUsers, setFeaturedUsers] = useState([]);
	const [featuredVotes, setFeaturedVotes] = useState([]);
	const [first, setFirst] = useState(true);
	const forceUpdate = React.useReducer(bool => !bool)[1];
	const [isDetailFromLinkShown, setIsDetailFromLinkShown] = useState(false);
	const [url, setUrl] = useState("");

	useEffect(() => {
		if(allArticle !== null && allArticle !== undefined && allArticle.length > 0){
			setArticles(allArticle);
		}
	},[allArticle]);

	useEffect(() => {
		if(featAllArticles !== null && featAllArticles !== undefined && featAllArticles.length > 0){
			setFeatured(featAllArticles);
		}
	},[featAllArticles]);
	
	var once = true;
	useEffect(() => {
		if(once && articles.length > 0){
			once = false;
			sortTrending();
		}
	},[articles]);

	useEffect(() => {
		if(tagInput !== undefined){
			dispatch(articleInfosByTagThunk(tagInput));
		}
	},[tagInput])

	useEffect(() => {
		if(artVotes !== null && artVotes !== undefined && artVotes.length > 0){
			setVotes(artVotes);
		}
	},[artVotes]);

	useEffect(() => {
		var dataVotes = featArtVotes;
		if(dataVotes !== undefined && dataVotes.length > 0) {
			setFeaturedVotes(dataVotes);
		}
	},[featArtVotes])

	useEffect(() => {
		if(allArticleUsers !== undefined && allArticleUsers.artUsers !== null && allArticleUsers.artUsers.length > 0){
			setArticleUsers(allArticleUsers.artUsers);
		}
	},[allArticleUsers])

	useEffect(() => {
		if(featAllArtUsers !== undefined && featAllArtUsers.featArtUsers !== null && featAllArtUsers.featArtUsers.length > 0){
			setFeaturedUsers(featAllArtUsers.featArtUsers);
		}
	},[featAllArtUsers])

	useEffect(()=>{
		if(articleID && articles){
			setIsDetailFromLinkShown(true);
			displayDetail(false, null);
		}
	},[articles]);

	useEffect(()=>{
		setActiveUsers(XRCreatorsInfos);
	},[XRCreatorsInfos])

	useEffect(() => {
		if(articleTags !== undefined && first){
			setTags(articleTags[2]);
			setFirst(false);
			setTimeout(() => { checkDuplicate(); }, 500);
		}
	},[articleTags])

	const checkDuplicate = () => {
		const resultArray = [];
		const tempTags = articleTags[2];
		Array.isArray(tempTags) && tempTags.map( item => {
			//for each item in arrayOfObjects check if the object exists in the resulting array
			if(resultArray.find(object => {
				if(object.tagName.toLowerCase() === item.tagName.toLowerCase()) {
					object.tagCountNum = object.tagCountNum + item.tagCountNum;
					return true;
					//if it does not return false
				} else {
					return false;
				}
			})){
			} else {
				//if the object does not exists push it to the resulting array and set the times count to 1
				resultArray.push(item);
			}
		})
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < resultArray.length; i += 1) {
				if (resultArray[i - 1].tagCountNum < resultArray[i].tagCountNum) {
					done = false;
					var tmp = resultArray[i - 1];
					resultArray[i - 1] = resultArray[i];
					resultArray[i] = tmp;
				}
			}
		}
		var slicedArray = resultArray.slice(0, 6).map((data, i) => {
			return data
		})
		setTags(slicedArray);
	}
	
	const toggleUpvote = (e, index, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		if (isAuthenticated){
			var data;
			if(isFeatured){
				data = featuredVotes;
			} else {
				data = artVotes;
			}
			let newObj;
			if(data[index] && data[index][0] !== undefined){
				newObj = data[index][0];
				newObj.voted = !newObj.voted;
			} else {
				data[index].push({});
				newObj = data[index][0];
				newObj.voted = true;
			}
			var data2;
			if(isFeatured){
				data2 = featured;
			} else {
				data2= articles;
			}
			if(newObj.voted){
				data2[index].vote = parseInt(data2[index].vote) + 1;
			} else {
				data2[index].vote = parseInt(data2[index].vote) - 1;
			}
			dispatch(updateArtVoteThunk(newObj.voted, data2[index].id));
			if(isFeatured){
				setFeatured(data2);
			} else {
				setArticles(data2);
			}
			if(e === "modal"){
				data2[index].index = index;
				data2[index].isFeatured = isFeatured;
				setDataDetailModal(data2[index]);
			}
			forceUpdate();
		} else {
			displaySignOptionModal()
		}
	}
	
	const handleShare = (e, id, index, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var temp;
		if(articleID && isDetailFromLinkShown){
			temp = window.location.href;
			temp = temp.substr(0, temp.lastIndexOf("/")+1);
			setUrl(temp + "articles/" + articleID);
		} else {
			temp = window.location.href;
			temp = temp.substr(0, temp.lastIndexOf("/")+1);
			setUrl(temp + "articles/" + id);
		}
		if (navigator.share) {
			navigator.share({
				title: 'Coreality',
				url: url
			}).then(() => {
				console.log('Thanks for sharing!');
			})
			.catch(console.error);
		} else {
			var newObj;
			if(isFeatured){
				newObj = featured[index];
			} else {
				newObj = articles[index];
			}
			newObj.url = url;
			setShareShown(true);
			setShareData(newObj);
		}
	}
	
	const handleCloseShare = () => {
		setShareShown(false);
	}
	
	const displayMoreTags = (e, id, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data;
		if(isFeatured){
			data = featured;
		} else {
			data = articles;
		}
		const elementsIndex = data.findIndex(element => element.id === id );
		setTagShown(true);
		setDataTags(data[elementsIndex]);
	}
	
	const closeTagsModal = () => {
		setTagShown(false)
	}

	const hideDetail = () =>{
		setDetailShown(false)
	}

	const displayDetail = (isFeatured = false, index) => {
		var data1;
		if(isFeatured){
			data1 = featured;
		} else {
			data1 = articles;
		}
		var data2;
		if(articleID && isDetailFromLinkShown){
			var indexElement = data1.findIndex(x => parseInt(x.id) === parseInt(articleID));
			data2 = data1[indexElement];
			data2 = {...data2, ...{['isFeatured'] : false}, ...{['index'] : indexElement}};
			setTimeout(() => { setIsDetailFromLinkShown(false); }, 400);
		} else {
			data2 = data1[index];
			data2 = {...data2, ...{['isFeatured'] : isFeatured}, ...{['index'] : index}};
		}
		setDetailShown(true);
		setDataDetailModal(data2);
		forceUpdate();
	}

	const displaySignOptionModal = () => {
		setIsSignOptionModalShown(true)
	}

	const hideSignOptionModal = () => {
		setIsSignOptionModalShown(false)
	}

	const sortTrending = () => {
		var data = articles;
		var artUser = articleUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (data[i - 1].vote < data[i].vote) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					//votes article
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//article author
					if(artUser.artUsers !== undefined){
						temp = artUser.artUsers[i - 1];
						artUser.artUsers[i - 1] = artUser.artUsers[i];
						artUser.artUsers[i] = temp;
					}
				}
			}
		}
		setArticles(data);
		setArticleUsers(artUser);
		setSorting("trending");
		forceUpdate();
	}
	
	const sortLatest = () => {
		var data = articles;
		var artUser = articleUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (new Date(data[i - 1].created_at) - Date.now() < new Date(data[i].created_at) - Date.now()) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//article author
					if(artUser.projUsers !== undefined){
						temp = artUser.artUsers[i - 1];
						artUser.artUsers[i - 1] = artUser.artUsers[i];
						artUser.artUsers[i] = temp;
					}
				}
			}
		}
		setArticles(data);
		setArticleUsers(artUser);
		setSorting("latest");
	}

	return (
		<div className="overflow-x-hidden">
			{
				isDisplayShare && 
				<ShareDialog 
					url = {url}
					data = {shareData}
					hideShare = {handleCloseShare}
				/>
			}

			{
				isMoreTagsShown &&
				<MoreTagsModal 
					data = {dataTags}
					hideModal = {closeTagsModal}
				/>
			}

			{
				isSignOptionModalShown &&
				<SignOptionModal
					hideSignOptionModal = {hideSignOptionModal}
				/>
			}

			<div className="d-flex home-container justify-content-between px-6">
				<div className="profile-col-1 profile-cols">
					<div className="text-20 mb-4">XR Creators</div>
					<div className="bg-white px-3 pt-3">
						<UserList 
							users = {activeUsers}
							type = "activeUsers"
						/>
						<Link to="/users">
							<div className="text-navy pb-4 text-center c-pointer">SEE MORE USERS</div>
						</Link>
					</div>
				</div>
				
				<div className="profile-col-2 profile-cols mx-5">
					<div className={tagInput !== undefined ? "d-none" : "d-block"}>
						<div className="text-20 mb-4">Featured Article</div>
						<ArticleList 
							isAuthenticated={isAuthenticated}
							loggedUser={userInfos}
							articles = {featured}
							type = "article"
							isFeatured={true}
							articleUsers = {featuredUsers}
							votes = {featuredVotes}
							shareData = {shareData}
							isMoreTagsShown = {isMoreTagsShown}
							isDisplayShare = {isDisplayShare}
							data = {dataDetailModal}
							isDetailShown = {isDetailShown}
							toggleUpvote={toggleUpvote}
							handleShare={handleShare}
							hideShare = {handleCloseShare}
							displayMoreTags={displayMoreTags}
							closeTagsModal = {closeTagsModal}
							displayDetail={displayDetail}
							hideDetail = {hideDetail}
						/>
					</div>
					<div className="text-20 mt-4 mb-2">Articles</div>
					<div className="d-flex text-black homepage-project-menu-container mb-3">
					<div className={"homepage-project-menu " + ( sort === "trending" ? "active" : "")} onClick={() => sortTrending("article")}>Trending</div>
						<div className={"homepage-project-menu " + ( sort === "latest" ? "active" : "")} onClick={() => sortLatest("article")}>Latest</div>
					</div>
					<div>
						<ArticleList 
							isAuthenticated={isAuthenticated}
							loggedUser={userInfos}
							articles = {articles}
							isFeatured = {false}
							type = "article"
							articleUsers = {articleUsers}
							shareData = {shareData}
							isMoreTagsShown = {isMoreTagsShown}
							isDisplayShare = {isDisplayShare}
							data = {dataDetailModal}
							votes = {votes}
							isDetailShown = {isDetailShown}
							toggleUpvote={toggleUpvote}
							handleShare={handleShare}
							hideShare = {handleCloseShare}
							displayMoreTags={displayMoreTags}
							closeTagsModal = {closeTagsModal}
							displayDetail={displayDetail}
							hideDetail = {hideDetail}
						/>
					</div>
				</div>


				<div className="profile-col-3 hide-in-mobile profile-cols">
					<div className="text-20 mb-4">Popular Tags</div>
					<div className="bg-white p-4">
						<PopularTagsList 
							tags={tags}
							type={"article"}
						/>
						<Link to="/tags/article">
							<div className="text-navy text-center c-pointer border-top pt-4">SEE MORE TAGS</div>
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
}

export default HomeArticle;