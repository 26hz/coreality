import React, { useState } from 'react';
import avatar from '../assets/img/avatar.png';
import google from '../assets/icon/google.png';
import facebook from '../assets/icon/white-facebook.png';
import GoogleLogin from 'react-google-login';
import ReactFacebookLogin from "react-facebook-login";
import { useDispatch, useSelector } from "react-redux";
import { loginFacebookThunk,loginGoogleThunk, loginThunk } from "../redux/auth/thunk";
import { useForm } from "react-hook-form";
import { Form, FormGroup, Input, Alert } from "reactstrap";
import { NavLink } from 'react-router-dom';

export default function LoginModal() {
	const { register, handleSubmit } = useForm();
	const dispatch = useDispatch();
	const msg = useSelector(state => state.auth.msg);
    const socialOnCLick = () => {
        return null;
    };

	const googleCallback = (userInfo) => {
		if (userInfo.accessToken) {
				dispatch(loginGoogleThunk(userInfo.accessToken));
		}
		return null;
	};
	
	const fBCallback = (userInfo) => {
		if (userInfo.accessToken) {
				dispatch(loginFacebookThunk(userInfo.accessToken));
		}
		return null;
	};

    const onSubmit = (data) => {
			if (data.email && data.password) {
				const { email, password } = data;
				dispatch(loginThunk(email, password));
			}
		};


		return (
			<div className="login-modal bg-white mt-5 margin-auto px-5 py-6 login-container d-flex justify-content-center flex-direction-column align-items-center">
				<img src={avatar} className="avatar-login" />
				<div className="text-36 my-5">Welcome to <span className="text-navy">Coreality</span></div>
				<Form onSubmit={handleSubmit(onSubmit)} className="login-button-container">
					<FormGroup>
					<GoogleLogin
					clientId="214477289347-dte80ikfae9nusp4sdk1dgdsmqcsqbfl.apps.googleusercontent.com"
					className="btn-login btn-google-login mb-3"
					buttonText="Login with Google"
					fields="name,email,picture"
					cookiePolicy={'single_host_origin'}
					onSuccess={googleCallback}
					onFailure={googleCallback}
					/>
					</FormGroup>
					<FormGroup>
					<div className="btn-login btn-facebook-login">
						<ReactFacebookLogin
							appId="1569238369930103"
							autoLoad={false}
							fields="name,email,picture"
							onClick={socialOnCLick}
							callback={fBCallback}
							icon={<img src={facebook} className="login-socmed-icon" />}
							cssClass="btn-login btn-facebook-login"
						/>
					</div>
					</FormGroup>
				</Form>
				<div className="py-6 text-center">or</div>
				{msg ? <Alert color="red">{msg}</Alert> : ""}
				<Form onSubmit={handleSubmit(onSubmit)} className="mb-4 w-100">
				<FormGroup>
                <Input
                    type="email"
                    name="email"
                    placeholder="Email"
					innerRef={register}
					className="login-input mb-4"
                />
            	</FormGroup>
				<FormGroup>
					<Input
						type="password"
						name="password"
						placeholder="Password"
						innerRef={register}
						className="login-input"
					/>
				</FormGroup>
				<div className="text-14 text-gray text-left mt-3 mb-4">Did you forget your password?</div>
				<Input type="submit" value="Submit" className="btn-primary mb-6"/>
				</Form>
				<div className="text-14 text-gray">
					You are not in Coreality yet? &nbsp;
					<span className="font-bold c-pointer">
						<NavLink to="/signup" className="link">
							Create an account
						</NavLink>
					</span>
				</div>
			</div>
		);
	}


