import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import {projectAllInfosThunk, projectInfosByTagThunk, createProjCommentThunk, createProjCommentReplyThunk} from '../redux/project/thunk';
import {updateProjVoteThunk} from '../redux/vote/thunk';
import {useSelector, useDispatch} from 'react-redux';
import { userInfosByVotesThunk, tagInfos} from '../redux/user/thunk'

import ProjectList from "../components/ProjectList";
import UserList from '../components/UserList';
import PopularTagsList from '../components/PopularTagsList';
import ShareDialog from "../components/ShareDialog";
import MoreTagsModal from "../components/MoreTagsModal";
import SignOptionModal from '../components/SignOptionModal';

const HomeProject = (props) => {
	const dispatch = useDispatch();
	const allProjects = useSelector(state => state.proj.projInfos.allProject);
	const allProjUsers = useSelector(state => state.proj.projInfos.allProjUsers);
	const allProjComment = useSelector(state => state.proj.projInfos.projComments);
	const allProjCommentsUsers = useSelector(state => state.proj.projInfos.allProjCommentsUsers);
	const allProjCommentReply = useSelector(state => state.proj.projInfos.projCommentReplys);
	const allProjCommentReplysUsers = useSelector(state => state.proj.projInfos.allProjCommentReplysUsers);
	const projCommentCount = useSelector(state => state.proj.projInfos.commentCount);
	const projVotes = useSelector(state => state.proj.projInfos.projVotes);

	const featAllProjects = useSelector(state => state.proj.projInfos.featAllProject);
	const featAllProjUsers = useSelector(state => state.proj.projInfos.featAllProjUsers);
	const featAllProjComment = useSelector(state => state.proj.projInfos.featProjComments);
	const featAllProjCommentsUsers = useSelector(state => state.proj.projInfos.featAllProjCommentsUsers);
	const featAllProjCommentReply = useSelector(state => state.proj.projInfos.featProjCommentReplys);
	const featAllProjCommentReplysUsers = useSelector(state => state.proj.projInfos.featAllProjCommentReplysUsers);
	const featCommentCount = useSelector(state => state.proj.projInfos.featCommentCount);
	const featProjVotes = useSelector(state => state.proj.projInfos.featProjVotes);

	const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
	const userInfos = useSelector(state => state.user.userInfos);
	const projectTags = useSelector(state => state.user.tagInfos);
	const XRCreatorsInfos = useSelector(state => state.user.XRCreatorsInfos);

	var projectID;
	if(props.match !== undefined){
		projectID  = props.match.params.projectID;
	}
	
	let { tagInput } = useParams();

	useEffect(() => {
		if(tagInput !== undefined){
			dispatch(projectInfosByTagThunk(tagInput));
		} else {
			dispatch(projectAllInfosThunk());
		}
		dispatch(userInfosByVotesThunk());
		dispatch(tagInfos());
	},[dispatch])

	const [activeUsers, setActiveUsers] = useState([]);
	const [description, setDescription] = useState("");
	const [isDisplayShare, setShareShown] = useState(false);
	const [isProjectDetailShown, setProjectDetailShown] = useState(false);
	const [isSignOptionModalShown, setIsSignOptionModalShown] = useState(false);
	const [dataDetailModal, setDataDetailModal] = useState({});
	const [isMoreTagsShown, setTagShown] = useState(false);
	const [dataTags, setDataTags] = useState([]);
	const [shareData, setShareData] = useState({});
	const [sort, setSorting] = useState("");
	const [tags, setTags] = useState([]);
	const [projects, setProjects] = useState([]);
	const [projectUsers, setProjectUsers] = useState([]);
	const [comments, setComments] = useState([]);
	const [commentUsers, setCommentUsers] = useState([]);
	const [commentsCount, setCommentsCount] = useState([]);
	const [reply, setReply] = useState([]);
	const [replyUsers, setReplyUsers] = useState([]);
	const [votes, setVotes] = useState([]);
	const [featured, setFeatured] = useState([]);
	const [featuredUsers, setFeaturedUsers] = useState([]);
	const [featuredComments, setFeaturedComments] = useState([]);
	const [featuredCommentUsers, setFeaturedCommentUsers] = useState([]);
	const [featuredCommentsCount, setFeaturedCommentsCount] = useState([]);
	const [featuredReply, setFeaturedReply] = useState([]);
	const [featuredReplyUsers, setFeaturedReplyUsers] = useState([]);
	const [featuredVotes, setFeaturedVotes] = useState([]);
	const [first, setFirst] = useState(true);
	const forceUpdate = React.useState()[1].bind(null, {});
	const [isDetailFromLinkShown, setIsDetailFromLinkShown] = useState(false);
	const [url, setUrl] = useState("");
	

	useEffect(() => {
		var dataProject = allProjects;
		var tempData;
		if(dataProject !== undefined && dataProject.length > 0) {
			for(var i = 0; i < dataProject.length; i++){
				if(projects[i] !== undefined){
					if(projects[i].hasOwnProperty("showComment"))
						dataProject[i].showComment = projects[i].showComment;
				} else {
					tempData = {...dataProject[i], ...{['showComment'] : false}};
					dataProject[i] = tempData
				}
			}
			setProjects(dataProject);
		}
		setProjectUsers(allProjUsers);
		setCommentsCount(projCommentCount);
		forceUpdate();
	},[allProjects]);

	useEffect(() => {
		var dataFeatured = featAllProjects;
		var tempData;
		if(dataFeatured !== undefined && dataFeatured.length > 0) {
			for(var i = 0; i < dataFeatured.length; i++){
				if(featured[i] !== undefined){
					if(featured[i] && featured[i].hasOwnProperty("showComment")){
						dataFeatured[i].showComment = featured[i].showComment;
					}
				} else {
					tempData = {...dataFeatured[i], ...{['showComment'] : false}};
					dataFeatured[i] = tempData
				}
			}
			setFeatured(dataFeatured);
		}
		setFeaturedUsers(featAllProjUsers);
		setFeaturedCommentsCount(featCommentCount);
	},[featAllProjects])

	var once = true;
	useEffect(() => {
		if(once && projects !== undefined){
			once = false;
			sortTrending();
		}
	},[projects])

	useEffect(() => {
		if(tagInput !== undefined){
			dispatch(projectInfosByTagThunk(tagInput));
		}
	},[tagInput])

	useEffect(() => {
		var dataProjectComments = allProjComment;
		if(dataProjectComments !== undefined && dataProjectComments.length > 0) {
			dataProjectComments && dataProjectComments.map((data, j) => {
				if(comments !== undefined && comments[j] !== undefined){
					if(comments[j] && comments[j].hasOwnProperty("showInput"))
						dataProjectComments[j].showInput = comments[j].showInput;
				} else {
					dataProjectComments[j]= {...dataProjectComments[j], ...{['showInput'] : false}};
				}
			})
		}
		setComments(dataProjectComments);
		setCommentUsers(allProjCommentsUsers);
	},[allProjComment])

	useEffect(() => {
		var dataFeaturedComments = featAllProjComment;
		if(dataFeaturedComments !== undefined && dataFeaturedComments.length > 0) {
			dataFeaturedComments && dataFeaturedComments.map((data, j) => {
				if(comments !== undefined && comments[j] !== undefined){
					if(comments[j].hasOwnProperty("showInput"))
					dataFeaturedComments[j].showInput = comments[j].showInput;
				} else {
					dataFeaturedComments[j]= {...dataFeaturedComments[j], ...{['showInput'] : false}};
				}
			})
			setFeaturedComments(dataFeaturedComments);
		}
		setFeaturedCommentUsers(featAllProjCommentsUsers);
	},[featAllProjComment])
	
	useEffect(() => {
		setReply(allProjCommentReply);
		setReplyUsers(allProjCommentReplysUsers);
	},[allProjCommentReply])

	useEffect(() => {
		setFeaturedReply(featAllProjCommentReply);
		setFeaturedReplyUsers(featAllProjCommentReplysUsers);
	},[featAllProjCommentReply])

	useEffect(() => {
		var dataVotes = projVotes;
		if(dataVotes !== undefined && dataVotes.length > 0) {
			setVotes(dataVotes);
		}
	},[projVotes])

	useEffect(() => {
		var dataVotes = featProjVotes;
		if(dataVotes !== undefined && dataVotes.length > 0) {
			setFeaturedVotes(dataVotes);
		}
	},[featProjVotes])

	useEffect(()=>{
		if(projectID && projects !== undefined && first){
			setIsDetailFromLinkShown(true);
			displayDetail(false, null);
		}
	},[projectID, projects]);
	
	useEffect(()=>{
		setActiveUsers(XRCreatorsInfos);
	},[XRCreatorsInfos]);

	useEffect(()=>{
		if(projectTags !== undefined && first){
			setFirst(false);
			setTags(projectTags[0]);
			setTimeout(() => { checkDuplicate(); }, 500);
		}
	},[projectTags]);

	const checkDuplicate = () => {
		const resultArray = [];
		const tempTags = projectTags[0];
		Array.isArray(tempTags) && tempTags.map( item => {
			//for each item in arrayOfObjects check if the object exists in the resulting array
			if(resultArray.find(object => {
				if(object.tagName === item.tagName) {
					object.tagCountNum = object.tagCountNum + item.tagCountNum;
					return true;
					//if it does not return false
				} else {
					return false;
				}
			})){
			} else {
				//if the object does not exists push it to the resulting array and set the times count to 1
				resultArray.push(item);
			}
		})
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < resultArray.length; i += 1) {
				if (resultArray[i - 1].tagCountNum < resultArray[i].tagCountNum) {
					done = false;
					var tmp = resultArray[i - 1];
					resultArray[i - 1] = resultArray[i];
					resultArray[i] = tmp;
				}
			}
		}
		var slicedArray = resultArray.slice(0, 6).map((data, i) => {
			return data
		})
		setTags(slicedArray);
	}
	
	const toggleUpvote = (e, index, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		if (isAuthenticated){
			var data;
			if(isFeatured){
				data = featProjVotes;
			} else {
				data = projVotes;
			}
			let newObj;
			if(data[index].length > 0){
				newObj = data[index][0];
				newObj.voted = !newObj.voted;
			} else {
				data[index].push({});
				newObj =  data[index][0];
				newObj.voted = true;
			}
			var data2;
			if(isFeatured){
				data2 = featured;
			} else {
				data2= projects;
			}
			if(newObj.voted){
				data2[index].vote = parseInt(data2[index].vote) + 1;
			} else {
				data2[index].vote = parseInt(data2[index].vote) - 1;
			}
			dispatch(updateProjVoteThunk(newObj.voted, data2[index].id));
			if(isFeatured){
				setFeatured(data2);
			} else {
				setProjects(data2);
			}
			if(e === "modal"){
				data2[index].index = index;
				data2[index].isFeatured = isFeatured;
				setDataDetailModal(data2[index]);
			}
			forceUpdate();
		} else {
			displaySignOptionModal()
		}
	}
	
	const handleShare = (e, id, index, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var temp;
		if(projectID && isDetailFromLinkShown){
			temp = window.location.href;
			temp = temp.substr(0, temp.lastIndexOf("/")+1);
			setUrl(temp + "projects/" + projectID);
		} else {
			temp = window.location.href;
			temp = temp.substr(0, temp.lastIndexOf("/")+1);
			setUrl(temp + "projects/" + id);
		}
		if (navigator.share) {
			navigator.share({
				title: 'Coreality',
				url: url
			}).then(() => {
				console.log('Thanks for sharing!');
			})
			.catch(console.error);
		} else {
			var newObj;
			if(isFeatured){
				newObj = featured[index];
			} else {
				newObj = projects[index];
			}
			newObj.url = url;
			setShareShown(true);
			setShareData(newObj);
		}
	}
	
	const handleCloseShare = () => {
		setShareShown(false);
	}
	
	const displayMoreTags = (e, id, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data;
		if(isFeatured){
			data = featured;
		} else {
			data = projects;
		}
		const elementsIndex = data.findIndex(element => parseInt(element.id) === parseInt(id));
		setTagShown(true);
		setDataTags(data[elementsIndex]);
	}
	
	const closeTagsModal = () => {
		setTagShown(false)
	}

	const sortTrending = () => {
		var data = projects;
		var projUser = projectUsers;
		var comment = comments;
		var commentUser = commentUsers;
		var commentCount = commentsCount;
		var replies = reply;
		var replyUser = replyUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (data[i - 1].vote < data[i].vote) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					//votes project
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//project author
					if(projUser.projUsers !== undefined){
						temp = projUser.projUsers[i - 1];
						projUser.projUsers[i - 1] = projUser.projUsers[i];
						projUser.projUsers[i] = temp;
					}
					//comments users
					if(commentUser.allProjCommentUsers !== undefined){
						temp = commentUser.allProjCommentUsers[i - 1];
						commentUser.allProjCommentUsers[i - 1] = commentUser.allProjCommentUsers[i];
						commentUser.allProjCommentUsers[i] = temp;
					}
					//coments
					if(comment[i]){
						temp = comment[i - 1];
						comment[i - 1] = comment[i];
						comment[i] = temp;
					}
					//replies
					if(replies[i]){
						temp = replies[i - 1];
						replies[i - 1] = replies[i];
						replies[i] = temp;
					}
					//reply users
					if(replyUser.allProjCommentUsersReplys[i]){
						temp = replyUser.allProjCommentUsersReplys[i - 1];
						replyUser.allProjCommentUsersReplys[i - 1] = replyUser.allProjCommentUsersReplys[i];
						replyUser.allProjCommentUsersReplys[i] = temp;
					}
					//comment counts
					if(commentCount !== undefined){
						temp = commentCount.commentCountNums[i - 1];
						commentCount.commentCountNums[i - 1] = commentCount.commentCountNums[i];
						commentCount.commentCountNums[i] = temp;
					}
				}
			}
		}
		setProjects(data);
		setProjectUsers(projUser);
		setComments(comment);
		setCommentUsers(commentUser);
		setReply(replies);
		setReplyUsers(replyUser);
		setCommentsCount(commentCount);
		setSorting("trending");
		forceUpdate();
	}
	
	const sortLatest = () => {
		var data = projects;
		var projUser = projectUsers;
		var comment = comments;
		var commentUser = commentUsers;
		var commentCount = commentsCount;
		var replies = reply;
		var replyUser = replyUsers;
		var vote = votes;
		var done = false;
		while (!done) {
			done = true;
			for (var i = 1; i < data.length; i += 1) {
				if (new Date(data[i - 1].created_at) - Date.now() < new Date(data[i].created_at) - Date.now()) {
					done = false;
					var tmp = data[i - 1];
					data[i - 1] = data[i];
					data[i] = tmp;
					if(vote[i]){
						var temp = vote[i - 1];
						vote[i - 1] = vote[i];
						vote[i] = temp;
					}
					//project author
					if(projUser.projUsers !== undefined){
						temp = projUser.projUsers[i - 1];
						projUser.projUsers[i - 1] = projUser.projUsers[i];
						projUser.projUsers[i] = temp;
					}
					//comments users
					if(commentUser.allProjCommentUsers[i]){
						temp = commentUser.allProjCommentUsers[i - 1];
						commentUser.allProjCommentUsers[i - 1] = commentUser.allProjCommentUsers[i];
						commentUser.allProjCommentUsers[i] = temp;
					}
					//coments
					if(comment[i]){
						temp = comment[i - 1];
						comment[i - 1] = comment[i];
						comment[i] = temp;
					}
					//replies
					if(replies[i]){
						temp = replies[i - 1];
						replies[i - 1] = replies[i];
						replies[i] = temp;
					}
					//reply users
					if(replyUser.allProjCommentUsersReplys[i]){
						temp = replyUser.allProjCommentUsersReplys[i - 1];
						replyUser.allProjCommentUsersReplys[i - 1] = replyUser.allProjCommentUsersReplys[i];
						replyUser.allProjCommentUsersReplys[i] = temp;
					}
					//comment counts
					if(commentCount !== undefined){
						temp = commentCount.commentCountNums[i - 1];
						commentCount.commentCountNums[i - 1] = commentCount.commentCountNums[i];
						commentCount.commentCountNums[i] = temp;
					}
				}
			}
		}
		setProjects(data);
		setProjectUsers(projUser);
		setComments(comment);
		setCommentUsers(commentUser);
		setReply(replies);
		setReplyUsers(replyUser);
		setCommentsCount(commentCount);
		setSorting("latest");
	}
	
	const preventDetail = (e) => {
		e.stopPropagation();
	}

	const hideDetail = () =>{
		setProjectDetailShown(false)
	}

	const displayDetail = (isFeatured = false, index) => {
		var data1;
		if(isFeatured){
			data1 = featured;
		} else {
			data1 = projects;
		}
		var data2;
		if(projectID && isDetailFromLinkShown){
			var indexElement = data1.findIndex(x => parseInt(x.id) === parseInt(projectID));
			data2 = data1[indexElement];
			data2 = {...data2, ...{['isFeatured'] : false}, ...{['index'] : indexElement}};
			setTimeout(() => { setIsDetailFromLinkShown(false); }, 400);
		} else {
			data2 = data1[index];
			data2 = {...data2, ...{['isFeatured'] : isFeatured}, ...{['index'] : index}};
		}
		setDataDetailModal(data2);
		setProjectDetailShown(true);
		forceUpdate()
	}

	const displayComments = (e, id, isFeatured) => {
		if(e !== "modal"){
			e.stopPropagation();
		}
		var data;
		if(isFeatured){
			data = featured;
		} else {
			data = projects;
		}
		var index = data.findIndex(x => x.id === id);
		data[index].showComment = !data[index].showComment;
		if(isFeatured){
			setFeatured(data);
		} else {
			setProjects(data);
		}
		forceUpdate();
	}

	const toggleComment = (j, k, isFeatured) => {
		var data;
		if(isFeatured){
			data = featuredComments;
		} else {
			data = comments;
		}
		data[j][k].showInput = !data[j][k].showInput;
		if(isFeatured){
			setFeaturedComments(data)
		} else {
			setComments(data);
		}
		forceUpdate();
	}

	const handleComment = (event) => {
		setDescription(event.target.value);
	}

	const submitComment = async (placename, proj_id, comment_id) => {
		if (isAuthenticated){
			if(placename === "child"){
				dispatch(createProjCommentReplyThunk(description, proj_id, comment_id));
			} else if(placename === "parent"){
				dispatch(createProjCommentThunk(description, proj_id));
			}
			setTimeout(() => { dispatch(projectAllInfosThunk()); }, 500);
			setDescription("");
			forceUpdate();
		} else {
			displaySignOptionModal()
		}
	}

	const displaySignOptionModal = () => {
		setIsSignOptionModalShown(true)
	}

	const hideSignOptionModal = () => {
		setIsSignOptionModalShown(false)
	}

	return (
		<div className="overflow-x-hidden">
			{
				isDisplayShare && 
				<ShareDialog 
					url = {url}
					data = {shareData}
					hideShare = {handleCloseShare}
				/>
			}

			{
				isMoreTagsShown &&
				<MoreTagsModal 
					data = {dataTags}
					hideModal = {closeTagsModal}
				/>
			}

			{
				isSignOptionModalShown &&
				<SignOptionModal
					hideSignOptionModal = {hideSignOptionModal}
				/>
			}

			<div className="d-flex home-container justify-content-between px-6">
				<div className="profile-col-1 profile-cols hide-in-mobile">
					<div className="text-20 mb-4">XR Creators</div>
					<div className="bg-white px-3 pt-3">
						<UserList 
							users = {activeUsers}
							type = "activeUsers"
						/>
						<Link to="/users">
							<div className="text-navy pb-4 text-center c-pointer">SEE MORE USERS</div>
						</Link>
					</div>
				</div>
		
				<div className="profile-col-2 profile-cols mx-5">
					<div className={tagInput !== undefined ? "d-none" : "d-block"}>
						<div className="text-20 mb-4">Featured Projects</div>
						<ProjectList
							isAuthenticated={isAuthenticated}
							loggedUser={userInfos} 
							projects={featured}
							authorUsers = {featuredUsers}
							comments={featuredComments}
							commentUsers = {featuredCommentUsers}
							commentReplyUsers = {featuredReplyUsers}
							reply={featuredReply}
							votes={featuredVotes}
							projCommentCount={featuredCommentsCount}
							type={"project"}
							isFeatured={true}
							toggleUpvote={toggleUpvote}
							displayComments = {displayComments}
							toggleComment = {toggleComment}
							handleComment = {handleComment}
							submitComment = {submitComment}
							handleShare={handleShare}
							handleCloseShare={handleCloseShare}
							displayMoreTags={displayMoreTags}
							preventDetail={preventDetail}
							displayDetail={displayDetail}
							hideDetail={hideDetail}
							dataDetailModal={dataDetailModal}
							isProjectDetailShown={isProjectDetailShown}
							isDisplayShare={isDisplayShare}
						/>
					</div>
				
					<div className="text-20 mt-4 mb-2">Projects</div>
					<div className="d-flex text-black homepage-project-menu-container mb-3">
						<div className={"homepage-project-menu " + ( sort === "trending" ? "active" : "")} onClick={() => sortTrending("project")}>Trending</div>
						<div className={"homepage-project-menu " + ( sort === "latest" ? "active" : "")} onClick={() => sortLatest("project")}>Latest</div>
					</div>
					<ProjectList
						isAuthenticated={isAuthenticated}
						loggedUser={userInfos} 
						projects={projects}
						authorUsers = {allProjUsers}
						comments={comments}
						commentUsers = {allProjCommentsUsers}
						commentReplyUsers = {allProjCommentReplysUsers}
						reply={reply}
						votes={votes}
						projCommentCount={commentsCount}
						type={"project"}
						isFeatured={false}
						toggleUpvote={toggleUpvote}
						displayComments = {displayComments}
						toggleComment = {toggleComment}
						handleComment = {handleComment}
						submitComment = {submitComment}
						handleShare={handleShare}
						handleCloseShare={handleCloseShare}
						displayMoreTags={displayMoreTags}
						preventDetail={preventDetail}
						displayDetail={displayDetail}
						hideDetail={hideDetail}
						dataDetailModal={dataDetailModal}
						isProjectDetailShown={isProjectDetailShown}
						isDisplayShare={isDisplayShare}
					/>
				</div>
		
				<div className="profile-col-3 hide-in-mobile profile-cols">
					<div className="text-20 mb-4">Popular Tags</div>
					<div className="bg-white p-4">
						<PopularTagsList 
							tags={tags}
							type="project"
						/>
						<Link to="/tags/project">
							<div className="text-navy text-center c-pointer border-top pt-4">SEE MORE TAGS</div>
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
}

export default HomeProject;