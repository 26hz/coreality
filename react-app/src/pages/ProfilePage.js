import React, { useState, useEffect } from 'react';
import {useSelector, useDispatch } from 'react-redux'
import {userInfosThunk, userProfileInfosThunk} from '../redux/user/thunk'
import moment from 'moment';
// import Header from "../components/Header"
import ProfileProject from '../components/ProfileProject';
import ProfileArticle from '../components/ProfileArticle';
import ProfileDiscussion from '../components/ProfileDiscussion';
import ProfileComment from '../components/ProfileComment';
import avatar from '../assets/img/avatar.png';
import editIcon from '../assets/icon/edit.png'; 
import fireIcon from '../assets/icon/fire.png';
import facebookIcon from '../assets/icon/black-facebook.png';
import twitterIcon from '../assets/icon/twitter.png';
import linkedinIcon from '../assets/icon/black-linkedin.png';
import comment from '../assets/icon/comment.png';
import discussion from '../assets/icon/discussion.png';
import numberArticles from '../assets/icon/number-of-articles-icon.png';
import numberProject from '../assets/icon/number-of-project-icon.png';
import userJoined from '../assets/icon/user-joined-icon.png';
import upvote from '../assets/icon/vote.png';
import {useParams} from 'react-router-dom';

function ProfilePage() {
	const [content, setContent] = useState("project");
	let param = useParams();
	const dispatch = useDispatch();
	const userInfos = useSelector(state => state.user.userProfileInfos);
    useEffect(()=>{
		dispatch(userProfileInfosThunk(param));
	},[dispatch])

	const switchContent = (c) => {
		setContent(c);
	}

	return (
		<div>
			<div className="d-flex profile-container flex-direction-column-mobile overflow-x-hidden justify-content-between px-6">
				<div className="profile-headline p-relative text-24 mobile-only mb-3 w-100 bg-white d-flex justify-content-between p-3 mt-3">
					<img src={editIcon} className="edit-icon-profile-discussion" />
				</div>
				<div className="profile-col-1 profile-cols bg-white px-3">
					<div className="mt-5 mb-2 avatar-container">
						<img src={userInfos.avator? userInfos.avator: avatar} className="profile-avatar" />
						<img src={fireIcon} className="profile-fire-icon" />
					</div>
					<div className="text-24 text-center mb-3">{userInfos.displayname ? userInfos.displayname : 'User#'+userInfos.id}</div>
					<div className="text-gray text-20 text-center mb-2">@{userInfos.username}</div>
					<div className="text-gray text-20 text-center mb-2">{userInfos.role}</div>
					<div className="pb-3 mb-2 d-flex justify-content-center align-items-center border-bottom">
						<a href={userInfos.facebook? userInfos.facebook : ''}><img className="socmed-profile-icon mr-4" src={facebookIcon} /></a>
						<a href={userInfos.linkedin? userInfos.linkedin : ''}><img className="socmed-profile-icon mr-4" src={linkedinIcon} /></a>
						<a href={userInfos.twitter? userInfos.twitter : ''}><img className="socmed-profile-icon" src={twitterIcon} /></a>
					</div>
					
					<div className="mt-3 d-flex justify-content-center align-items-center border-bottom pb-3 mb-2">
						<button className="btn-inverted p-2 mr-2">CONTACT</button>
						<a href={userInfos.website? userInfos.website : ''}><button className="btn-primary">WEBSITE</button></a>
					</div>
					
					<div className="text-20 mt-3 mb-2">About me</div>
					<div className="text-gray text-14 border-bottom pb-3 mb-2">{userInfos.bio ? userInfos.bio : 'Biography'}</div>

					<div className="text-20 mb-2">Core Skills</div>
					<div className="skill-container d-block mt-3 pb-3 mb-0 border-bottom w-100">
						{ userInfos.skilltag && userInfos.skilltag.map((tag, j) =>
							<div key={j} className={"tag mr-2 " + (j > 0 ? "hide-in-mobile " : "")}>{tag}</div>
						)}
					</div>
					
					<div className="text-20 mt-4 mb-2">Experience</div>
					<div className="text-14 text-gray mb-5">{userInfos.exp} Years</div>
				</div>


				<div className="profile-col-2 profile-cols mx-5">
					<div className="text-24 hide-in-mobile mb-3 bg-white d-flex justify-content-between p-3 mb-5">
						<div>
							{userInfos.headline}
						</div> 
					</div>

					<div className="header-profile bg-white d-flex justify-content-center">
						<button 
							onClick={() => switchContent("project")} 
							className={"text-14 text-center py-3 btn-nav-profile " + (content === "project" ? "active" : '')}
						>
							PROJECTS
						</button>
						<button 
							onClick={() => switchContent("discussion")} 
							className={"text-14 text-center py-3 btn-nav-profile " + (content === "discussion" ? "active" : '')}
						>
							DISCUSSIONS
						</button>
						<button 
							onClick={() => switchContent("article")} 
							className={"text-14 text-center py-3 btn-nav-profile " + (content === "article" ? "active" : '')}
						>
							ARTICLES
						</button>
						{/* <button 
							onClick={() => switchContent("answer")} 
							className={"text-14 text-center py-3 btn-nav-profile " + (content === "answer" ? "active" : '')}
						>
							ANSWERS
						</button> */}
					</div>

					<div className="profile-content">
						{(() => {
							switch (content) {
								case 'project':
									return (
										<ProfileProject />
									)
								case 'discussion':
									return (
										<ProfileDiscussion />
									)
								case 'article':
									return (
										<ProfileArticle />
									)
								case 'answer':
									return (
										<ProfileComment />
									)
								default:
									return (
										<ProfileProject />
									)
							}
						})()}
					</div> 
				</div>

				<div className="profile-col-3 profile-cols bg-white p-4">
					<div className="d-flex align-items-center pb-4 mb-4 border-bottom">
						<img src={userJoined} className="profile-info-icon mr-3" /> 
						<div>Joined on {moment( new Date(userInfos.created_at), "DD/MM/YYYY h:mm:ss").startOf('second').fromNow()}</div>
					</div>
					<div className="text-14 d-flex align-items-center pb-4">
						<img src={numberProject} className="profile-info-icon mr-3" /> 
						<div className="text-14 line-height-24">Number of Projects: {userInfos.projCount}</div>
					</div>
					<div className="d-flex align-items-center pb-4 mb-4 border-bottom">
						<img src={upvote} className="profile-info-icon mr-3" />
						<div className="text-14">Total votes received {userInfos.voteCount}</div>
					</div>
					<div className="text-14 d-flex align-items-center pb-4">
						<img src={comment} className="profile-info-icon mr-3" />
						<div className="text-14">Number of Comments {userInfos.commentCount}</div>
					</div>
					<div className="text-14 d-flex align-items-center pb-4">
						<img src={discussion} className="profile-info-icon mr-3" />
						<div className="text-14">Number of Discussions {userInfos.discusCount}</div>
					</div>
					<div className="text-14 d-flex align-items-center">
						<img src={numberArticles} className="profile-info-icon mr-3" />
						<div className="text-14">Number of Articles {userInfos.artCount}</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ProfilePage;