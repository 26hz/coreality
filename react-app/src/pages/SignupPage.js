/* eslint-disable jsx-a11y/accessible-emoji */
import React, { useState } from 'react';
import { Alert } from 'reactstrap';
import { Field } from 'react-final-form';
import Wizard from '../wizard';
import avatar from '../assets/img/avatar.png';
import {useSelector, useDispatch } from "react-redux";
import signupThunk from "../redux/auth/thunk";
import {NavLink} from 'react-router-dom';
import wrongIcon from '../assets/icon/wrong.png';

export default function SignupPage() {
  
  const useForceUpdate = () => {
    const [value, setValue] = useState(0); // integer state
    return () => setValue(value => value + 1); // update the state to force render
  }

  let [tag, setTag] = React.useState('');
  let [tags, setTags] = React.useState([]);
  let [isTagEmpty, setIsTagEmpty] = React.useState(false);
  const forceUpdate = useForceUpdate();

  const handleTag = (e) =>{
    if(e.keyCode === 13){
      tag = tag.trim();
      setTags(tags.concat(tag))
      tag = '';
      setTag(tag)
      setIsTagEmpty(false)
    } 
  }
  
  const removeTag = (e, i) => {
    tags.splice(i,1);
    setTags(tags);
    if(tags.length === 0){
      setIsTagEmpty(true)
    }
    forceUpdate();
  }

  const handleInputChange = (e)=>{
    const target = e.target;
    const value = target.value;
    setTag(value);
  }

  const dispatch = useDispatch();
  const onSubmit = async (values) => {
    if(tags.length === 0){
      setIsTagEmpty(true)
    } else {
      const { email, username, password, displayname, role, exp} = values;
      dispatch(signupThunk(email, username, password, displayname, role, exp, tags ));
    }
  }

  let msg = useSelector(state => state.auth.msg);
  let alertmsg = <Alert className='alertDanger'>{msg}</Alert>

  const [visible, setVisible] = useState(false);

  const Error = ({ name }) => (
    <Field
      name={name}
      subscribe={{ touched: true, error: true }}
      render={({ meta: { touched, error } }) =>
        touched && error ? <span>{error}</span> : null
      }
    />
  )
  
  const required = value => (value ? undefined : 'Required');
  const validatePassword = value => (value.length > 6 ) ? setVisible(false) : setVisible(true);
  const composeValidators = (...validators) => value =>
    validators.reduce((error, validator) => error || validator(value), undefined)

  return (
    <div className="login-modal-container">
      <div className="login-modal bg-white mt-5 margin-auto px-5 py-6 login-container d-flex justify-content-center flex-direction-column align-items-center">
      <img src={avatar} className="avatar-login" />
      <div className="text-36 my-5">Welcome to <span className="text-navy">Coreality</span></div>
      <Wizard
        onSubmit={onSubmit}
      >
        <Wizard.Page>
          <div className="mb-4 w-100">
          <Field
              name="username"
              component="input"
              type="text"
              placeholder="Username"
              validate={required}
            />
            <Error name="username" />
          </div>
          <div className="mb-4 w-100">
            <Field
              name="email"
              component="input"
              type="email"
              placeholder="Email"
              validate={required}
            />
            <Error name="email" />
          </div>        
          <div className="mb-4 w-100">
            <Field
              name="password"
              component="input"
              type="password"
              placeholder="Password"
              validate={composeValidators(required, validatePassword)}
            />
            <Error name="password" />
          </div>
          <Alert color="danger" isOpen={visible}>
            Password must have at least 6 characters
          </Alert>
        </Wizard.Page>
        <Wizard.Page>
          <div className="mb-4 w-100">
          <Field
              name="displayname"
              component="input"
              type="text"
              placeholder="Display Name"
              validate={required}
            />
            <Error name="displayname" />
          </div>
          <div className="mb-4 w-100">
            <Field
              name="role"
              component="input"
              type="text"
              placeholder="Role"
              validate={required}
            />
            <Error name="role" />
          </div>        
          <div className="mb-4 w-100">
            <Field
              name="exp"
              component="input"
              type="number"
              placeholder="Year of Experience"
              validate={required}
            />
            <Error name="exp" />
          </div>
        </Wizard.Page>
        <Wizard.Page>
          <div className="mb-4 w-100">
            <div className="input-container">
              <input 
                type="text" 
                name="tags" 
                className="pr-6" 
                placeholder="Skill Tags" 
                onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                onKeyUp={(e) => handleTag(e)}
                value={tag} 
                onChange={(e) =>  handleInputChange(e)} />
						</div>
            <div className="tags-container d-block mt-2 mb-2">
              { tags.map((e, i) =>
                <div key={i} className="tag-input mr-2 mb-2">
                  {e}
                  <img 
                    src={wrongIcon} 
                    onClick={(e) => removeTag(e, i)} 
                    className="ml-2 c-pointer"/>
                </div>
              )}
            </div>
            {isTagEmpty ? "Required" : ""}
          </div>
          {!msg? msg : alertmsg}
        </Wizard.Page>
      </Wizard>
          <div className="text-14 text-gray">
        Do you have an account? &nbsp; 
        <span className="font-bold c-pointer">
          <NavLink to="/login" >
            Login Here
          </NavLink>
        </span>
        </div>
      </div>
    </div>
  )
}


// render(<App />, document.getElementById('root'))
